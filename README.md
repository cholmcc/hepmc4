![logo](doc/logo.png)

# High-Energy Physics Monte-Carlo event library - version "4" 

This is a direction that the
[**HepMC3**](https://gitlab.cern.ch/HepMC/HepMC3) library could go. 

[GitLab repository](https://gitlab.com/cholmcc/hepmc4).

![W event](doc/w.png)

## Introduction 

This library of classes is an attempt at generating the next version
of [HepMC](https://gitlab.cern.ch/HepMC/HepMC3).  It differs in that
- the library is _header only_,
- data classes are templated on floating point type, 
- and many other things ... 

More details are given in [this file](CHANGES.md).
  
## Building 

There is no installable code to build _per-se_ (except if
[ProtoBuf](https://protobuf.dev/) is to be used).  Instead, a user
should copy the directory `hepmc4` to any desired location, e.g.,
`/usr/local/include`. 

The project does come with some tests, examples, and benchmarks. To
build those, simply do

    make 
    
To run the tests, do 

    make test 
    
To run some example tests, do 

    make -C examples test 
    
If ROOT is found (more specifically, if the script `root-config` is in
the users `PATH`), then the tests and examples will be build with ROOT
I/O support enabled.  To disable that, do 

    make DISABLE_ROOT=1 ...
    
If ProtoBuf is found (more specifically, if the application `protoc`
is in the users `PATH`, and `pkg-config --exists protobuf` succeeds),
then the tests and examples will be build with ProtoBuf I/O support.
To disable this, do 

    make DISABLE_PROTOBUF=1 ...
    
To use the Protobuf interface, the two files `hepmc4.pb.cc` and
`hepmc4.pb.hh` need to be built.   Simply do 

    make -C hepmc4 
    
to build these. 
    
If Pythia8 is found (more specifically, if the script `pythia8-config`
is in the users `PATH`), then the example application
[`examples/pythia`](examples/pythia.cc) will be build.  To disable
that, do 

    make DISABLE_PYTHIA8=1 ...
    
[Boost iostreams](https://boost.org) library can be used to read and
write ASCII formats from and to compressed files, respectively.  If it
is not desirable to use this libary, one can likewise do

    make DISABLE_BOOST_IOSTREAMS=1 ... 
    
If no dependent library (e.g., ROOT or ProtoBuf) enforces a specific
C++ standard (e.g., through their `CXXFLAGS`), then a C++ standard can
be specified by for example

    make CXX_STANDARD=c++14
    
C++ standard version 2011 is the _least_ possible standard to use.
Any earlier standard (`98` and `03`) are _not_ supported. For some
older standards, the header [`hepmc4/compat`](hepmc4/compat) provides
compatibility features. 

Doxygen documentation is generated in the sub-directory `doc`.
Please refer to the documentation (and code comments) for more on all
the points above. 

If you need to change compiler, linker, or some flags, edit
`Flags.mk`.  This is used throughout the project and changes should
only be done there. 

## Benchmarking 

See [here](bench/README.md)

## Tests 

See [here](tests/README.md)

