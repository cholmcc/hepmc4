#  Copyright (C) 2023  Christian Holm Christensen
#
#  This program is free software: you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public License
#  as published by the Free Software Foundation, either version 3 of
#  the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see
#  <https://www.gnu.org/licenses/>.
#                              
#
# Note, we need at least C++11 (-std=c++11)
#
# Various flags and rules
#
ifndef DISABLE_ROOT
ROOT_CONFIG	:= $(shell command -v root-config)
endif
ifneq ($(ROOT_CONFIG),)
WITH_ROOT	:= 1
ROOT_CPPFLAGS	:= -DHEPMC4_HAS_ROOT=1 \
		   $(filter -D% -I%, $(shell $(ROOT_CONFIG) --cflags))
ROOT_CXXFLAGS	:= $(filter-out -D% -I%, $(shell $(ROOT_CONFIG) --cflags))
ROOT_LDFLAGS	:= $(filter-out -l%, $(shell $(ROOT_CONFIG) --libs))
ROOT_LIBS	:= $(filter -l%, $(shell $(ROOT_CONFIG) --libs)) -lEG
endif

ifndef DISABLE_PROTOBUF
PROTOC		:= $(shell command -v protoc)
endif 
ifneq ($(PROTOC),)
WITH_PROTOBUF	:= 1
PROTOC_CPPFLAGS := -DHEPMC4_HAS_PROTOBUF=1 \
		   $(shell pkg-config --cflags-only-I protobuf)
PROTOC_CXXFLAGS := $(shell pkg-config --cflags-only-other protobuf)
PROTOC_LDFLAGS  := $(shell pkg-config --libs-only-L protobuf)	\
		   $(shell pkg-config --libs-only-other protobuf)
PROTOC_LIBS	:= $(shell pkg-config --libs-only-l protobuf)
endif

ifndef DISABLE_BOOST_ISTREAMS
WITH_BOOST	:= 1
BOOST_CPPFLAGS	:= -DHEPMC4_HAS_BOOST_IOSTREAMS=1
BOOST_CXXFLAGS	:=
BOOST_LDFLAGS	:=
BOOST_LIBS	:= -lboost_iostreams
endif

ifndef DISABLE_PYTHIA8
PYTHIA8_CONFIG	:= $(shell command -v pythia8-config)
endif
ifneq ($(PYTHIA8_CONFIG),)
WITH_PYTHIA8	:= 1
PYTHIA8_CPPFLAGS:= $(filter -D% -I%, $(shell $(PYTHIA8_CONFIG) --cxxflags))
PYTHIA8_CXXFLAGS:= $(filter-out -D% -I%, $(shell $(PYTHIA8_CONFIG) --cxxflags))
PYTHIA8_LDFLAGS	:= $(filter-out -l%, $(shell $(PYTHIA8_CONFIG) --ldflags))
PYTHIA8_LIBS	:= $(filter -l%, $(shell $(PYTHIA8_CONFIG) --ldflags))
endif

ifndef DISABLE_PYTHON
PYBIND11_EXISTS	:= $(shell pkg-config --exists pybind11 && echo "yes")
endif
ifneq ($(PYBIND11_EXISTS),)
WITH_PYTHON	:= 1
PYTHON		:= python
endif

CXX		:= g++
CXXFLAGS	:= -c -g -Wall -Wextra -pedantic \
		   $(ROOT_CXXFLAGS) $(PROTOC_CXXFLAGS) $(BOOST_CXXFLAGS)
CPPFLAGS	:= -I.. $(ROOT_CPPFLAGS) $(PROTOC_CPPFLAGS) $(BOOST_CPPFLAGS)
LD		:= g++
LDFLAGS		:= $(ROOT_LDFLAGS) $(PROTOC_LDFLAGS) $(BOOST_LDFLAGS)
LIBS		:= $(ROOT_LIBS) $(PROTOC_LIBS) $(BOOST_LIBS)
#CXX_STANDARD	:= c++11

ifdef CXX_STANDARD
ifeq ($(filter -std=%, $(CXXFLAGS)),)
CXXFLAGS	+= -std=$(CXX_STANDARD)
endif
endif
ifdef OPTIMIZE
CXXFLAGS	+= -O2
endif

MUTE		:= @
ifdef VERBOSE
MUTE		:=
endif

#
# EOF
#
