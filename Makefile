#  Copyright (C) 2023  Christian Holm Christensen
#
#  This program is free software: you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public License
#  as published by the Free Software Foundation, either version 3 of
#  the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see
#  <https://www.gnu.org/licenses/>.
#
#
#
SUBDIRS		= hepmc4 tests examples bench python doc

all install clean:	.headers .tests
	$(foreach s, $(SUBDIRS), $(MAKE) $@ -C $(s);)
	$(MAKE) local-$@

test:	all
	$(MAKE) -C tests $@ 

bench:	all
	$(MAKE) -C bench time_all

local-all:

local-install:

local-clean:
	rm -f *~ tools/c++vers
	rm -rf build dist *.so *.egg-info

all-python:
	python -m build --wheel 

install-python:all-python
	pip install -m python -m build --wheel 

.headers: $(filter-out Makefile README.md %~, $(wildcard hepmc4/*))
	@for h in $^ ; do \
	    printf "\t\t   %-30s\t\\\\\n" $$h; done | \
	   sed -e '1 s/[[:space:]]*/HEADERS\t\t:= /' -e '$$ s/\\//' > $@

.tests: $(filter-out tests/Test% tests/foo% %~, $(wildcard tests/test*.cc))
	@for h in $^ ; do \
	    printf "\t\t   %-30s\t\\\\\n" $$h; done | \
	   sed -e '1 s/[[:space:]]*/TEST_SOURCES\t:= /' -e '$$ s/\\//' > $@

.benches:: $(filter-out %~, $(wildcard bench/tests/*.cc))
	@for h in $^ ; do \
	    printf "\t\t   %-30s\t\\\\\n" $$h; done | \
	   sed -e '1 s/[[:space:]]*/BENCH_SOURCES\t:= /' -e '$$ s/\\//' > $@
.benches:: $(filter-out %~, $(wildcard bench/bench/*))
	@for h in $^ ; do \
	    printf "\t\t   %-30s\t\\\\\n" $$h; done | \
	   sed -e '1 s/[[:space:]]*/BENCH_HEADERS\t:= /' -e '$$ s/\\//' >> $@

.examples:: $(filter-out %~, $(wildcard examples/*.cc))
	@for h in $^ ; do \
	    printf "\t\t   %-30s\t\\\\\n" $$h; done | \
	   sed -e '1 s/[[:space:]]*/EXAMPLE_SOURCES\t:= /' -e '$$ s/\\//' > $@
.examples:: $(filter-out %~ %.cc %.o %.hepmc3 %Makefile %angantyr, $(wildcard examples/*))
	@for h in $^ ; do \
            if test -f $$h.cc ; then continue ; fi ; \
	    printf "\t\t   %-30s\t\\\\\n" $$h; done | \
	   sed -e '1 s/[[:space:]]*/EXAMPLE_HEADERS\t:= /' -e '$$ s/\\//' >> $@


.gitignore:$(filter-out %~, $(wildcard tests/*.cc))
	@echo "*~"		>  $@
	@echo "*.o"		>> $@
	@echo "*.hepmc*"	>> $@
	@echo "*.hepevt*"	>> $@
	@echo "*.root*"		>> $@
	@echo "*.log"		>> $@
	@echo "*.gv*"		>> $@
	@echo "html"		>> $@
	@echo "c++vers"		>> $@
	@echo "foo*"		>> $@
	@for h in $^; do echo `basename $$h .cc`; done >> $@



#
#
