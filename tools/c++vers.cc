#include <iostream>
#include <map>

int main()
{
  std::map<int,int> v
#if __cplusplus >= 201103L
    {{199711L,98},
     {201103L,11},
     {201402L,14},
     {201703L,17},
     {202002L,20},
     {202302L,23}};
  auto iter = v.find(__cplusplus);
#else
  ;
  v[199711L] = 98;
  v[201103L] = 11;
  v[201402L] = 14;
  v[201703L] = 17;
  v[202002L] = 20;
  v[202302L] = 23;
  std::map<int,int>::iterator iter = v.find(__cplusplus);
#endif  

  if (iter == v.end()) {
    std::cerr << "Unknown version: " << __cplusplus << std::endl;
    return 1;
  }

  std::cout << "C++ standard is " << iter->second << std::endl;

  return 0;
}
