# Benchmark base code

Base code for benchmark test. 

## Content

- [`driver`](driver) Driver of the tests.  This defines the
  function templates `bench::driver` and `bench::looper`. 
  
  `bench::driver` takes care of 
  
  - Deducing, from the program name, the version being tested via the
    function [`bench::version`](version)
  - Parsing command line and setting options 
  - Creating a [`reader`](reader) for the input file. 
  - Loop over read input using the function template `looper` 
  
  The `bench::driver` takes the name of test being performed, the
  function to call on each event, and the command line parameters. 
  
  `bench::looper` Loops over the events read by a reader.  For each
  event read, it then calls the passed test function.  If the
  corresponding option is set, then each call to the test function is
  timed, and result, together with the number of particles in the
  event, are output.
  
- [`logger`](logger) Takes care of setting up an log stream and
  reporting timing information on that stream. 
  
- [`options`](options) Structure of command line options, and a
  function to parse the command line flags.  
  
- [`timer`](timer) Defines a timer guard.  An object of this
  type starts a timer on construction and stops and reports the
  result, on destruction.
  
- [`version`](version)  Defines the function `bench::version`
  which will deduce the version test from the name of program. 
  
