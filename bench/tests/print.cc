//
// Copyright (c) 2023 Christian Holm Christensen <cholmcc@gmail.com>
// Distributed under the GPL-3
//
#include <bench/driver>
#include <iostream>
#include <fstream>

//--------------------------------------------------------------------
/**
   Print an event

   @tparam T     Floating point values type 
   @param event
   @return Number of operations
   @ingroup hepmc_bench_tests
*/
template <typename T>
int print(hepmc4::event_ptr<T>& event)
{
  std::ofstream out("/dev/null");
  out << *event << std::endl;
  return 1;
}
  
//--------------------------------------------------------------------
int
main(int argc, char** argv)
{
  return bench::driver<double>("print",print<double>,argc,argv);
}
//
// EOF
//


      
        
        
        
