//
// Copyright (c) 2023 Christian Holm Christensen <cholmcc@gmail.com>
// Distributed under the GPL-3
//
#include <bench/driver>
#include <HepMC3/FourVector.h>

//--------------------------------------------------------------------
/**
   Rotate an event

   @tparam T     Floating point values type 
   @param event
   @return Number of operations
   @ingroup hepmc_bench_tests
*/
template <typename T>
int rotate(hepmc4::event_ptr<T>& event)
{
  event->rotate(0.2,0.3,0.4);
  return 1;
}
  
//--------------------------------------------------------------------
int
main(int argc, char** argv)
{
  
  return bench::driver<double>("rotate",rotate<double>,argc,argv);
}
//
// EOF
//


      
        
        
        
