//
// Copyright (c) 2023 Christian Holm Christensen <cholmcc@gmail.com>
// Distributed under the GPL-3
//
#include <bench/driver>
#include <hepmc4/io_factory>

namespace bench {
  //------------------------------------------------------------------
  /**
     Loop over events and possibly time the reading. This is
     specialised so that we may declare the writer outside of the
     event loop.
     
     @tparam T     Floating point values type 
     @param reader   Reader of data
     @param opts     Options
     @param log      Log stream
     @ingroup hepmc_bench_tests
  */
  template <typename T, typename F>
  typename std::enable_if<std::is_same<F, std::nullptr_t>::value, void>::type
  looper(hepmc4::reader_ptr<T> reader,
         options&              opts,
         logger&               log,
         F                     )
  {
    using builder_type=hepmc4::builder<T>;

    std::string outName = (opts.verbose ? "" : "/dev/null");
    auto output = hepmc4::io_factory::open_output(outName, std::cerr);
    auto writer = hepmc4::io_factory::deduce_writer<T>(outName,output.stream);
    auto event  = builder_type::event();
    auto more   = [](int iev, int max) { return max <= 0 or iev < max; };
    
    int iev = 0;
    while (more(iev++, opts.maxEvents) and reader->read_event(event)) {
      if (iev < opts.firstEvent) continue;
      show(event, opts);

      timer t(log, event->particles().size(), opts.timing);
      t._m = 1;
      writer->write_event(event);
    }
  }
}
//--------------------------------------------------------------------
int
main(int argc, char** argv)
{
  return bench::driver<double>("write",nullptr,argc,argv);
}
//
// EOF
//


      
        
        
        
