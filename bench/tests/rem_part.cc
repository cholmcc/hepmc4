//
// Copyright (c) 2023 Christian Holm Christensen <cholmcc@gmail.com>
// Distributed under the GPL-3
//
#include <bench/driver>
#include <list>

//--------------------------------------------------------------------
/**
   Remove particles from an event

   @tparam T     Floating point values type 
   @param event
   @return Number of operations
   @ingroup hepmc_bench_tests
*/
template <typename T>
int rem_part(hepmc4::event_ptr<T>& event)
{
  // This is two-staged, because if we remove elements while looping
  // over the list, then we can get problems.
  size_t n = 0;
  std::list<hepmc4::particle_ptr<T>> toRemove;
  for (auto iter = event->particles().begin();
       iter != event->particles().end(); ++iter) {
    if (n++ % 10 == 0) 
      toRemove.emplace_back(*iter);
  }
  for (auto& p : toRemove) event->remove_particle(p);
  return toRemove.size();
}
  
//--------------------------------------------------------------------
int
main(int argc, char** argv)
{
  return bench::driver<double>("rem_part",rem_part<double>,argc,argv);
}
//
// EOF
//


      
        
        
        
