//
// Copyright (c) 2023 Christian Holm Christensen <cholmcc@gmail.com>
// Distributed under the GPL-3
//
#include <bench/driver>
#include <list>

// ---------------------------------------------------------------
/**
   Generate a sub-branch of a particle.  The particle is marked
   as decayed (not physical), and two particles are produced
   splitting the input momentum 30-70%.  The decay vertex has
   it's status set to the depth.  The generated particles are
   added to the list of leaves

   @tparam T     Floating point values type 
   @param p      Incoming particle
   @param d      Depth
   @param n      Iteration at current depth
   @param leaves Outgoing particles are added to this list
   @return The number of particles added

   @ingroup hepmc_bench_tests
*/ 
template <typename T>
size_t sub_generate(hepmc4::particle_ptr<T>             p,
                    size_t                      d,
                    size_t                      n,
                    std::list<hepmc4::particle_ptr<T>>& leaves)
{
  using builder_type=hepmc4::builder<T>;
  
  // p->status() = 2;
  auto m  = p->momentum;
  auto m1 = m * .3;
  auto m2 = m * .7;
  auto a  = M_PI / 10 * d;
  m1.rotate( a,  a,  a);
  m2.rotate(-a, -a, -a);
  auto c  = d * .01 + n * 0.001;
  auto v  = builder_type::vertex(c, c, c, c / 1000, d);
  auto p1 = builder_type::particle(m1,  1, d);
  auto p2 = builder_type::particle(m2, -1, d);

  v->add_incoming(p);
  v->add_outgoing(p1);
  v->add_outgoing(p2);
  
  leaves.emplace_back(p1);
  leaves.emplace_back(p2);

  return 2;
}

// ---------------------------------------------------------------
/**
   Generate an event.  This makes two beam protons and add them
   to a vertex.  It then adds a single out-going particle of that
   vertex.  Then down to the maximum depth, or until enough
   particles have been added (whatever happens first), for each
   out-going particle at given depth, we add two new particles.
   
   The positions of vertices are crafted to enable easy
   identification of the vertices.
   
   @tparam T     Floating point values type 
   @param event      Event to populate
   @param depth      Maximum depth to go to
   @param nparticles Maximum number of particles to generate
   
   @ingroup hepmc4_bench_tests
*/
template <typename T>
void generate(hepmc4::event_ptr<T>  event,
              size_t                depth=5,
              size_t                nparticles=100)
{
  using builder_type=hepmc4::builder<T>;

  event ->clear();
  event ->number++;
  auto b1 = builder_type::particle(0, 0,  7000, 7000,  2212, 4);
  auto b2 = builder_type::particle(0, 0, -7000, 7000,  2212, 4);
  auto ip = builder_type::vertex  (builder_type::four_vector(),0);
  auto o1 = builder_type::particle(1000, 0, 0, 1000, 21, 1);
  ip->add_incoming(b1);
  ip->add_incoming(b2);
  ip->add_outgoing(o1);
  // o1->momentum.rotate(M_PI / 10, M_PI / 10, M_PI / 10);

  // Must be done early 
  event->add_vertex(ip);
            
  size_t counter = 3;
  size_t level   = 1;
  bool   more    = true;
  std::list<typename builder_type::particle_ptr> leaves{o1};
      
  while (level < depth and more) {
    std::list<typename builder_type::particle_ptr> new_leaves;
    size_t n = 0;
    for (auto& p : leaves) {
      counter += sub_generate(p, level, n++, new_leaves);
      if (counter < nparticles) continue;
          
      more = false;
      break;
    }
    level++;
    leaves = new_leaves;
  }
}

//--------------------------------------------------------------------
/**
   Add a tree to an event

   @tparam T     Floating point values type 
   @param event
   @return Number of operations
   @ingroup hepmc_bench_tests
*/
template <typename T>
int add_tree(hepmc4::event_ptr<T>& event)
{
  size_t n = event->particles().size();
  generate(event,10000,n);

  return 1;
}
  
//--------------------------------------------------------------------
int
main(int argc, char** argv)
{
  return bench::driver<double>("add_tree",add_tree<double>, argc,argv);
}
//
// EOF
//


      
        
        
        
