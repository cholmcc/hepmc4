//
// Copyright (c) 2023 Christian Holm Christensen <cholmcc@gmail.com>
// Distributed under the GPL-3
//
#include <bench/driver>

//--------------------------------------------------------------------
/**
   Count number of ancestors of particles in event

   @tparam T     Floating point values type 
   @param event
   @return Number of operations
   @ingroup hepmc_bench_tests
*/
template <typename T>
int cnt_anc(hepmc4::event_ptr<T>& event)
{
  int n = 0;
  for (auto particle : event->particles()) {
    std::unordered_set<std::shared_ptr<hepmc4::particle<T>>> anc;
    particle->ancestors(anc);
    n += anc.size();
  }
  
  n--;
  return event->particles().size();
}
  
//--------------------------------------------------------------------
int
main(int argc, char** argv)
{
  return bench::driver<double>("cnt_anc",cnt_anc<double>,argc,argv);
}
//
// EOF
//


      
        
        
        
