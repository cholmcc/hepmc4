//
// Copyright (c) 2023 Christian Holm Christensen <cholmcc@gmail.com>
// Distributed under the GPL-3
//
#include <bench/driver>

//--------------------------------------------------------------------
/**
   Prune an event

   @tparam T     Floating point values type 
   @param event
   @return Number of operations
   @ingroup hepmc_bench_tests
*/
template <typename T>
int prune(hepmc4::event_ptr<T>& event)
{
  // The selection of particles
  auto select = [](const hepmc4::particle_ptr<T>& particle) {
    switch (particle->status) {
    case hepmc4::particle<T>::final_state: // Final st
    case hepmc4::particle<T>::decayed:     // Decayed
    case hepmc4::particle<T>::beam:        // Beam
      return true;
    }
    // Keep diffractive protons 
    // if (particle->pid() == 9902210) return true;
    return false;
  };

  // pruneEvent(event, select, 0);
  event->prune(select);
  return 1;
}
  
//--------------------------------------------------------------------
int
main(int argc, char** argv)
{  
  return bench::driver<double>("prune",prune<double>,argc,argv);
}
//
// EOF
//
