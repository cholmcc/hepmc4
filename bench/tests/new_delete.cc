//
// Copyright (c) 2023 Christian Holm Christensen <cholmcc@gmail.com>
// Distributed under the GPL-3
//
#include <bench/driver>

//--------------------------------------------------------------------
/**
   Create a copy of an event, and remove it again.

   @tparam T     Floating point values type 
   @param event
   @return Number of operations
   @ingroup hepmc_bench_tests
*/
template <typename T>
int new_delete(hepmc4::event_ptr<T>& event)
{
  auto copy = event->copy(); // new hepmc4::event<T>(*event);
  copy.reset();
  return 1;
}
  
//--------------------------------------------------------------------
int
main(int argc, char** argv)
{  
  return bench::driver<double>("new_delete",new_delete<double>,argc,argv);
}
//
// EOF
//


      
        
        
        
