//
// Copyright (c) 2023 Christian Holm Christensen <cholmcc@gmail.com>
// Distributed under the GPL-3
//
#include <bench/driver>

namespace bench {
  //__________________________________________________________________
  /**
     Read in one event, and output time it took to read 
     
     @tparam T     Floating point values type 
     @param reader  Reader of events
     @param event   Structure to read event into
     @param log     Log output
     @param enable  Enable timer 
     
     @return true on success
     @ingroup hepmc_bench_tests
  */
  template <typename T>
  bool timed_read(hepmc4::reader_ptr<T>& reader,
                  hepmc4::event_ptr<T>& event,
                  std::ostream& log,
                  bool enable=true)
  {
    timer t(log, 0, enable);

    bool ret = reader->read_event(event);

    t._n = event->particles().size();
    t._m = 1;
    return ret;
  }
  
  //------------------------------------------------------------------
  /**
     Loop over events and possibly time the reading. This does not
     execute any function on the event itself.
     
     @tparam T     Floating point values type 
     @param reader   Reader of data
     @param opts     Options
     @param log      Log stream
     @ingroup hepmc_bench_tests
  */
  template <typename T, typename F>
  typename std::enable_if<std::is_same<F, std::nullptr_t>::value, void>::type
  looper(hepmc4::reader_ptr<T>& reader,
         options&               opts,
         logger&                log,
         F                      )
  {
    using builder_type=hepmc4::builder<T>;
    auto event = builder_type::event();
    auto more  = [](int iev, int max) { return max <= 0 or iev < max; };

    int iev = 0;
    while (more(iev++, opts.maxEvents) and
           timed_read(reader, event, log, opts.timing)) {
      show(event, opts);
    }
  }
}

//--------------------------------------------------------------------
int
main(int argc, char** argv)
{
  return bench::driver<double>("read",nullptr,argc,argv);
}
//
// EOF
//


      
        
        
        
