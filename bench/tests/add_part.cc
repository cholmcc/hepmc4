//
// Copyright (c) 2023 Christian Holm Christensen <cholmcc@gmail.com>
// Distributed under the GPL-3
//
#include <bench/driver>

//--------------------------------------------------------------------
/**
   Add particles to an event

   @tparam T     Floating point values type 
   @param event
   @return Number of operations
   @ingroup hepmc_bench_tests
*/
template <typename T>
int add_part(hepmc4::event_ptr<T>& event)
{
  using builder_type=hepmc4::builder<T>;
  
  size_t n = 0;
  size_t m = 0;
  for (auto iter = event->vertices().begin();
       iter != event->vertices().end(); ++iter) {
    if (n++ % 5 == 0)  {
      (*iter)->add_outgoing(builder_type::particle());
      m++;
    }
  }
  return m;
}
  
//--------------------------------------------------------------------
int
main(int argc, char** argv)
{  
  return bench::driver<double>("add_part",add_part<double>,argc,argv);
}
//
// EOF
//


      
        
        
        
