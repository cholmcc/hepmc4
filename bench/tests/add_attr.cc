//
// Copyright (c) 2023 Christian Holm Christensen <cholmcc@gmail.com>
// Distributed under the GPL-3
//
#include <bench/driver>

/**
   @defgroup hepmc_bench_tests Specific benchmark tests
   @ingroup hepmc_bench

   The specific functions used for benchmarking.

   Each source file defines a separate test as a stand-alone program. 
   
   The tests are defined in terms of a single function that is called
   for each event (except `read.cc` and `write.cc` - more below) with
   a reference to the `HepMC3::GenEvent` object as the sole argument.
   
   The test programs then uses the `bench::driver` function to execute
   that test on each event by passing the test function to the
   `bench::driver` function.  The calls are optionally timed by a
   `bench::timer` and the result written to out put, together with the
   number of particles in the event.
    
   The single event test functions should do what ever operations on
   the event needed.  The functions are executed by the function
   template bench::looper.
    
    ## Read and write tests 
    
    These two tests `read.cc` and `write.cc` does not define a event
    specific test function.  Instead, the specialise the
    `bench::looper` template function on `nullptr`, and pass `nullptr`
    to `bench::driver`.
    
    - For `read.cc` it is done so that we may time reading in an
      event.

    - For `write.cc` it is done so we may declare the writer outside
      of the event loop.
      
    ## Available tests 
    
    
    - `add_attr.cc` Add attribute to all particles in event
      
    - `add_part.cc` Add a particle to every 5th vertex in event
    
    - `cnt_anc.cc` Count ancestors of all particles in event
    
    - `cnt_dec.cc` Count descendants of all particles in event
    
    - `new_delete.cc` Allocate copy of event and deallocate it
      
    - `print.cc` Print events
      
    - `read.cc` Read in events
      
    - `rem_part.cc` Remove every 10th particle from the event
      
      Note, this fails for `master` in certain input, but does not
      fail for `mine`
      
    - `rem_vtx.cc` Remove every 10th vertex from the event
    
      Note, this fails for `master` in certain input, but does not
      fail for `mine`
    
    - `rotate.cc` Rotate event
      
    - `write.cc` Write events.  If verbosity is 0, then the events are
      written to `/dev/null`, otherwise they are written to standard
      error.
  

*/
//--------------------------------------------------------------------
/**
   Add attribues to an event

   @tparam T     Floating point values type 
   @param event
   @return Number of operations
   @ingroup hepmc_bench_tests
*/
template <typename T>
int add_attr(hepmc4::event_ptr<T>& event)
{
  int i = 1;
  for (auto particle : event->particles())
    particle->add_attribute("some", 10+i++);

  return event->particles().size();
}
  
//--------------------------------------------------------------------
int
main(int argc, char** argv)
{
  
  return bench::driver<double>("add_attr",add_attr<double>,
                               argc,argv);
}
//
// EOF
//


      
        
        
        
