# Test data sets 

Datasets can be retrieved from 

- [https://rivetval.web.cern.ch/rivetval/HEPMC/](https://rivetval.web.cern.ch/rivetval/HEPMC/) 

They should be placed in this directory.

The top-level Makefile is geared to use the data sets 

- [`BFactory-10.45.hepmc`](https://rivetval.web.cern.ch/rivetval/HEPMC/BFactory-10.45.hepmc.gz)
- [`LEP-93.0.hepmc`](https://rivetval.web.cern.ch/rivetval/HEPMC/LEP-93.0.hepmc.gz)
- [`LHC-13-Minbias.hepmc`](https://rivetval.web.cern.ch/rivetval/HEPMC/LHC-13-Minbias.hepmc.gz)
- [`LHC-13-Top-All.hepmc`](https://rivetval.web.cern.ch/rivetval/HEPMC/LHC-13-Top-All.hepmc.gz)
- [`LHC-7-DiJets-2-C.hepmc`](https://rivetval.web.cern.ch/rivetval/HEPMC/LHC-7-DiJets-2-C.hepmc.gz)

These files are all gzip'ped HepMC Asciiv2 (aka `IO_GenEvent`)
formatted files. In principle, the HepMC4 library can transparently
read from gzip'ped input files if compiled with Boost `iostreams`
support. 

To gauge the improvements of the new Asciiv4 format, these files can
be converted into that format and used instead (set Make variable
`INPUT_FORMAT=hepmc4`). 


