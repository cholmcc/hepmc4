# Benchmark HepMC4 

This is bench-marking tools for HepMC4.  This is similar to the
[benchmark for HepMC3](https://gitlab.com/cholmcc/hepmc3_bench).

## Inputs 

Datasets can be retrieved from 

- [https://rivetval.web.cern.ch/rivetval/HEPMC/](https://rivetval.web.cern.ch/rivetval/HEPMC/) 

They should be placed in the sub-directory [`inputs`](inputs).

HepMC4 can read from gzip'ped data and from Asciiv2, Asciiv3, and
Asciiv4.  What to use as input for the bench-mark tests can be
specified by the Make variable `INPUT_FORMAT` (defaults to `hepmc`). 

| Format version | Asciiv2     | Asciiv3     | Asciiv4     |
|----------------|-------------|-------------|-------------|
| Un-compressed  | `hepmc`     | `hepmc3`    | `hepmc4`    |
|                | `hepmc2`    |             |             |
| Compressed     | `hepmc.gz`  | `hepmc3.gz` | `hepmc4.gz` |
|                | `hepmc2.gz` |             |             |

## Test data sets 

The top-level Makefile is geared to use the data sets 

- [`BFactory-10.45.hepmc`](https://rivetval.web.cern.ch/rivetval/HEPMC/BFactory-10.45.hepmc.gz)
- [`LEP-93.0.hepmc`](https://rivetval.web.cern.ch/rivetval/HEPMC/LEP-93.0.hepmc.gz)
- [`LHC-13-Minbias.hepmc`](https://rivetval.web.cern.ch/rivetval/HEPMC/LHC-13-Minbias.hepmc.gz)
- [`LHC-13-Top-All.hepmc`](https://rivetval.web.cern.ch/rivetval/HEPMC/LHC-13-Top-All.hepmc.gz)
- [`LHC-7-DiJets-2-C.hepmc`](https://rivetval.web.cern.ch/rivetval/HEPMC/LHC-7-DiJets-2-C.hepmc.gz)

These files are all gzip'ped HepMC Asciiv2 (aka `IO_GenEvent`)
formatted files. In principle, the HepMC4 library can transparently
read from gzip'ped input files if compiled with Boost `iostreams`
support. 

To gauge the improvements of the new Asciiv4 format, these files can
be converted into that format and used instead (set Make variable
`INPUT_FORMAT=hepmc4`). 


## Analysis 

The analysis of the results is done in a [Jupyter
Notebook](analysis/hepmc4.ipynb) in the sub-directory
[`analysis`](analysis).

## Results 

**TL;DR**: 

- <span style='color:#ff7f0e'>HepMC4</span> is faster at
  - Adding attributes
  - Adding particles 
  - Removing particles 
  - Removing vertices 
  - Pruning events 
- <span style='color:#1f77b4'>HepMC3</span> is faster at 
  - Copying event and free-ing it
  - Writing out data 
  - Reading in HepMC2 (`IO_GenEvent`) data
- In other cases, which one is faster depends a bit on the event
  structure. 

### BFactory-10.45

![](analysis/BFactory-10.45.png)

### LEP-93.0 

![LEP-93.0](analysis/LEP-93.0.png)

### LHC-13-Minbias

![LHC-13-Minbias](analysis/LHC-13-Minbias.png)

### LHC-13-Top-All 

![LHC-13-Top-All](analysis/LHC-13-Top-All.png)

### LHC-7-DiJets-2-C

![LHC-7-DiJets-2-C](analysis/LHC-7-DiJets-2-C.png)


### Read in various formats 

![read](analysis/read.png)
