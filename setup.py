#!/usr/bin/env python
#
#  Library for handling HEP MC data
#  Copyright (C) 2023  Christian Holm Christensen
#
#  This program is free software: you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public License
#  as published by the Free Software Foundation, either version 3 of
#  the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see
#  <https://www.gnu.org/licenses/>.
#                              
# Install editable with
#
#     pip install --user --break-system-packages --editable .
#
# Would like to get rid of the `--break-system-packages` option, but
# don't know how.
#
#
from pathlib import Path

from pybind11.setup_helpers import Pybind11Extension, build_ext
from setuptools import setup
from os import getenv
from re import sub

cxx_std = getenv("CXX_STANDARD")
if cxx_std:
    cxx_std = int(cxx_std.replace('c++','').replace('gnu',''))
else:
    cxx_std = 11

with open('.headers','r') as file:
    headers = [sub(r'[\t ]*([a-z_0-9][a-z0-9_/]*).*',
                   r'\1',
                   l.replace('HEADERS','').replace(':=','').strip())
               for l in file.readlines()]

headers += ['python/pystreambuf',
            '.headers']    
    
hepmc4_module = Pybind11Extension(
    'hepmc4',
    ['python/pyhepmc.cc'],
    depends=headers,
    include_dirs=['.'],
    cxx_std=cxx_std,
    # extra_compile_args=['-g','-O0'],
    # extra_link_args=[]
 )

setup(
    name='hepmc4',
    packages=['hepmc4'],
    ext_modules=[hepmc4_module],
    cmdclass={"build_ext": build_ext},
)
#
# EOF
#
