# Python interface to HepMC 

This directory defines the Python interface to HepMC4. 

The source file [`pyhepmc.cc`](pyhepmc.cc) contains the declaration of
the bindings using
[`pybind11`](https://pybind11.readthedocs.io/en/stable/).  This file
is hand-crafted so that we can make the interface more Pythonic than
what an automatised tool like
[`binder`](https://cppbinder.readthedocs.io/en/latest/) would do. 

The header file [`pystreambuf`](pystreambuf) defines a way to use
Python stream objects (`io.StringIO`, `io.TextIOWrapper`, or other
_file-like_ objects) to read from and write to.   This is accomplished
by wrapping a Python file-like object in a `std::streambuf` which we
then instantise a `std::istream` or `std::ostream` object with. 

## Examples 

- [`read.py`](read.py)  Reads in events and displays their content on
  standard output.  Mainly to illustrate the use of a reader 
  
- [`convert.py`](convert.py)  Reads in events from one source and
  writes them out on another source.  This effectively converts data
  from one format to another.  
  
  Note, if the output file name ends in `.dot`, then GraphViz graphs
  are written to the output file.  In this way, we can make
  illustrations of events. 
  
- [`pythia.py`](pythia.py)  Example of using
  [Pythia](https://pythia.org) and HepMC together in Python.   Also
  illustrates how to easily set up a specialised Pythia event
  generator. 
  
  There are two symbolic links to this script:
  
  - `angantyr.py`  When this is executed, then `pythia.py` will be run
    in "heavy-ion mode" (using the Angantyr) model. 
    
  - `lepz.py` When this is executed, it runs Pythia at LEP energies
    (near the Z) and produces Z events. 
    
  Note, that we can ask HepMC to prune events using the command line
  option `-P`.   For `pythia.py` and `angantyr.py` this will only keep 
  final-state (status=1), decayed (status=2), and beam (status=4)
  particles in the output.   For `lepz.py`, it will _also_ store the
  initial Z (status=21) and the two quarks produced (status=22). 
  
- [`w.py`](w.py) An illustration of making an event by-hand. 


