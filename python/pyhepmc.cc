//  Library for handling HEP MC data
//  Copyright (C) 2023  Christian Holm Christensen
//
//  This program is free software: you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation, either version 3 of
//  the License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see
//  <https://www.gnu.org/licenses/>.
//                              
/**
   @file      pyhepmc
   @copyright (c) 2023 Christian Holm Christensen
   @brief     General parameters and constants
   @ingroup   Bindings of hepmc4 to Python using pybind11

   The code in this file is entirely hand-crafted.  This allows us to
   set up the Python side of things in a more pythonic fashion.  For
   example

   - Class names are CamelCase, method names lower snake-case
   - Reader and Writer are context manages
   - Reader is iterable
   - Attributes are set and retrieved using indexing
   - ... 
   
*/
//
// See also
//
//    https://pybind11.readthedocs.io/en/stable/index.html
//
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <pybind11/stl_bind.h>
#include <pybind11/functional.h>
#include <hepmc4/builder>
#include <hepmc4/stream>
#include <hepmc4/database>
#include "pystreambuf"

namespace py = pybind11;

template <typename... Args>
using overload_cast_ = pybind11::detail::overload_cast_impl<Args...>;

using value_type=double;
using four_vector     = hepmc4::four_vector<value_type>;
using with_attributes = hepmc4::with_attributes;
using attribute_ptr   = hepmc4::attribute_ptr;
using attribute_base  = hepmc4::attribute_base;
template <typename T,
          template <typename> class Trait>
using attribute       = hepmc4::attribute<T,Trait>;
template <typename T>
using attribute_trait   = hepmc4::attribute_trait<T>;
using particle          = hepmc4::particle<value_type>;
using particle_ptr      = hepmc4::particle_ptr<value_type>;
using vertex            = hepmc4::vertex<value_type>;
using vertex_ptr        = hepmc4::vertex_ptr<value_type>;
using run_info          = hepmc4::run_info<value_type>;
using run_info_ptr      = hepmc4::run_info_ptr<value_type>;
using tools             = hepmc4::tools;
using weights           = hepmc4::weights<value_type>;
using event             = hepmc4::event<value_type>;
using event_ptr         = hepmc4::event_ptr<value_type>;
using units             = hepmc4::units;
using cross_section     = hepmc4::cross_section<value_type>;
using cross_section_ptr = hepmc4::cross_section_ptr<value_type>;
using pdf_info          = hepmc4::pdf_info<value_type>;
using pdf_info_ptr      = hepmc4::pdf_info_ptr<value_type>;
using heavy_ion         = hepmc4::heavy_ion<value_type>;
using heavy_ion_ptr     = hepmc4::heavy_ion_ptr<value_type>;
using builder           = hepmc4::builder<value_type>;
using flow_map          = typename particle::flow_map;
using polarity_type     = typename particle::polarity_type;
using particle_set      = typename particle::particle_set;
using writer            = hepmc4::stream::writer<value_type>;
using reader            = hepmc4::stream::reader<value_type>;
using value_vector      = std::vector<value_type>;
using name_map          = std::unordered_map<std::string,size_t>;

#ifdef WITH_PYTHIA8
#include <hepmc4/pythia8>
using pythia8           = hepmc4::pythia8::converter<value_type>;
#endif

PYBIND11_MAKE_OPAQUE(flow_map);
PYBIND11_MAKE_OPAQUE(value_vector);
PYBIND11_MAKE_OPAQUE(name_map);

namespace pybind11 {
  template <typename T,
            template <typename> class Trait=attribute_trait, 
            typename Holder=std::shared_ptr<attribute<T,Trait>>,
            typename ...Args>
  class_<attribute<T,Trait>,attribute_base,Holder>
  attr_(handle scope, std::string const& name, Args &&...args)
  {
    using attribute_type=attribute<T,Trait>;
    class_<attribute_type,attribute_base,Holder>
      cl(scope,name.c_str(),
         std::forward<Args>(args)...);
    cl
      .def(py::init<const T&>(),
           py::arg("value")=T(),
           "Construct attribute from value")
      .def(py::init<const std::shared_ptr<attribute<std::string,
           attribute_trait>>&>(),
           py::arg("other"),
           "Construct from string attribute")
      .def_property("value",
                    overload_cast_<>()(&attribute_type::value),
                    [](attribute_type& a, const T& v) {
                      a.value() = v;
                    },
                    "Get or set value of attribute")
      ;
    implicitly_convertible<T,attribute_type>();
    return cl;
  }
}

      

PYBIND11_MODULE(hepmc4, m) {
  m.doc() = "HepMC4 python bindings";
    
  py::bind_map<std::map<int,int>>(m, "FlowMap", py::module_local(true));
  py::bind_vector<value_vector>(m, "ValueVector", py::module_local(true));
  // py::bind_map<name_map>(m, "weight_name_map", py::module_local(false));
    
  // --- Four vector -----------------------------------------------
  py::class_<four_vector>(m,"FourVector")
    .def(py::init<value_type,value_type,value_type,value_type>(),
         py::arg("x")=0,
         py::arg("y")=0,
         py::arg("z")=0,
         py::arg("t")=0)
    .def("x",
         static_cast<value_type& (four_vector::*)()>(&four_vector::x),
         "Access X coordinate")
    .def("x",
         static_cast<const value_type& (four_vector::*)() const>
         (&four_vector::x),
         "Access X coordinate")
    .def("y",
         static_cast<value_type& (four_vector::*)()>(&four_vector::y),
         "Access Y coordinate")
    .def("y",
         static_cast<const value_type& (four_vector::*)() const>
         (&four_vector::y),
         "Access Y coordinate")
    .def("z",
         static_cast<value_type& (four_vector::*)()>(&four_vector::z),
         "Access Z coordinate")
    .def("z",
         static_cast<const value_type& (four_vector::*)() const>
         (&four_vector::z),
         "Access Z coordinate")
    .def("t",
         static_cast<value_type& (four_vector::*)()>(&four_vector::t),
         "Access T coordinate")
    .def("t",
         static_cast<const value_type& (four_vector::*)() const>
         (&four_vector::t),
         "Access T coordinate")
    .def("square_length",
         &four_vector::square_length,
         "Square length of 3-part")
    .def("length",
         &four_vector::length,
         "Length of 3-part")
    .def("square_perpendicular",
         &four_vector::square_perpendicular,
         "Square perpendicular of 3-part")
    .def("perpendicular",
         &four_vector::perpendicular,
         "Perpendicular of 3-part")
    .def("square_invariant",
         &four_vector::square_invariant,
         "Square of invariant (mass)")
    .def("invariant",
         &four_vector::invariant,
         "Invariant (mass)")
    .def("pseudorapidity",
         &four_vector::pseudorapidity,
         "Pseudorapidity")
    .def("rapidity",
         &four_vector::rapidity,
         "Rapidity")
    .def("polar",
         &four_vector::polar,
         "Polar")
    .def("azimuthal",
         &four_vector::azimuthal,
         "Azimuthal")
    .def("factor",
         &four_vector::factor,
         "Factor")
    .def("is_zero",
         &four_vector::is_zero,
         "Is Zero?")
    .def("rotate",
         overload_cast_<const value_type&,const value_type&,
         const value_type&>()(&four_vector::rotate),
         py::arg("theta"), py::arg("phi"), py::arg("psi"),
         "Rotate around three axis")
    .def("boost",
         &four_vector::boost,
         "Boost along other four-vector")
    .def("__getitem__",
         [](const four_vector& v,size_t i)->value_type {
           return v[i]; },
         "Retrive the ith coordinate")
    .def("__setitem__",
         [](four_vector& v,size_t i,value_type u)->value_type {
           return v[i] = u; },
         "Set and retrive the ith coordinate")
    .def("__str__",
         [](const four_vector& v) {
           std::stringstream s; s << v; return s.str(); })
    ;

  // --- Attribute base -----------------------------------------
  py::class_<attribute_base,attribute_ptr>(m,"AttributeBase")
    .def("format",
         &attribute_base::format,
         "Format attribute")
    .def("parse",
         &attribute_base::parse,
         "Parse attribute")
    .def("__str__",
         &attribute_base::to_string,
         "String representation")
    .def("type_name",
         &attribute_base::type_name,
         "Get raw attribute value type")
    .def("reset",
         &attribute_base::reset,
         "Reset attribute to default value")
    .def_readwrite("persistent",
                   &attribute_base::persistent,
                   "Peristency flag")
    ;
      
  // --- With attributes ----------------------------------------
  py::class_<with_attributes,
             std::shared_ptr<with_attributes>>(m,"WithAttributes")
    .def("has_attribute",
         &with_attributes::has_attribute,
         "Check if this has a specific attribute")
    .def("__contains__",
         &with_attributes::has_attribute,
         "Check if this has a specific attribute")
    .def("__getitem__",
         [](const with_attributes& wa, const std::string& key) {
           // auto a = wa.attribute(key);
           // if (a) return a->to_string();
           // return nullptr;
           return wa.attribute(key);
         },
         "Get attribute")
    .def("__setitem__",
         [](with_attributes& wa,
            const std::string& key,
            const std::string& value) {
           wa.add_attribute(key,value); },
         "Set attribute by string value",
         py::keep_alive<1,3>() /* Keep value alive */)
    .def("__setitem__",
         [](with_attributes& wa,
            const std::string& key,
            const attribute_ptr& value) {
           wa.add_attribute(key,value); },
         "Set attribute by string value",
         py::keep_alive<1,3>() /* Keep value alive */)
    .def("__iter__",
         [](with_attributes& wa) {
           return py::make_key_iterator<
             py::return_value_policy::reference_internal>
             (wa.attributes().begin(), wa.attributes().end()); },
         "Get iterator over attributes")
    ;

  // --- Specific attributes ------------------------------------
  py::attr_<bool>           (m,"BoolAttribute");
  py::attr_<char>           (m,"CharAttribute");
  py::attr_<short>          (m,"ShortAttribute");
  py::attr_<int>            (m,"IntAttribute");
  py::attr_<long>           (m,"LongAttribute");
  py::attr_<unsigned short> (m,"UshortAttribute");
  py::attr_<unsigned int>   (m,"UIntAttribute");
  py::attr_<unsigned long>  (m,"ULongAttribute");
  py::attr_<unsigned char>  (m,"UCharAttribute");
  py::attr_<float>          (m,"FloatAttribute");
  py::attr_<double>         (m,"DoubleAttribute");
  py::attr_<std::string>    (m,"StringAttribute");
  py::attr_<flow_map>       (m,"FlowAttribute");
  py::attr_<polarity_type>  (m,"PolarityAttribute");
    
    
  // --- Particle -----------------------------------------------
  py::class_<particle,
             with_attributes,
             particle_ptr> part(m,"Particle");
  part
    .def(py::init<value_type,value_type,value_type,value_type,
         int,int,value_type>(),
         py::arg("px")=0,
         py::arg("py")=0,
         py::arg("pz")=0,
         py::arg("e")=0,
         py::arg("pid")=0,
         py::arg("status")=0,
         py::arg("mass")=0
         )
    .def(py::init<four_vector,int,int,value_type>(),
         py::arg("momentum"),
         py::arg("pid")=0,
         py::arg("status")=0,
         py::arg("mass")=0)
    .def_readwrite("momentum",
                   &particle::momentum,
                   "Particle four momentum")
    .def_readwrite("status",
                   &particle::status,
                   "Particle status")
    .def_readwrite("pid",
                   &particle::pid,
                   "Particle identity (PDG code)")
    .def_readwrite("mass",
                   &particle::mass,
                   "Generated mass")
    .def("production",
         &particle::production,
         "Get the production vertex, if any")
    .def("termination",
         &particle::termination,
         "Get the termination vertex, if any")
    .def("clear",
         &particle::clear,
         "Clear particle")
    .def_property("flows",
                  static_cast<const flow_map&(particle::*)(const flow_map&) const>
                  (&particle::flows),
                  // overload_cast_<>()(&particle::flows),
                  [](particle& p, const flow_map& m) {
                    p.flows() = m;
                  },
                  "Flow attributes")
    .def_property("polarity",
                  static_cast<const polarity_type&(particle::*)(const polarity_type&) const>
                  (&particle::polarity),
                  [](particle& p, const polarity_type& m) {
                    p.polarity() = m;
                  },
                  "Polarity attribute")
    .def("ancestors",
         overload_cast_<>()(&particle::ancestors),
         "Get ancestors of the particle")
    .def("descendants",
         overload_cast_<>()(&particle::descendants),
         "Get descendants of the particle")
    .def("__str__",
         [](const particle& v) {
           std::stringstream s; s << v; return s.str(); })
    ;
      
  // --- Particle status -------------------------------------------
  py::enum_<particle::status_values>(part,"StatusValues")
    .value("final_state",particle::final_state)
    .value("decayed",particle::decayed)
    .value("history",particle::history)
    .value("beam",particle::beam)
    ;
    
  // --- Vertex -----------------------------------------------
  py::class_<vertex,
             with_attributes,
             vertex_ptr>(m,"Vertex")
    .def(py::init<value_type,value_type,value_type,value_type,int>(),
         py::arg("vx")=0,
         py::arg("vy")=0,
         py::arg("vz")=0,
         py::arg("vt")=0,
         py::arg("status")=0
         )
    .def(py::init<four_vector,int>(),
         py::arg("position"),
         py::arg("status")=0)
    .def_readwrite("position",
                   &vertex::position,
                   "Vertex four position")
    .def_readwrite("status",
                   &vertex::status,
                   "Vertex status")
    .def("incoming",
         &vertex::incoming,
         "Get the incoming particles, if any")
    .def("outgoing",
         &vertex::outgoing,
         "Get the outgoing particles, if any")
    .def("add_incoming",
         &vertex::add_incoming,
         "Add an incoming particle",
         py::keep_alive<1,2>() /* Keep particle alive */)
    .def("add_outgoing",
         &vertex::add_outgoing,
         "Add outgoing particle",
         py::keep_alive<1,2>() /* Keep particle alive */)
    .def("remove_incoming",
         &vertex::remove_incoming,
         "Remove an incoming particle")
    .def("remove_outgoing",
         &vertex::remove_outgoing,
         "Remove outgoing particle")
    .def("clear",
         &vertex::clear,
         "Clear vertex")
    .def("ancestors",
         overload_cast_<>()(&vertex::ancestors),
         "Get ancestors of the vertex")
    .def("descendants",
         overload_cast_<>()(&vertex::descendants),
         "Get descendants of the vertex")
    .def("__str__",
         [](const vertex& v) {
           std::stringstream s; s << v; return s.str(); })
    ;

  // --- Tools -----------------------------------------------
  py::class_<tools,
             std::shared_ptr<tools>> tls(m,"Tools");
  tls
    .def(py::init<>(),
         "Tools information")
    .def("__getitem__",
         [](const tools& t,const std::string& n) { return t[n]; },
         "Get tool information")
    .def("__setitem__",
         [](tools& t, const std::string& n, const tools::info& i) {
           t[n] = i; },
         py::arg("name"),
         py::arg("info"),
         "Set tool information")
    .def("__setitem__",
         [](tools& t, const std::string& n,
            const std::pair<std::string,std::string>& i) {
           tools::info info;
           info.version = i.first;
           info.description = i.second;
           t[n] = info; },
         py::arg("name"),
         py::arg("version_description"),
         "Set tool information")
    .def("__delitem__",
         &tools::erase,
         py::arg("name"),
         "Remove a tool")
    .def("__iter__", 
         [](tools& t) {
           return py::make_key_iterator<
             py::return_value_policy::reference_internal>
             (t.infos().begin(), t.infos().end()); },
         "Get iterator over tools")
    .def("__len__",
         &tools::size,
         "Get number of tools")
    .def("__contains__",
         [](const tools& t,const std::string& n) { return t.has(n); },
         py::arg("name"),
         "Check if this has a specific tool")
    .def("__contains__",
         [](const tools& t,const std::pair<std::string,std::string>& nv) {
           return t.has(nv.first,nv.second);
         },           
         py::arg("name_version"),
         "Check if this has a specific version of a tool")
    .def("clear",
         &tools::clear,
         "Clear this")
    .def("__str__",
         [](const tools& v) {
           std::stringstream s; s << v; return s.str(); })
    ;

  // --- Tools information -----------------------------------------
  py::class_<tools::info,
             std::shared_ptr<tools::info>>(tls,"Info")
    .def(py::init([](const std::string& v,const std::string& d) {
      auto ret = new tools::info;
      ret->version = v;
      ret->description = d;
      return ret; }),
      py::arg("version")="",
      py::arg("description")="",
      "Construct tool information")
    .def_readwrite("version",
                   &tools::info::version,
                   "Tool version number")
    .def_readwrite("description",
                   &tools::info::description,
                   "Tool description")
    .def("__str__",
         [](const tools::info& v) {
           std::stringstream s;
           s << std::quoted(v.version) << " "
             << std::quoted(v.description);
           return s.str(); })
    ;

  // --- Weights ---------------------------------------------------
  py::class_<weights,
             std::shared_ptr<weights>>(m,"Weights")
    .def(py::init<size_t>(),
         py::arg("n")=1,
         "Construct with number of weights")
    .def(py::init<>([](const py::iterable& n) -> weights {
      std::vector<std::string> nn;
      nn.reserve(py::len_hint(n));
      for (auto h : n) nn.push_back(h.cast<std::string>());
      return weights(nn);}))
    .def(py::init<std::initializer_list<std::string>&>(),
         py::arg("weight_names"),
         "Specify names of weights")
    .def("__getitem__",
         [](const weights& w,const std::string& n) { return w[n]; },
         py::arg("name"),
         "Get tool information")
    .def("__getitem__",
         [](const weights& w,size_t n) { return w[n]; },
         py::arg("index"),
         "Get tool information")
    .def("__setitem__",
         [](weights& w, const std::string& n, value_type& v) { w[n] = v; },
         py::arg("name"),
         py::arg("value"),
         "Set tool information")
    .def("__setitem__",
         [](weights& w, size_t& i, value_type& v) { w[i] = v; },
         py::arg("index"),
         py::arg("value"),
         "Set tool information")
    .def("__iter__", 
         [](weights& w) {
           return py::make_iterator<
             py::return_value_policy::reference_internal>
             (w.values().begin(), w.values().end()); },
         "Get iterator over weights")
    .def("__len__",
         &weights::size,
         "Get number of weights")
    .def("__contains__",
         [](const weights& t,const std::string& n) {
           return t.has(n);
         },
         py::arg("name"),
         "Check if this has a specific tool")
    .def("values",
         &weights::values,
         "Get values container")
    .def("names",
         &weights::names,
         "Get name to index mapping")
    .def("are_named",
         &weights::are_named,
         "Check if weight names are defined")
    .def("clear",
         &weights::clear,
         "Clear this")
    .def("add_weight",
         overload_cast_<const value_type&>()(&weights::add_weight),
         py::arg("value"),
         "Add a weight")
    .def("add_weight",
         overload_cast_<const std::string&,const value_type&>()
         (&weights::add_weight),
         py::arg("name"),
         py::arg("value"),
         "Add a weight")
    .def("name_index",
         &weights::name_index,
         py::arg("name"),
         "Get index of named weight")
    .def("__str__",
         [](const weights& v) {
           std::stringstream s; s << v; return s.str(); })
    ;
      
  // --- Run_Info --------------------------------------------------
  py::class_<run_info,
             with_attributes,
             run_info_ptr>(m,"RunInfo")
    .def(py::init<int>(),
         py::arg("nweights")=1,
         "Specify number of weights")
    .def(py::init<>([](const py::iterable& n) -> run_info {
      std::vector<std::string> nn;
      nn.reserve(py::len_hint(n));
      for (auto h : n) nn.push_back(h.cast<std::string>());
      return run_info(nn);}))
    .def(py::init<std::initializer_list<std::string>&>(),
         py::arg("weight_names"),
         "Specify names of weights")
    .def("clear",
         &run_info::clear,
         "Reset values")
    .def_property("tools",
                  static_cast<const tools&(run_info::*)() const>
                  (&run_info::tools),
                  [](run_info& ri, const tools& t) {
                    ri.tools() = t; },
                  "Defined tools")
    .def_property("weights",
                  static_cast<const weights&(run_info::*)() const>
                  (&run_info::weights),
                  [](run_info& ri, const weights& t) {
                    ri.weights() = t; },
                  "Defined weights")
    .def("__str__",
         [](const run_info& v) {
           std::stringstream s; s << v; return s.str(); })
    ;

  // --- Cross_section ---------------------------------------------
  py::class_<cross_section,cross_section_ptr>(m,"CrossSection")
    .def(py::init<>(), "Cross-section information")
    .def_readwrite("n_accepted",&cross_section::n_accepted,
                   "Number of accepted events")
    .def_readwrite("n_attempted",&cross_section::n_attempted,
                   "Number of attempted events")
    .def_readwrite("values",&cross_section::values,
                   "Values of cross-sections")
    .def_readwrite("uncertainties",&cross_section::uncertainties,
                   "Uncertainties of cross-sections")
    .def("clear",&cross_section::clear,
         "Clear cross-sections")
    .def("__len__",[](const cross_section& cs) { return cs.values.size();},
         "Number of cross-sections")
    .def("__str__",
         [](const cross_section& cs) {
           std::stringstream s; s << cs; return s.str(); })
    ;
  py::attr_<cross_section_ptr>  (m,"CrossSectionAttribute");
  // --- pdf_info ---------------------------------------------
  py::class_<pdf_info,pdf_info_ptr>(m,"PdfInfo")
    .def(py::init<>(), "PDF information")
    .def_readwrite("parton_id",&pdf_info::parton_id,
                   "Particle IDs (PDG codes) of partons")
    .def_readwrite("pdf_id",&pdf_info::pdf_id,
                   "Parton distribution function identifiers")
    .def_readwrite("x",&pdf_info::x,
                   "Parton momentum fractions")
    .def_readwrite("xf",&pdf_info::xf,
                   "x*f(x)")
    .def_readwrite("scale",&pdf_info::scale,  "Event scale")
    .def("clear",&pdf_info::clear,
         "Clear pdf info")
    .def("__str__",
         [](const pdf_info& cs) {
           std::stringstream s; s << cs; return s.str(); })
    ;
  py::attr_<pdf_info_ptr>  (m,"PdfInfoAttribute");

  // --- heavy_ion ---------------------------------------------
  py::class_<heavy_ion,heavy_ion_ptr>(m,"HeavyIon")
    .def(py::init<>(), "Havy ion information")
    .def_readwrite("n_coll_hard",
                   &heavy_ion::n_coll_hard,
                   "Number of hard nucleon-nucleon collisions")
    .def_readwrite("n_participants_projectile",
                   &heavy_ion::n_participants_projectile,
                   "Number of projectile nucleons participating "
                   "in collisions")
    .def_readwrite("n_participants_target",
                   &heavy_ion::n_participants_target,
                   "Number of target nucleons participating in collisions")
    .def_readwrite("n_coll",
                   &heavy_ion::n_coll,
                   "Number of nucleon-nucleon collisions")
    .def_readwrite("n_coll_n_wounded",
                   &heavy_ion::n_coll_n_wounded,
                   "Number of single diffractive nucleon-nucleon collisions "
                   "where target nucleon is excited")
    .def_readwrite("n_coll_wounded_n",
                   &heavy_ion::n_coll_wounded_n,
                   "Number of single diffractive nucleon-nucleon collisions "
                   "where projectile nucleon is excited")
    .def_readwrite("n_coll_wounded_wounded",
                   &heavy_ion::n_coll_wounded_wounded,
                   "Number of double diffractive nucleon-nucleon collisions "
                   "where both nucleons are excited")
    .def_readwrite("impact_parameter",
                   &heavy_ion::impact_parameter,
                   "Impact parameter")
    .def_readwrite("event_plane_angle",
                   &heavy_ion::event_plane_angle,
                   "Event plane angle (radians)")
    .def_readwrite("sigma_inel_nn",
                   &heavy_ion::sigma_inel_nn,
                   "Nucleon-nucleon inelastic cross-section in mili-barn")
    .def_readwrite("centrality",
                   &heavy_ion::centrality,
                   "Centrality estimate (in %) from input")
    .def_readwrite("user_centrality",
                   &heavy_ion::user_centrality,
                   "User supplied centrality")
    .def_readwrite("n_neutron_spectators_projectile",
                   &heavy_ion::n_neutron_spectators_projectile,
                   "Number of projectile spectator (not colliding) neutrons")
    .def_readwrite("n_neutron_spectators_target",
                   &heavy_ion::n_neutron_spectators_target,
                   "Number of target spectator (not colliding) neutrons")
    .def_readwrite("n_proton_spectators_projectile",
                   &heavy_ion::n_proton_spectators_projectile,
                   "Number of projectile spectator (not colliding) protons")
    .def_readwrite("n_proton_spectators_target",
                   &heavy_ion::n_proton_spectators_target,
                   "Number of target spectator (not colliding) protons")
    .def_readwrite("participant_plane_angles",
                   &heavy_ion::participant_plane_angles,
                   "Participant plane angles")
    .def_readwrite("eccentricities",
                   &heavy_ion::eccentricities,
                   "Eccentricities")
    .def_readwrite("format_old",
                   &heavy_ion::format_old,
                   "Whether for trait to format as old")
    .def("clear",&heavy_ion::clear,
         "Clear heavy-ion")
    .def("__str__",
         [](const heavy_ion& cs) {
           std::stringstream s; s << cs; return s.str(); })
    ;
  py::attr_<heavy_ion_ptr>  (m,"HeavyIonAttribute");
    
    
  // --- Units -----------------------------------------------
  py::class_<units,std::shared_ptr<units>> unt(m,"Units");
  unt
    .def_readwrite("length",&units::length)
    .def_readwrite("momentum",&units::momentum)
    .def("parse",&units::parse)
    .def("format",&units::format)
    ;
  py::enum_<units::length_type>(unt,"LengthType")
    .value("milimeter",units::length_type::milimeter)
    .value("centimeter",units::length_type::centimeter)
    ;
  py::enum_<units::momentum_type>(unt,"MomentumType")
    .value("giga_electron_volt",units::momentum_type::giga_electron_volt)
    .value("mega_electron_volt",units::momentum_type::mega_electron_volt)
    ;
    
    
  // --- Event -----------------------------------------------
  py::class_<event,
             with_attributes,
             event_ptr>(m,"Event")
    .def(py::init<int,const run_info_ptr&>(),
         py::arg("number"),
         py::arg("run_info")=nullptr)
    .def(py::init<int>(),
         py::arg("number"))
    .def_readwrite("number",
                   &event::number,
                   "Event number")
    .def_readwrite("units",
                   &event::units,
                   "Event units")
    .def_property("position",
                  static_cast<const four_vector&(event::*)() const>
                  (&event::position),
                  [](event& e, const four_vector& v) {
                    e.position() = v; },
                  "Event four position")
    .def_property("run_info",
                  static_cast<const run_info_ptr&(event::*)() const>
                  (&event::run_info),
                  [](event& e, const run_info_ptr& v){
                    e.run_info() = v; },
                  "Event run_info")
    .def_property("cross_section",
                  static_cast<const cross_section_ptr&(event::*)() const>
                  (&event::cross_section),
                  [](event& e, const cross_section_ptr& v){
                    e.cross_section() = v; },
                  "Event cross_section")
    .def_property("pdf_info",
                  static_cast<const pdf_info_ptr&(event::*)() const>
                  (&event::pdf_info),
                  [](event& e, const pdf_info_ptr& v){
                    e.pdf_info() = v; },
                  "Event pdf_info")
    .def_property("heavy_ion",
                  static_cast<const heavy_ion_ptr&(event::*)() const>
                  (&event::heavy_ion),
                  [](event& e, const heavy_ion_ptr& v){
                    e.heavy_ion() = v; },
                  "Event heavy_ion")
    .def("particles",
         &event::particles,
         "Get particles")
    .def("add_particle",
         &event::add_particle,
         py::arg("particle"),
         "Add a particle to the event",
         py::keep_alive<1,2>() /* Keep particle alive */)
    .def("remove_particle",
         &event::remove_particle,
         py::arg("particle"),
         "Remove a particle from the event")
    .def("vertices",
         &event::vertices,
         "Get vertices")
    .def("add_vertex",
         &event::add_vertex,
         py::arg("vertex"),
         "Add a vertex to the event",
         py::keep_alive<1,2>() /* Keep vertex alive */)
    .def("remove_vertex",
         &event::remove_vertex,
         py::arg("vertex"),
         "Remove a vertex from the event")
    .def("beams",
         &event::beams,
         "Get beam particles")
    .def("clear",
         &event::clear,
         "Clear event")
    .def("rotate",
         &event::rotate,
         py::arg("theta"),
         py::arg("phi"),
         py::arg("psi"),
         "Rotate event")
    .def("boost",
         &event::boost,
         py::arg("delta"),
         "Boost event")
    .def("shift",
         &event::shift,
         py::arg("delta"),
         "Shift event")
    .def("reflect",
         &event::reflect,
         py::arg("axis"),
         "Reflect event around axis")
    .def("check_cycle",
         &event::check_cycle,
         "Check for cycles in event")
    .def("prune",
         [](event& e, const std::function<bool(const particle_ptr&)>& s) {
           e.prune(s);},
         py::arg("select"),
         "Prune event of particles not selected by select")
    .def("__str__",
         [](const event& e) {
           std::stringstream s; s << e; return s.str(); })
    ;

  // --- Reader -----------------------------------------------
  py::class_<reader,std::shared_ptr<reader>>(m,"Reader")
    .def(py::init<const std::string&>(),
         "Create a reader that reads from a named file",
         py::arg("filename"))
    .def(py::init<const std::string&,pystream::istream&>(),
         "Create a reader that either reads from a named file "
         "or from a stream.  If filename is empty or '-', then "
         "read from stream (e.g., sys.stdin)",
         py::arg("filename"),
         py::arg("stream"),
         py::keep_alive<1,3>() // Keep wrapped stream alive 
         )
    .def("read_event",
         &reader::read_event,
         py::arg("event"),
         "Read in one event")
    .def("skip_event",
         &reader::skip_event,
         "Skip over one event")
    .def("close",
         &reader::close,
         "Close input")
    .def("__enter__",
         [](reader& r) -> reader& { return r; },
         "Enter context")
    .def("__exit__",
         [](reader& r,
            py::object /* exc_type*/,
            py::object /* exc_val*/,
            py::object /* tb*/) { r.close(); },
         "Exit context")
    .def("__iter__",
         [](reader& r) -> reader& { return r; },
         "Iterate over input events")
    .def("__next__",
         [](reader& r) -> event_ptr {
           auto e = r.next();
           if (not e) throw py::stop_iteration();
           return e;
         },
         "Get next event")
    ;

  // --- Writer -----------------------------------------------
  py::class_<writer, std::shared_ptr<writer>>(m,"Writer")
    .def(py::init<const std::string&>(),
         "Create a writer that writes to a named file",
         py::arg("filename"))
    .def(py::init<const std::string&,pystream::ostream&>(),
         "Create a writer that either writes to a named file "
         "or to a stream. If filename is empty or '-', then "
         "write to stream (e.g., sys.stdout)",
         py::arg("filename"),
         py::arg("stream"),
         py::keep_alive<1,3>() // Keep wrapped stream alive
         )
    .def("write_event",
         &writer::write_event,
         py::arg("event"),
         "Write out one event")
    .def("next",
         &writer::next,
         "Write own event to output")
    .def("close",
         &writer::close,
         "Close input")
    .def("__enter__",
         [](writer& r) -> writer&{ return r; },
         "Enter context")
    .def("__exit__",
         [](writer& r,
            py::object /*exc_type*/,
            py::object /*exc_val*/,
            py::object /*tb*/) { r.close(); },
         "Exit context")
    ;

  py::class_<builder>(m,"Builder")
    .def_static("four_vector",
                [](value_type x,
                   value_type y,
                   value_type z,
                   value_type t)->four_vector {
                  return builder::four_vector(x,y,z,t); })
    .def_static("particle",
                [](const four_vector& v,
                   int pid,
                   int status,
                   value_type mass) -> particle_ptr {
                  return builder::particle(v,pid,status,mass); },
                py::arg("momentum"),
                py::arg("pid")=0,
                py::arg("status")=0,
                py::arg("mass")=0,
                "Create a particle")
    .def_static("particle",
                [](const value_type& px,
                   const value_type& py,
                   const value_type& pz,
                   const value_type& e,
                   int pid,
                   int status,
                   value_type mass) -> particle_ptr {
                  return builder::particle(px,py,pz,e,pid,status,mass); },
                py::arg("px")=0,
                py::arg("py")=0,
                py::arg("pz")=0,
                py::arg("e")=0,
                py::arg("pid")=0,
                py::arg("status")=0,
                py::arg("mass")=0,
                "Create a particle")
    .def_static("vertex",
                [](const four_vector& v,
                   int status) -> vertex_ptr {
                  return builder::vertex(v,status); },
                py::arg("position"),
                py::arg("status")=0,
                "Create a vertex")
    .def_static("vertex",
                [](const value_type& x,
                   const value_type& y,
                   const value_type& z,
                   const value_type& t,
                   int status) -> vertex_ptr {
                  return builder::vertex(x,y,z,t,status); },
                py::arg("x")=0,
                py::arg("y")=0,
                py::arg("z")=0,
                py::arg("t")=0,
                py::arg("status")=0,
                "Create a vertex")
    .def_static("run_info",
                [](int n) -> run_info_ptr {
                  return builder::run_info(n); },
                py::arg("nweights")=1,
                "Create a run_info object with a number of weights")
    .def_static("run_info",
                [](const py::iterable& n) -> run_info_ptr {
                  std::vector<std::string> nn;
                  nn.reserve(py::len_hint(n));
                  for (auto h : n) nn.push_back(h.cast<std::string>());
                  return builder::run_info(nn); },
                py::arg("weight_names"),
                "Create a run_info object with named weights")
    .def_static("cross_section",
                []() -> cross_section_ptr {
                  return builder::cross_section(); },
                "Create cross-section object")
    .def_static("pdf_info",
                []() -> pdf_info_ptr {
                  return builder::pdf_info(); },
                "Create PDF info object")
    .def_static("heavy_ion",
                []() -> heavy_ion_ptr {
                  return builder::heavy_ion(); },
                "Create heavy ion object")
    .def_static("event",
                [](int no, const run_info_ptr& ri) -> event_ptr {
                  return builder::event(no,ri); },
                "Create event object",
                py::arg("number")=0,
                py::arg("run_info")=nullptr)
    .def_static("event",
                [](const event& other) -> event_ptr {
                  return builder::event(other); },
                "Create deep copy of an event object")
    .def_static("decay",
                [](const particle_ptr& decayed,
                   const py::iterable& prods) ->vertex_ptr  {
                  auto vtx = builder::vertex();
                  decayed->status = 2;
                  vtx->add_incoming(decayed);
                  for (auto h : prods) {
                    auto o = h.cast<particle_ptr>();
                    vtx->add_outgoing(o);
                    o->status = 1;
                  }
                  return vtx;
                },
                py::arg("decayed"),
                py::arg("products")
                )
    .def_static("interaction",
                [](const py::iterable& incoming,
                   const py::iterable& outgoing,
                   vertex_ptr vtx) ->vertex_ptr  {
                  if (not vtx)
                    vtx = builder::vertex();
                  for (auto h : incoming) {
                    auto i = h.cast<particle_ptr>();
                    vtx->add_incoming(i);
                    if (i->status == 1) i->status = 3;
                  }
                  for (auto h : outgoing) {
                    auto o = h.cast<particle_ptr>();
                    vtx->add_outgoing(o);
                    o->status = 1;
                  }
                  return vtx; },
                py::arg("incoming"),
                py::arg("outgoing"),
                py::arg("vertex")=nullptr,
                "Create an interaction vertex")
    ;

  auto database = m.def_submodule("database",
                                  "Sub-module of particle database");
  database.attr("db")      = hepmc4::database::db;
  database.attr("aliases") = hepmc4::database::aliases;
  database.attr("proton")  = py::int_(int(hepmc4::database::proton));
  database.attr("neutron") = py::int_(int(hepmc4::database::neutron));
  database.attr("pi")      = py::int_(int(hepmc4::database::pi));
  database.attr("pi0")     = py::int_(int(hepmc4::database::pi0));
  database.attr("K")       = py::int_(int(hepmc4::database::K));
  database.attr("K0")      = py::int_(int(hepmc4::database::K0));
  database.attr("K0_S")    = py::int_(int(hepmc4::database::K0_S));
  database.attr("K0_L")    = py::int_(int(hepmc4::database::K0_L));

  database.def("find",&hepmc4::database::find,
               py::arg("pdg"),
               "Find information about a particle type given its PDG code. "
               "Throws an exception in case the particle couldn't be found.");
  database.def("name",&hepmc4::database::name,
               py::arg("pdg"),
               "Get the name of a particle type, given its PDG code. "
               "Returns the empty string if it couldn't be found.");
  database.def("mass",&hepmc4::database::mass,
               py::arg("pdg"),
               "Get the mass - in GeV -  of a particle type, given its "
               "PDG code. Returns -1 if it couldn't be found.");
  database.def("charge",&hepmc4::database::charge,
               py::arg("pdg"),
               "Get the charge - in e/3, i.e., unit-quark charges - "
               "of a particle ype, given its"
               "PDG code. Returns 0 if it couldn't be found.");
  database.def("echarge",&hepmc4::database::echarge,
               py::arg("pdg"),
               "Get the charge - in e, i.e., unit-electron charges - "
               "of a particle ype, given its"
               "PDG code. Returns 0 if it couldn't be found.");
  database.def("latex",&hepmc4::database::latex,
               py::arg("pdg"),
               "Get the LaTeX formatted name of a particle type, "
               "given its PDG code. "
               "Returns the empty string if it couldn't be found.");
  database.def("html",&hepmc4::database::html,
               py::arg("pdg"),
               "Get the HTML formatted name of a particle type, "
               "given its PDG code. "
               "Returns the empty string if it couldn't be found.");
  database.def("pdg",&hepmc4::database::pdg,
               py::arg("name"),
               "Try to find the PDG of a particle type given its name. "
               "If the particle couldn't be found, return 0");
  

#ifdef WITH_PYTHIA8
  auto pythia = m.def_submodule("pythia8",
                                "Sub-module for Pythia8 converter");
  try {
    py::module_::import("pythia8");
    py::class_<pythia8,std::shared_ptr<pythia8>>(pythia,"Converter")
      .def(py::init<>([] { return std::make_shared<pythia8>(); }))
      .def("__call__", [](pythia8&             c,
                          run_info_ptr&        ri,
                          const Pythia8::Info& i,
                          Pythia8::Settings&   s) -> bool {
        return c(ri,i,s); },
           py::arg("run_info"),
           py::arg("pythia_info"),
           py::arg("pythia_settings"),
           "Convert Pythia info to HepMC run info")
      .def("__call__",[](pythia8&              c,
                         event_ptr&            e,
                         const Pythia8::Event& p,
                         const Pythia8::Info*  i) -> bool {
        return c(e,p,i); },
           py::arg("event"),
           py::arg("pythia_event"),
           py::arg("pythia_info"),
           "Convert Pythia event to HepMC event")
      ;
  } catch (py::error_already_set& e) {
    std::cerr << e.what() << ", Pythia interface not available" << std::endl;
  }
                         
#endif      
}

//
// EOF
//
