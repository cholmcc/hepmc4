#!/usr/bin/env python

from setuptools.dist import Distribution
from pybind11.setup_helpers import Pybind11Extension, build_ext

def wheel_name(**kwargs):
    # create a fake distribution from arguments
    dist = Distribution(attrs=kwargs)
    # finalize bdist_wheel command
    bdist_wheel_cmd = dist.get_command_obj('bdist_wheel')
    bdist_wheel_cmd.ensure_finalized()
    # assemble wheel file name
    distname = bdist_wheel_cmd.wheel_dist_name
    tag = '-'.join(bdist_wheel_cmd.get_tag())
    return f'{distname}-{tag}.whl'

args = { 'name': 'hepmc4',
         'version': '4.0.0',
         'ext_modules': [Pybind11Extension(
             'hepmc4',
            ['python/pyhepmc.cc'])]
        }

print(wheel_name(**args))
