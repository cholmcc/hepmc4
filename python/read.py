#!/usr/bin/env python
#
#  Library for handling HEP MC data
#  Copyright (C) 2023  Christian Holm Christensen
#
#  This program is free software: you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public License
#  as published by the Free Software Foundation, either version 3 of
#  the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see
#  <https://www.gnu.org/licenses/>.


def read_events(filename, maxEvents):
    from hepmc4 import Reader
    from sys import stdin

    with Reader(filename,stdin) as reader:

        for no, event in enumerate(reader):
            print(event)

            if maxEvents > 0 and no+1 >= maxEvents:
                break



if __name__ == '__main__':
    from argparse import ArgumentParser

    ap = ArgumentParser(description='Read in events and print')
    ap.add_argument('input',type=str,help='Input file name, - for stdin')
    ap.add_argument('-n','--max-events',type=int,default=-1,
                    help='Maximum number of events to read')

    args = ap.parse_args()

    read_events(args.input, args.max_events)

    
#
# EOF
#
