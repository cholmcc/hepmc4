#!/usr/bin/env python

'''
     Simple generator of an event.  The graph looks like 

                               p7
         p1                   /  
           \v1__p2      p5---v4  
                 \_v3_/       \  
                 /    \        p8
            v2__p4     \         
           /            p6       
         p3                      


     where

     | # | type | st | pdg  | par | px  | py   | pz     | E     | m    |
     |---|------|----|------|-----|-----|------|--------|-------|------|
     | 1 | p    | 4  | 2212 | 0,0 | 0   |  0   | 7000   |7000   | 0.938|
     | 2 | p    | 4  | 2212 | 0,0 | 0   |  0   |-7000   |7000   | 0.938|
     | 3 | d    | 3  |    1 | 1,1 | 0.75| -1.56|   32.19|  32.24| 0    |
     | 4 | ubar | 3  |   -2 | 2,2 |-3.04|-19.00|  -54.63|  57.92| 0    |
     | 5 | W-   | 2  |  -24 | 1,2 | 1.52|-20.68|  -20.61|  85.93|80.799|
     | 6 | gamma| 1  |   22 | 1,2 |-3.81|  0.11|   -1.83|   4.23| 0    |
     | 7 | d    | 1  |    1 | 5,5 |-2.45| 28.82|    6.08|  29.55| 0.01 |
     | 8 | ubar | 1  |    2 | 5,5 | 3.96|-49.50|  -26.69|  56.37| 0.006|
'''

from hepmc4 import Builder, Writer
from sys import stdout

event      = Builder.event()

projectile   = Builder.particle(0,0, 7000,7000,2212,4)
target       = Builder.particle(0,0,-7000,7000,2212,4)
d1           = Builder.particle(0.750, -1.569, 32.191,32.238,  1, 1)
ubar1        = Builder.particle(3.047,-19.0,  -54.629,57.920, -2, 1);

v1           = Builder.interaction([projectile],[d1])
v2           = Builder.interaction([target],    [ubar1])
event.add_vertex(v1)
event.add_vertex(v2)

v1.status    = 1
v2.status    = 2

gamma        = Builder.particle(-3.813,  0.113, -1.833, 4.233,  22);
w            = Builder.particle( 1.517,-20.68, -20.605,85.925, -24);

ip           = Builder.interaction([d1,ubar1],[gamma,w]);
ip.status    = 3
#event.add_vertex(ip)

d2           = Builder.particle(-2.445, 28.816,  6.082,29.552,  1);
ubar2        = Builder.particle( 3.962,-49.498,-26.687,56.373, -2);

v4           = Builder.decay(w,{d2,ubar2});
v4.status    = 4

loop = False # True
if loop:
    l = Builder.particle(0,0,0,0,22,10);
    ip.add_outgoing(l);
    v2.add_incoming(l);

    
with Writer("w.dot",stdout) as writer:
    writer.write_event(event)

    
