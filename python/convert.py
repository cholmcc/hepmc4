#!/usr/bin/env python
#
#  Library for handling HEP MC data
#  Copyright (C) 2023  Christian Holm Christensen
#
#  This program is free software: you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public License
#  as published by the Free Software Foundation, either version 3 of
#  the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see
#  <https://www.gnu.org/licenses/>.


def convert_events(inFileName, outFileName, maxEvents):
    from hepmc4 import Reader, Writer
    from sys import stdin, stdout
    from io import StringIO

    with Reader(inFileName,stdin) as reader:

        with Writer(outFileName,stdout) as writer:
            for no, event in enumerate(reader):
                writer.write_event(event)
                
                if maxEvents > 0 and no+1 >= maxEvents:
                    break



if __name__ == '__main__':
    from argparse import ArgumentParser

    ap = ArgumentParser(description='Read in events and print')
    ap.add_argument('input',type=str,help='Input file name, - for stdin')
    ap.add_argument('output',type=str,help='Output file name, - for stdout')
    ap.add_argument('-n','--max-events',type=int,default=-1,
                    help='Maximum number of events to read')

    args = ap.parse_args()

    convert_events(args.input, args.output, args.max_events)

    
#
# EOF
#
