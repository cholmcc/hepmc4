#!/usr/bin/env python
'''
Run Pythia from Python and output data as HepMC
'''

# ====================================================================    
class Base:
    def __init__(self,
                 verbose=False,
                 seed=-1,
                 projectile=2212,
                 target=2212,
                 cm_energy=14000,
                 config=None,
                 output='-',
                 n_events=10,
                 prune=False,
                 **kwargs):
        '''Base class for Pythia event generator.  This defines the
        common setup.  Derived classes may set up Pythia for specific
        purposes.

        The member `_select` is a lambda used when pruning events.  It
        should return true if a particle is to be kept in the
        output. Specific classes can override this member to do
        particular selections.  The default is to select particles
        with status codes 1 (final state), 2 (decayed) and 4 (beam).
        But the class LepZ overrides this to also select particles
        with status code 22 and 23 so that we may investigate the
        quark pair produced by the Z boson.

        Parameters
        ----------
        verbose : boolean
            Whether to be verbose
        seed : int
            Random number seed to use.  If less than 0, then do not
            set the seed, and Pythia will use the current time as the
            seed. If zero, then use default Pythia seed.  If larger
            than zero, use the specified number as seed.
        projectile: int
            PDG code of projectile particle
        target : int
            PDG code of target particle
        cm_energy : float
            Centre-of-mass energy in GeV
        config : str
            Optional configuration file to read
        output : str
            Output file to write to.  If the empty string or "-", then
            write to standard output
        n_events : int
            Number of events to generate
        prune : boolena
            If true, prune events
        kwargs : dict
            Additional keyword arguments

        '''
        from hepmc4.pythia8 import Converter
        from pythia8 import Pythia

        self._verbose    = verbose
        self._seed       = seed
        self._projectile = self.beam_pdg(projectile,'Projectile')
        self._target     = self.beam_pdg(target,    'Target')
        self._cm_energy  = cm_energy
        self._n_errors   = 0
        self._max_errors = 10
        self._config     = config
        self._output     = output
        self._nev        = n_events
        self._prune      = prune
        self._select     = lambda p : p.status in self._select
        
        self._pythia     = Pythia("",self._verbose)
        self._converter  = Converter()

    def beam_pdg(self,beam,which):
        from hepmc4.database import pdg
        
        try:
            pid = int(beam);
        except:
            pid = pdg(beam);
        if pid == 0:
            raise RuntimeError('{which} {target} invalid ({pid})')

        if self._verbose:
            print(f'{which} set to {pid} ({beam})')

        return pid
        
    def silence(self):
        '''Possibly suppress messages from Pythia'''
        q = "off" if self._verbose else "on"
        v = "on"  if self._verbose else "off"
        self._pythia.readString("Print:quiet                      = "+q)
        self._pythia.readString("Init:showAllSettings             = "+v)
        self._pythia.readString("Stat:showProcessLevel            = "+v)
        self._pythia.readString("Init:showAllParticleData         = "+v)
        self._pythia.readString("Init:showChangedParticleData     = "+v)
        self._pythia.readString("Init:showChangedSettings         = "+v)
        self._pythia.readString("Init:showMultipartonInteractions = "+v)


    def seed(self):
        '''Set the random number seed based on constructor arguments'''
        self._pythia.readString("Random:setSeed = "+
                                ('yes' if self._seed >= 0 else 'no'))
        self._pythia.readString("Random:seed = "+str(self._seed))


    def configure(self):
        '''Configure Pythia.  This should be overriden in specific
        classes to set-up Pythia for the specific purposes.  For
        example, the class PbPb overrides this to select the heavy-ion
        model to use, and similar, while the class PP selects between
        different collision types (INEL or NSD).
        '''
        self._pythia.readString("Beams:idA = "+str(self._projectile));
        self._pythia.readString("Beams:idB = "+str(self._target));
        self._pythia.readString("Beams:eA  = "+str(self._cm_energy/2));
        self._pythia.readString("Beams:eB  = "+str(self._cm_energy/2));
        self._pythia.readString("Beams:eCM = "+str(self._cm_energy));
        self._pythia.readString("ParticleDecays:limitTau0     = on");
        self._pythia.readString("ParticleDecays:tau0Max       = 10"); 


    def init(self):
        '''Initialise Pythia.  This is based on the (possibly
        overriden) configure member function.  If a configuration file
        was specified, then also load that for additional settings.'''
        self.silence()
        self.seed()
        self.configure()

        if self._config is not None and len(self._config) > 0:
            self._pythia.readFile(self._config)

        self._pythia.init()

    def make_event(self):
        '''Make a single event.  If the number of failed events exceed
        the maximum number of failures (default to 10), then return
        False, and the process stops.
        '''
        if not self._pythia.next():
            self._n_errors += 1
            if self._n_errors < self._max_errors: return True

            return False

        return True

    def loop(self):
        '''Run Pythia.  This sets up a writer to write out the events
        generated as HepMC events.  It generates as many events as
        specified at the time of construction. Returns true on
        success.''' 
        from hepmc4 import Writer, Builder 
        from sys import stdout

        if self._verbose:
            print('Creating writer on '+self._output)

        with Writer(self._output,stdout) as writer:
            event          = Builder.event()
            event.run_info = Builder.run_info()

            first = True

            for iev in range(self._nev):
                if self._verbose:
                    print(f'Creating event # {iev}')
                    
                if not self.make_event():
                    return False

                event.clear()

                if first:
                    if self._verbose:
                        print(f'First event, settting run_info')
                        
                    self._converter(event.run_info,
                                    self._pythia.infoPython(),
                                    self._pythia.settings)
                    first = False

                if self._verbose:
                    print(f'Converting event to HepMC')
                    
                self._converter(event,
                                self._pythia.event,
                                self._pythia.infoPython())

                event.number = iev

                if self._prune:
                    if self._verbose:
                        print('Pruning event')

                    event.prune(self._select)
                    
                if self._verbose:
                    print(f'Write out event')
                writer.write_event(event)


        return True

    def run(self):
        '''Initialise and run'''
        self.init()
        return self.loop()

# ====================================================================    
class PP(Base):
    def __init__(self,**kwargs):
        '''Set up Pythia to produce either INEL or NSD pp events'''
        super(PP,self).__init__(**kwargs)
        self._type = kwargs.get('type','INEL').upper()
        
    def configure(self):
        '''Configure the model'''
        if self._type not in ['NSD','INEL']:
            raise RuntimeError('Invalid event types: '+self._type)
        
        if self._type != 'NSD':
            self._pythia.readString('SoftQCD:inelastic = on')
        else:
            self._pythia.readString("SoftQCD:all                 = off")
            self._pythia.readString("SoftQCD:nonDifferactive     = on")
            self._pythia.readString("SoftQCD:doubleDifferactive  = on")
            self._pythia.readString("SoftQCD:centralDifferactive = on")
        
        self._pythia.readString("Tune:ee                       = 7");
        self._pythia.readString("Tune:pp                       = 14");
            
        super(PP,self).configure()


# ====================================================================    
class PbPb(Base):
    pb_pdg = 1000822080
    
    def __init__(self,**kwargs):
        '''Configure Pythia/Angantyr for heavy-ion collisions'''

        if 'projectile' not in kwargs: kwargs['projectile'] = PbPb.pb_pdg
        if 'target'     not in kwargs: kwargs['target']     = PbPb.pb_pdg
            
        super(PbPb,self).__init__(**kwargs)

        self._fit_sigma  = kwargs.get('fit_sigma',False)
        self._model      = kwargs.get('model','random').lower()
        self._max_b      = kwargs.get('max_b',14.5)
        self._cm_energy  = kwargs.get('cm_energy', 5023)


    def configure(self):
        '''Configure the model'''
        models = ['fixed','random','opacity']
        mod_no = models.index(self._model)
        fit    = [13.91,1.78,0.22,0.0,0.0,0.0,0.0,0.0]

        if self._projectile != PbPb.pb_pdg or \
           self._target     != PbPb.pb_pdg or \
               self._cm_energy != 5023:
            self._fit_sigma = True

        self._pythia.readString("Angantyr::CollisionModel     = "+str(mod_no))
        self._pythia.readString("HeavyIon:showInit            = "+
                                ("on" if self._verbose else "off"))
        self._pythia.readString("HeavyIon:SigFitPrint         = off");
        self._pythia.readString("HeavyIon:SigFitDefPar        = "+
                                ','.join([str(p) for p in fit]))
        self._pythia.readString("HeavyIon:SigFitNGen          = "+
                                ('20' if self._fit_sigma else '0'))
        self._pythia.readString("HeavyIon:bWidth              = "+
                                str(self._max_b))

        super(PbPb,self).configure()

# ====================================================================    
class LepZ(Base):
    def __init__(self,**kwargs):
        '''Example of generating Z's at LEP energies.  From Pythia example
        `main06.cc`
        '''
        super(LepZ,self).__init__(**kwargs)
        self._select = lambda p : p.status in [1,2,4,22,23]

    def configure(self):
        # LEP1 initialization at Z0 mass.
        self._pythia.readString("PDF:lepton = off");
        self._pythia.readString("WeakSingleBoson:ffbar2gmZ = on");
        self._pythia.readString("23:onMode = off");
        self._pythia.readString("23:onIfAny = 1 2 3 4 5");
        self._pythia.readString("Beams:idA =  11");
        self._pythia.readString("Beams:idB = -11");
        mZ = self._pythia.particleData.m0(23);
        self._pythia.settings.parm("Beams:eCM", mZ);

        
# ====================================================================    
if __name__ == '__main__':
    from argparse import ArgumentParser
    from sys import argv, path

    path.append('/opt/sw/inst/lib')

    ap = ArgumentParser(description='Run Pythia')

    if 'angantyr' in argv[0]:
        pythia_type  = PbPb
        proj         = PbPb.pb_pdg
        targ         = PbPb.pb_pdg
        ap.add_argument('-m','--model',choices=['fixed','random','opacity'],
                        default='random',
                        help='Type interaction model')
        ap.add_argument('-f','--fit-sigma',action='store_true',
                        help='Fit cross section')
        ap.add_argument('-b','--max-b',type=float,default=14.5,
                        help='Maximum impact parameter in fermimeter')
    elif 'lepz' in argv[0].lower():
        pythia_type = LepZ
        proj         = 11
        targ         = -11
    else:
        pythia_type  = PP
        proj         = 2212
        targ         = 2212
        ap.add_argument('-T','--type',choices=['INEL','NSD'],default='INEL',
                        help='Type of pp collisions')
    
    ap.add_argument('-v','--verbose',action='store_true',
                    help='Be verbose')
    ap.add_argument('-n','--n-events',type=int,default=10,
                    help='Number of events to generate')
    ap.add_argument('-p','--projectile',type=str,default=proj,
                    help='PDG number of projectile')
    ap.add_argument('-t','--target',type=str,default=targ,
                    help='PDG number of target')
    ap.add_argument('-s','--seed',type=int,default=-1,
                    help='Random number seed')
    ap.add_argument('-e','--cm-energy',type=float,default=14000,
                    help='Centre of mass energy in GeV')
    ap.add_argument('-c','--config',type=str,default='',
                    help='Configuration file')
    ap.add_argument('-o','--output',type=str,default='-',
                    help='Output filename, "-" means standard output')
    ap.add_argument('-P','--prune',action='store_true',
                    help='Prune events')


    args = ap.parse_args()
    
    eg = pythia_type(**vars(args))

    if not eg.run():
        raise RuntimeError('Running the model failed')

#
# EOF
#
