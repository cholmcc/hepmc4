//  Library for handling HEP MC data
//  Copyright (C) 2023  Christian Holm Christensen
//
//  This program is free software: you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation, either version 3 of
//  the License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see
//  <https://www.gnu.org/licenses/>.
//                              
/**
   @file      testReadback.cc
   @copyright (c) 2023 Christian Holm Christensen
   @brief     Test writing and reading back same event
*/
#include "generate"
#include <hepmc4/ascii2>
#include <hepmc4/ascii3>
#include <hepmc4/ascii4>
#include <hepmc4/hepevt>
#include <hepmc4/root>
#include <hepmc4/protobuf>
#include <hepmc4/stream>

// -------------------------------------------------------------------
/**
   Type of middle function

   @tparam  T      Floating point value type
   @ingroup hepmc_test
*/
template<typename T>
using middle_func = void(hepmc4::event_ptr<T>&);


// -------------------------------------------------------------------
/**
   Test writing and reading events in a specific format.

   An event is generated and written to a string stream.

   The event is read back from the string stream.

   The two events, before writing and after reading, are compared. 

   @tparam  T      Floating point value type
   @tparam  W      Writer type
   @tparam  R      Reader type
   @tparam  Middle Type of function to execute on events
   @param   what   What are we testing
   @param   show   Whether to show the event
   @param   middle Function to execute on events before comparisons
   @return  Non-zero on error
   @ingroup hepmc_test
*/
template <typename T,
          template <typename> class W,
          template <typename> class R,
          typename Middle=middle_func<T>>
int testReadback(const std::string& what,
                 bool show=false,
                 Middle middle=[](hepmc4::event_ptr<T>&){})
{
  using builder_type=hepmc4::builder<T>;
  using reader_type=R<T>;
  using writer_type=W<T>;

  auto event_out = hepmc4::tests::make<T>(); // populated
  auto event_in  = builder_type::event();    // empty
  
  // Stream to write to and read from 
  std::stringstream stream;
  stream << std::scientific << std::setprecision(8);
  
  {
    writer_type writer(stream);
    hepmc4::tests::generate(event_out); // fill event
    writer.write_event(event_out); // write to stream 
  }
    
  middle(event_out);
    
  if (show) std::cout << stream.str() << std::endl;
    
  int ret = 0;
  
  {
    reader_type reader(stream);
    ret += not reader.read_event(event_in);
  }
  
  event_out->verbose_eq = true;
  bool eq = (*event_out == *event_in);
  std::clog << what << ": Reading back does " << (eq ? "" : "NOT ")
            << "restore event" << std::endl;
  if (not eq) {
    ret++;
    
    std::clog << "Output event ====================================\n"
              << *event_out << "\n"
              << "Input event =====================================\n"
              << *event_in << std::endl;
  }
  return ret;
}

/**
   Test all formats for readback

   @tparam  T Floating point value type
   @return  Non-zero on error
   @ingroup hepmc_test
*/
template <typename T=hepmc4::default_values_type>
int testReadbackAll()
{
  int ret = 0;

  ret += testReadback<T,
                      hepmc4::ascii2::writer,
                      hepmc4::ascii2::reader>("ASCII2");
  ret += testReadback<T,
                      hepmc4::ascii3::writer,
                      hepmc4::ascii3::reader>("ASCII3");
  ret += testReadback<T,
                      hepmc4::hepevt::writer,
                      hepmc4::hepevt::reader>("HEPEVT",
                                              false,
                                              hepmc4::hepevt
                                              ::bare_bones<float>);
  ret += testReadback<T,
                      hepmc4::ascii4::writer,
                      hepmc4::ascii4::reader>("ASCII4");

  ret += testReadback<T,
                      hepmc4::stream::writer,
                      hepmc4::stream::reader>("Stream");
  
  try {
    ret += testReadback<T,
                        hepmc4::root::writer,
                        hepmc4::root::reader>("ROOT",
                                              false,
                                              hepmc4::root::bare_bones<float>);
  }
  catch (hepmc4::not_supported& e) {
    std::cerr << e.what() << " (ignored)" << std::endl;
  }
  try {
    ret += testReadback<T,
                        hepmc4::protobuf::writer,
                        hepmc4::protobuf::reader>("ProtoBuf");
  }
  catch (hepmc4::not_supported& e) {
    std::cerr << e.what() << " (ignored)" << std::endl;
  }

  
  return ret;
}

/**
   Test writing and reading events in the HepMC version 2 (aka
   IO_GenEvent) and 3 (aka Asciiv3) formats, as well as the HEPEVT
   legacy format.

   An event is generated and written to a string stream

   The event is read back from the string stream

   The two events, before writing and after reading, are compared. 


   @ingroup hepmc_test
*/
int main()
{
  return testReadbackAll<>();
}
//
// EOF
//

