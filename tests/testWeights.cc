//  Library for handling HEP MC data
//  Copyright (C) 2023  Christian Holm Christensen
//
//  This program is free software: you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation, either version 3 of
//  the License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see
//  <https://www.gnu.org/licenses/>.
//                              
/**
   @file      testWeights.cc
   @copyright (c) 2023 Christian Holm Christensen
   @brief     Test of weights
*/
#include <iostream>
#include <iomanip>
#include <hepmc4/weights>

/**
   Test weight parsing and formatting, etc.

   @tparam  T Floating point value type
   @return  Non-zero on error
   @ingroup hepmc_test
*/
template <typename T=hepmc4::default_values_type>
int testWeights()
{
  int ret = 0;

  using real_type = typename hepmc4::weights<T>::value_type;
  
  hepmc4::weights<T> w1(3);
  w1 = {1,2,3};

  real_type x = 1;
  for (auto ww : w1.values()) {
    if (x != ww) {
      std::cerr << "Wrong value: " << ww << " expected " << x << std::endl;
      ret++;
    }
    x++;
  }
  try {
    w1["foo"];
  } catch(std::exception& e) {
    std::cerr << "This is expected: " << e.what() << std::endl;
  }
  if (w1.has("foo")) {
    std::cerr << "Names not set, no test with 'has'" << std::endl;
    ret++;
  }
  
  std::cout << w1[2] << " == " << 3 << "?" << std::endl;

  w1.add_weight(4);
  if (w1.size() != 4) {
    std::cerr << "Faild to add weight to weights" << std::endl;
    ret++;
  }
  w1.add_weight(5);
  if (w1.size() != 5) {
    std::cerr << "Faild to add weight to weights" << std::endl;
    ret++;
  }

  hepmc4::weights<T> w2;
  w2 = {{"a",1},{"b",2},{"c",3}};

  if (w2.names().size() != 3) {
    std::cerr << "Not enough names" << std::endl;
    ret++;
  }
  x = 1;
  for (auto ww : w2.values()) {
    if (x != ww) {
      std::cerr << "Wrong value: " << ww << " expected " << x << std::endl;
      ret++;
    }
    x++;
  }
  w2["c"] = 5;

  if (not w2.has("a")) {
    std::cerr << "Failed to find weight 'a'" << std::endl;
    ret++;
  }
  w2.add_weight(4);

  w2.add_weight("d", 4);
  if (w2.size() != 4) {
    std::cerr << "Failed to add named weight" << std::endl;
    ret++;
  }
    
  
  
  return ret;
}

/**
   Test weight parsing and formatting, etc.

   @ingroup hepmc_test   
*/
int main()
{
  return testWeights<>();
}
//
// EOF
//
