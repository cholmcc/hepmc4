//  Library for handling HEP MC data
//  Copyright (C) 2023  Christian Holm Christensen
//
//  This program is free software: you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation, either version 3 of
//  the License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see
//  <https://www.gnu.org/licenses/>.
//                              
/**
   @file      testEvent.cc
   @copyright (c) 2023 Christian Holm Christensen
   @brief     Test events
*/
#include <iostream>
#include <iomanip>
#include "generate"
#include <hepmc4/indexed_event>

/**
   Test basic creation of events.  Note, we use the builder class for
   it.

   @tparam  T Floating point value type
   @return  Non-zero on error
   @ingroup hepmc_test
*/
template <typename T=hepmc4::default_values_type>
int testEvent()
{
  auto  event        = hepmc4::tests::make<T>();
  hepmc4::tests::generate(event);
  hepmc4::indexed_event<T> iev(hepmc4::depth_indexer<T>()(event));

  std::cout << *event << std::endl;
  
  for (auto p : iev.particles()) 
    std::cout << std::setw(3) << iev.particle_index(p)
              << std::setw(12) << p->pid
              << std::setw(4) << p->status << "\t"
              << p << std::endl;
  return 0;
}

  
/**
   Test basic creation of events.  Note, we use the builder class for
   it.

   @ingroup hepmc_tezt
*/
int main()
{
  return testEvent<>();
}
//
// EOF
//

