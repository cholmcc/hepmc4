//  Library for handling HEP MC data
//  Copyright (C) 2023  Christian Holm Christensen
//
//  This program is free software: you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation, either version 3 of
//  the License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see
//  <https://www.gnu.org/licenses/>.
//                              
/**
   @file      testSkip.cc
   @copyright (c) 2023 Christian Holm Christensen
   @brief     Test skipping events
*/
#include "generate"
#include <hepmc4/ascii2>
#include <hepmc4/ascii3>
#include <hepmc4/ascii4>
#include <hepmc4/hepevt>
#include <hepmc4/root>
#include <hepmc4/protobuf>

// -------------------------------------------------------------------
/**
   Test of skipping events.

   A number of events @a nwrite are generated and written to a string
   stream.

   The events are then read back from the string stream, except we
   skip over every @a nfreq event.

   The number of written events, and the sum of number of read and
   skipped events are then compared.

   @tparam  T      Floating point value type
   @tparam  W      Writer type
   @tparam  R      Reader type
   @param   what   What are we testing
   @param   nwrite Number of events to write
   @param   nfreq  Frequency of skipping
   @param   dump   Show output
   @return  Non-zero on error
   @ingroup hepmc_test
*/
template <typename T,
          template <typename> class W,
          template <typename> class R>
int testSkip(const std::string& what, size_t nwrite=10, size_t nfreq=3,
             bool dump=false)
{
  using builder_type = hepmc4::builder<T>;
  using writer_type  = W<T>;
  using reader_type  = R<T>;

  auto event_out = hepmc4::tests::make<T>(); // populated
  auto event_in  = builder_type::event();    // empty

  std::stringstream stream;
  stream << std::scientific << std::setprecision(8);
  
  {
    writer_type writer(stream);
    for (size_t iev = 0; iev < nwrite; iev++) {
      hepmc4::tests::generate(event_out);
      writer.write_event(event_out);
    }
  }

  if (dump) std::cout << stream.str() << std::endl;

  size_t nread = 0;
  size_t nskip = 0;

  {
    reader_type reader(stream);

    size_t iev = 0;
    while (true) {
      if (iev % nfreq == 0) {
        if (not reader.skip_event()) break;
        ++nskip;
      }
      else {
        if (not reader.read_event(event_in)) break;
        ++nread;
      }
      ++iev;
    }
  }

  std::clog << what
            << ": Wrote " << nwrite
            << ", read " << nread
            << ", and skipped " << nskip
            << " events" << std::endl;

  return (nread+nskip) != nwrite;
}

// -------------------------------------------------------------------
/**
   This is a test of the comparison of events.

   The test generates two events, and then compares them using the ==
   operator.  The test fails if they do not compare equal.

   The test then changes the momentum of one particle in second and
   does the comparison again.  The test fails if the events _do_
   compare equal.


   @tparam  T Floating point value type
   @return  Non-zero on error
   @ingroup hepmc_test
*/
template <typename T=hepmc4::default_values_type>
int testSkipAll()
{
  int ret = 0;

  ret += testSkip<T,
                  hepmc4::ascii2::writer,
                  hepmc4::ascii2::reader>("ASCII2",11);
  ret += testSkip<T,
                  hepmc4::ascii3::writer,
                  hepmc4::ascii3::reader>("ASCII3",11);
  ret += testSkip<T,
                  hepmc4::hepevt::writer,
                  hepmc4::hepevt::reader>("HEPEVT",11);
  ret += testSkip<T,
                  hepmc4::ascii4::writer,
                  hepmc4::ascii4::reader>("ASCII4",11);
  try {
    ret += testSkip<T,
                    hepmc4::root::writer,
                    hepmc4::root::reader>("ROOT",11);
  }
  catch (hepmc4::not_supported& e) {
    std::cerr << e.what() << " (ignored)" << std::endl;
  }
  
  try {
    ret += testSkip<T,
                    hepmc4::protobuf::writer,
                    hepmc4::protobuf::reader>("ProtoBuf",11);
  }
  catch (hepmc4::not_supported& e) {
    std::cerr << e.what() << " (ignored)" << std::endl;
  }
  
  return ret;
}

// -------------------------------------------------------------------
/**
   Test of skipping events.

   A number of events @a nwrite are generated and written to a string
   stream.

   The events are then read back from the string stream, except we
   skip over every @a nfreq event.

   The number of written events, and the sum of number of read and
   skipped events are then compared.


   @ingroup hepmc_test
*/
int main()
{
  return testSkipAll<>();
}
//
// EOF
//

