# Tests of HepMC4 

## Content 

- [`testAttributes.cc`](testAttributes.cc)  Test of attributes:
  Writing, reading, conversion to types, etc. 
- [`testCompare.cc`](testCompare.cc) Test comparison of events 
- [`testCopy.cc`](testCopy.cc) Test of copying events
- [`testEvent.cc`](testEvent.cc) Test building an event 
- [`testFourVector.cc`](testFourVector.cc) Test four-vector
  calculations 
- [`testHepevt.cc`](testHepevt.cc) Test of `COMMON/HEPEVT/` interface. 
- [`testPrune.cc`](testPrune.cc) Test of pruning events 
- [`testReadback.cc`](testReadback.cc) Test of I/O of events in all
  supported formats. 
- [`testRelatives.cc`](testRelatives.cc) Test of finding relatives
  (ancestors and descendants) of vertices and particles 
- [`testSkip.cc`](testSkip.cc) Test of skipping events for all
  supported formats. 
- [`testSpecial.cc`](testSpecial.cc) Test of special attributes
  (`hepmc4::cross_section`, `hepmc4::pdf_info`, and
  `hepmc4::heavy_ion`). 
- [`testWeights.cc`](testWeights.cc) Test of event weights. 

## Run the tests

Simply do 

    make tests
    
