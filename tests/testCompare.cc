//  Library for handling HEP MC data
//  Copyright (C) 2023  Christian Holm Christensen
//
//  This program is free software: you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation, either version 3 of
//  the License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see
//  <https://www.gnu.org/licenses/>.
//                              
/**
   @file      testCompare.cc
   @copyright (c) 2023 Christian Holm Christensen
   @brief     Test comparion of events
*/
#include <iostream>
#include <iomanip>
#include "generate"
#include <hepmc4/indexed_event>

/**
   This is a test of the comparison of events.

   The test generates two events, and then compares them using the ==
   operator.  The test fails if they do not compare equal.

   The test then changes the momentum of one particle in second and
   does the comparison again.  The test fails if the events _do_
   compare equal.


   @tparam  T Floating point value type
   @return  Non-zero on error
   @ingroup hepmc_test
*/
template <typename T=hepmc4::default_values_type>
int testCompare()
{
  int ret = 0;
  auto  lhs          = hepmc4::tests::make<T>();
  auto  rhs          = hepmc4::tests::make<T>();
  hepmc4::tests::generate(lhs);
  hepmc4::tests::generate(rhs);

  bool eq = lhs->operator==(*rhs);
  std::cout << "Events are " << (eq ? "" : "NOT ") << "equal" << std::endl;
  if (not eq) ret++;

  std::cout << "Change first particle momentum" << std::endl;
  (*lhs->particles().begin())->momentum[2] = 10;
  bool neq = lhs->operator!=(*rhs);
  std::cout << "Events are " << (neq ? "NOT " : "") << "equal" << std::endl;
  if (not neq) ret++;
  
  return ret;
}  

/**
   Test comparisons

   @ingroup hepmc_tezt
*/
int main()
{
  return testCompare<>();
}
//
// EOF
//

