//  Library for handling HEP MC data
//  Copyright (C) 2023  Christian Holm Christensen
//
//  This program is free software: you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation, either version 3 of
//  the License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see
//  <https://www.gnu.org/licenses/>.
//                              
/**
   @file      testAttributes.cc
   @copyright (c) 2023 Christian Holm Christensen
   @brief     Test of attributes
*/
#include <iostream>
#include <iomanip>
#include <hepmc4/with_attributes>
#include <hepmc4/heavy_ion>
#include <vector>
#include <list>
#include <unordered_set>

/**
   This test tests all things attribute related.  Attributes are
   created and in a with_attributes object.  We retrieve attributes
   and change types if needed, clear attributes, and so on.

   @tparam T     Floating point value type
   @return Non-zero on error
   @ingroup hepmc_test
*/
template <typename T=hepmc4::default_values_type>
int testAttributes()
{
  int ret = 0;
  
  hepmc4::with_attributes as;

  // Create a pointer to attribute 
  auto a = std::make_shared<hepmc4::attribute<int>>(1);
  std::cout << a << std::endl;

  // Add some attributes 
  as.add_attribute("a",a);
  as.add_attribute("b",42);
  as.add_attribute("c",3.14);
  as.add_attribute("d",std::string("12345"));
  as.add_attribute("e",std::vector<float>{1.1,1.2,1.3});
  as.add_attribute("f",std::list<int>{1,2,3,4,5});
  as.add_attribute("g",std::unordered_set<char>{'a','b','c'});
  as.add_attribute("h",std::unordered_map<int,char>{{1,'a'},{2,'b'},{3,'c'}});
  as.add_attribute("i",std::string("foo 1 bar 2 baz 3"));

  std::cout << "After setup\n" << as << std::flush;

  // Get a string attribute as an integer attribute, and make it
  // persistent
  auto d = as.attribute<int>("d");
  d->persistent = true;
  if (d->value() != 12345) {
    std::cout << "Got wrong value: " << d->value() << std::endl;
    ret++;
  }

  // Get a string attribute and parse it as a map attribute 
  auto i = as.attribute<std::unordered_map<std::string,int>>("i");
  if (i->value().size() != 3 or i->value().count("foo") != 1) {
    std::cout << "Got wrong values: \"" << i << "\"" << std::endl;
    for (auto ii : i->value()) 
      std::cout << "- " << ii.first << " -> " << ii.second << std::endl;
    ret++;
  }

  // Check value of an attribute 
  if (as.attribute<int>("a")->value() != 1) {
    std::cout << "Got wrong value, expected 1" << std::endl;
    ret++;
  }

  // Show attributes after changing some types 
  std::cout << "After retrival\n" << as << std::endl;

  // Store formatted attributes to a string
  std::stringstream str;
  as.format(1,str);

  // Reset all attributes to default values 
  as.reset();
  std::cout << "After reset\n" << as << std::endl;

  // Clear away all attributes, except those persistent 
  as.clear();  
  std::cout << "After clear\n" << as << std::endl;

  // Create heavy-ion attribute 
  auto hi = std::make_shared<hepmc4::heavy_ion<>>();
  hi->impact_parameter = 10;
  hi->n_coll_hard      = 999;
  as.add_attribute(hepmc4::heavy_ion<>::name,hi);

  std::cout << "After heavy-ion\n" << as << std::endl;

  // Retrive formatted attributes from a string
  int id;
  as.parse(str,id);
  std::cout << "After retrival\n" << as << std::endl;
  
  return ret;
}

/**
   Test of attributes

   @ingroup hepmc_test
*/
int main()
{
  return testAttributes<>();
  
}

//
// EOF
//
