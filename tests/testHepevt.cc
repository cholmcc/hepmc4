//  Library for handling HEP MC data
//  Copyright (C) 2023  Christian Holm Christensen
//
//  This program is free software: you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation, either version 3 of
//  the License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see
//  <https://www.gnu.org/licenses/>.
//                              
/**
   @file      testHepevt.cc
   @copyright (c) 2023 Christian Holm Christensen
   @brief     Test interace to `COMMON/HEPEVT/`
*/
#include "generate"
#include <hepmc4/hepevt>

/**
   Test exporting and importing events to and from a HEPEVT common
   block.

   An event is generated, and attributes are cleared (since they are
   not retained by HEPEVT) and written to a string stream

   The event is read back from the string stream

   The two events, before writing and after reading, are compared.

   @tparam  T Floating point value type
   @return  Non-zero on error
   @ingroup hepmc_test
*/
template <typename T=hepmc4::default_values_type>
int testHepevt()
{
  int ret = 0;
  
  using builder_type  = hepmc4::builder<T>;  
  auto  event_exp     = hepmc4::tests::make<T>();
  auto  event_imp     = builder_type::event();
  hepmc4::hepevt::common<100> common;

  // Generate event and strip-off stuff HEPEVT doesn't handle
  hepmc4::tests::generate(event_exp);
  hepmc4::hepevt::bare_bones(event_exp);

  // Try to export to HEPEVT structure
  hepmc4::hepevt::convert(event_exp, common);

  // Try to import from HEPEVT structure
  hepmc4::hepevt::convert(common, event_imp);

  // Write out common block 
  std::cout << common << std::endl;

  // Verbose comparison 
  event_exp->verbose_eq = true;

  // Compare events
  bool eq = (*event_exp == *event_imp);
  std::cout << "Export and import does " << (eq ? "" : "NOT ")
            << "restore event" << std::endl;

  if (not eq) {
    ret++;

    std::cout << "Output event ====================================\n"
              << *event_exp << "\n"
              << "Input event =====================================\n"
              << *event_imp << std::endl;
  }
  
  return ret;
}

/**
   Test of HEPEVT

   @ingroup hepmc_test
*/
int main()
{
  return testHepevt<>();
}
//
// EOF
//

