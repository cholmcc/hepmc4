//  Library for handling HEP MC data
//  Copyright (C) 2023  Christian Holm Christensen
//
//  This program is free software: you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation, either version 3 of
//  the License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see
//  <https://www.gnu.org/licenses/>.
//                              
/**
   @file      testFourVector.cc
   @copyright (c) 2023 Christian Holm Christensen
   @brief     Test four vector calculations
*/
#include <iostream>
#include <iomanip>
#include <hepmc4/four_vector>
#include <vector>

/**
   Test calculations on four-vectors

   @tparam  T Floating point value type
   @return  Non-zero on error
   @ingroup hepmc_test
*/
template <typename T=hepmc4::default_values_type>
int testFourVector()
{
  int ret = 0;

  std::cout << std::scientific << std::setprecision(10);
  
  hepmc4::four_vector<T> v;
  v = {1,2,3,4};
  if (v != hepmc4::four_vector<T>(1,2,3,4)) {
    std::cerr << "Assignment failed!" << std::endl;
    ret ++;
  }
  else 
    std::cout << v << " == [1,2,3,4]" << std::endl;
  
  if (not hepmc4::isclose(v.length(), std::sqrt(1*1+2*2+3*3))) {
    std::cerr << "Length failed! " << v.length()
              << " != " << std::sqrt(14) << std::endl;
    ret ++;
  }
    
    
  using real_type = typename hepmc4::four_vector<T>::value_type;
  auto  eta = [](real_type px, real_type py, real_type pz) -> real_type{
    real_type p = std::sqrt(px*px+py*py+pz*pz);
    return std::log((p + pz) / (p - pz))*0.5;
  };
  auto  rap = [](real_type e, real_type pz)  -> real_type{
    return std::log((e + pz) / (e - pz))*0.5;
  };
  
  std::vector<hepmc4::four_vector<T>>  test{
    hepmc4::four_vector<T>(0.0, 0.0, 0.0, 0.0),
    hepmc4::four_vector<T>(1.0, 2.0, 0.0, 0.0),
    hepmc4::four_vector<T>(0.0, 0.0, 0.0, 1.0),
    hepmc4::four_vector<T>(0.0, 0.0, 0.0,-1.0),
    hepmc4::four_vector<T>(0.0, 0.0, 1.0, 0.0),
    hepmc4::four_vector<T>(0.0, 0.0,-1.0, 0.0),
    hepmc4::four_vector<T>(1.0, 2.0, 3.0, 4.0),
    hepmc4::four_vector<T>(1.0, 2.0, 3.0, -4.0),
    hepmc4::four_vector<T>(1.0, 2.0, -3.0, 4.0),
    hepmc4::four_vector<T>(1.0, 2.0, -3.0, -4.0),
    hepmc4::four_vector<T>(1.0, 2.0, -3.0, 40.0),
    hepmc4::four_vector<T>(1.0, 2.0, -3.0, -40.0)
  };
  std::vector<real_type> correct_eta{
    0.0,
    0.0,
    0.0,
    0.0,
    std::numeric_limits<real_type>::infinity(),
    -std::numeric_limits<real_type>::infinity(),
    /*
      std::numeric_limits<real_type>::max(),
      -std::numeric_limits<real_type>::max(),
    */
    eta(1.0,2.0,3.0),
    eta(1.0,2.0,3.0),
    eta(1.0,2.0,-3.0),
    eta(1.0,2.0,-3.0),
    eta(1.0,2.0,-3.0),
    eta(1.0,2.0,-3.0)
  };
  std::vector<real_type>   correct_rap{
    0.0,
    0.0,
    0.0,
    0.0,
    0.0,
    0.0,
    rap(4.0,3.0),
    rap(-4.0,3.0),
    rap(4.0,-3.0),
    rap(-4.0,-3.0),
    rap(40.0,-3.0),
    rap(-40.0,-3.0)
  };

  auto t = test.begin();
  auto e = correct_eta.begin();
  auto r = correct_rap.begin();
  for (;
       t != test.end() and
         e != correct_eta.end() and 
         r != correct_rap.end();
       (void)++t, (void)++e, ++r) {
    auto et  = t->pseudorapidity();
    auto y   = t->rapidity();
    bool eOK = (et == *e);
    bool yOK = (et == *e);
    std::cout << et << " vs " << *e << " " << (eOK ? "OK" : "Bad") << "\t"
              << y  << " vs " << *r << " " << (yOK ? "OK" : "Bad") << std::endl;
    if (not eOK) ret++;
    if (not yOK) ret++;
  }

  if (not (test[0]+test[1] == test[1])) {
    std::cout << "Failed on addition of 0 and 1" << std::endl;
    ret++;
  }
  if (not (test[2]+test[3]).is_zero()) {
    std::cout << "Failed on addition of 0 and 1" << std::endl;
    ret++;
  }

  auto rot = [](const hepmc4::four_vector<T>& v,
                bool x, bool y, bool z) {
    auto r = v;
    auto a = M_PI/2;
    return r.rotate(x * a, y * a, z * a);
  };
  auto testRot = [&ret,rot](const hepmc4::four_vector<T>& v,
                            bool x, bool y, bool z)
  {
    auto r = rot(v,x,y,z);
    std::cout << v << " (" << x << y << z << ")->" << r << std::endl;
    if (not hepmc4::isclose(r.t(),v.t())) {
      std::cerr << "Rotation changed fourth coordinate" << std::endl;
      ret++;
    }
    if (x) {
      if (not hepmc4::isclose(r.x(),v.x())) {
        std::cerr << "X coordinate changed by rotation about X" << std::endl;
        ret++;
      }
      if (not hepmc4::isclose(r.y(),v.z()) or
          not hepmc4::isclose(r.z(),-v.y())) {
        std::cerr << "X rotation not correct" << std::endl;
        ret++;
      }
    }
    if (y) {
      if (not hepmc4::isclose(r.y(),v.y())) {
        std::cerr << "Y coordinate changed by rotation about Y" << std::endl;
        ret++;
      }
      if (not hepmc4::isclose(r.x(),-v.z()) or
          not hepmc4::isclose(r.z(),v.x())) {
        std::cerr << "Y rotation not correct" << std::endl;
        ret++;
      }
    }
    if (z) {
      if (not hepmc4::isclose(r.z(),v.z())) {
        std::cerr << "Z coordinate changed by rotation about Z" << std::endl;
        ret++;
      }
      if (not hepmc4::isclose(r.x(),v.y()) or
          not hepmc4::isclose(r.y(),-v.x())) {
        std::cerr << "Y rotation not correct" << std::endl;
        ret++;
      }
    }
  };

  
  testRot(test[11],true, false,false);
  testRot(test[11],false,true, false);
  testRot(test[11],false,false,true );
  
  return ret;
}

/**
   Test calculations on four-vectors

   @ingroup hepmc_test
*/
int main()
{
  return testFourVector<>();
}
//
// EOF
//
