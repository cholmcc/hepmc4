//  Library for handling HEP MC data
//  Copyright (C) 2024  Christian Holm Christensen
//
//  This program is free software: you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation, either version 3 of
//  the License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see
//  <https://www.gnu.org/licenses/>.
//                              
/**
   @file      testRelatives.cc
   @copyright (c) 2024 Christian Holm Christensen
   @brief     Test of relatives 
*/
#include <iostream>
#include <iomanip>
#include "generate"
#include <hepmc4/io_factory>

// ===================================================================
/**
   Look for ancestors of a particle.  The test fails if the number of
   ancestors found does not match the expected number of ancestors.
   Note, this dependent on the depth to which we create the event.
   The assumption here is we create up to depth 5.

   @tparam  T Floating point value type
   @param   p Particle to investigate 
   @return  True on success
   @ingroup hepmc_test
*/
template <typename T>
bool testAncestors(std::shared_ptr<hepmc4::particle<T>>& p)
{
  using particle_ptr = std::shared_ptr<hepmc4::particle<T>>;
  std::unordered_set<particle_ptr> rel;
  p->ancestors(rel);

  size_t expected = (p->pid == 2212 ? 0 :
                     p->pid == 21 ? 2 :
                     p->status + 2);
  
  if (rel.size() != expected) {
    std::cerr << "Particle has wrong number of ancestors="
              << rel.size() << " expected=" << expected << "\n"
              << *p << std::endl;
    for (auto a : rel) {
      std::cerr << "<== " << *a << std::endl;
    }
    return false;
  }
  return true;
}

// -------------------------------------------------------------------
/**
   Look for descendants of a particle.  The test fails if the number of
   descendants found does not match the expected number of descendants.
   Note, this dependent on the depth to which we create the event.
   The assumption here is we create up to depth 5.

   @tparam  T Floating point value type
   @param   p Particle to investigate 
   @return  True on success
   @ingroup hepmc_test
*/
template <typename T>
bool testDescendants(std::shared_ptr<hepmc4::particle<T>>& p)
{
  using particle_ptr = std::shared_ptr<hepmc4::particle<T>>;
  std::unordered_set<particle_ptr> rel;
  p->descendants(rel);

  size_t ngen     = 8 * 2 + 4 * 2 + 2 * 2 + 2 + 1;
  size_t expected = (p->pid == 2212 ? ngen      :
                     p->pid == 21   ? ngen-1    :
                     p->status == 1 ? 2+2*2+4*2 :
                     p->status == 2 ? 2+2*2     : 
                     p->status == 3 ? 2         :
                     0);
  
  if (rel.size() != expected) {
    std::cerr << "Particle has wrong number of descendants="
              << rel.size() << " expected=" << expected << "\n"
              << *p << std::endl;
    for (auto d : rel) {
      std::cerr << "<== " << *d << std::endl;
    }
    return false;
  }
  return true;
}

// ===================================================================
/**
   Look for ancestors of a vertex.  The test fails if the number of
   ancestors found does not match the expected number of ancestors.
   Note, this dependent on the depth to which we create the event.
   The assumption here is we create up to depth 5.

   @tparam  T Floating point value type
   @param   v Vertex to investigate 
   @return  True on success
   @ingroup hepmc_test
*/
template <typename T>
bool testAncestors(std::shared_ptr<hepmc4::vertex<T>>& v)
{
  using vertex_ptr = std::shared_ptr<hepmc4::vertex<T>>;
  std::unordered_set<vertex_ptr> rel;
  v->ancestors(rel);

  size_t expected = v->status;
  
  if (rel.size() != expected) {
    std::cerr << "Vertex has wrong number of ancestors="
              << rel.size() << " expected=" << expected << "\n"
              << *v << std::endl;
    for (auto a : rel) {
      std::cerr << "<== " << *a << std::endl;
    }
    return false;
  }

  return true;
}

// -------------------------------------------------------------------
/**
   Look for descendants of a vertex.  The test fails if the number of
   descendants found does not match the expected number of
   descendants.  Note, this dependent on the depth to which we create
   the event.  The assumption here is we create up to depth 5.

   @tparam  T Floating point value type
   @param   v Vertex to investigate 
   @return  True on success
   @ingroup hepmc_test
*/
template <typename T>
bool testDescendants(std::shared_ptr<hepmc4::vertex<T>>& v)
{
  using vertex_ptr = std::shared_ptr<hepmc4::vertex<T>>;
  std::unordered_set<vertex_ptr> rel;
  v->descendants(rel);

  size_t ngen     = 8 + 4 + 2 + 1;
  size_t expected = (v->status == 0 ? ngen      :
                     v->status == 1 ? ngen-1    :
                     v->status == 2 ? 2+2*2     : 
                     v->status == 3 ? 2         :
                     0);
  
  if (rel.size() != expected) {
    std::cerr << "Vertex has wrong number of descendants="
              << rel.size() << " expected=" << expected << "\n"
              << *v << std::endl;
    for (auto d : rel) {
      std::cerr << "<== " << *d << std::endl;
    }
    return false;
  }
  return true;
}
// ===================================================================
/**
   Create an event with a loop (cycle) in it, and see if we can find it.
   
   See also example `w.cc`


   @tparam  T Floating point value type
   @return  True on success
   @ingroup hepmc_test
*/
template <typename T>
bool testLoop()
{
  bool ret = true;
  
  using builder_type=hepmc4::builder<T>;

  auto event = builder_type::event();
  auto ip    = builder_type::vertex(0,0,0,0,3);
  event->add_vertex(ip);
  
  auto proj  = builder_type::particle(0,0, 7000,7000, 2212,4);
  auto targ  = builder_type::particle(0,0,-7000,7000,-2212,4);
  auto d1    = builder_type::particle(0.750, -1.569, 32.191,32.238,  1);
  auto ubar1 = builder_type::particle(3.047,-19.0,  -54.629,57.920, -2);

  auto v1 = builder_type::interaction({proj},{d1});
  auto v2 = builder_type::interaction({targ},{ubar1});
  v1->status = 1;
  v2->status = 2;
  d1->status = 3;
  ubar1->status = 3;
  
  auto gamma = builder_type::particle(-3.813,  0.113, -1.833, 4.233,  22);
  auto w     = builder_type::particle( 1.517,-20.68, -20.605,85.925, -24);

  builder_type::interaction({d1,ubar1},{gamma,w},ip);
  
  auto d2    = builder_type::particle(-2.445, 28.816,  6.082,29.552,  1);
  auto ubar2 = builder_type::particle( 3.962,-49.498,-26.687,56.373, -2);
  
  auto v4 = builder_type::decay(w,{d2,ubar2});

  auto l = builder_type::particle(0,0,0,0,22,10);
  ip->add_outgoing(l);
  v2->add_incoming(l);

  // auto writer = hepmc4::io_factory::deduce_writer<T>("foo.hepmc3",std::cout);
  // writer->write_event(event);

  std::unordered_set<std::shared_ptr<hepmc4::vertex<T>>> relv;
  
  bool ipcycle = not ip->ancestors(relv);

  if (not ipcycle) {
    std::clog << "Didn't find cycle of IP vertex" << std::endl;
    ret = false;
  }

  std::unordered_set<std::shared_ptr<hepmc4::particle<T>>> relp;
  
  bool targcycle = not targ->descendants(relp);
  if (not targcycle) {
    std::clog << "Didn't find cycle of target" << std::endl;
    ret = false;
  }

  relp.clear();
  bool wcycle = not w->descendants(relp);
  if (wcycle) {
    std::clog << "Find cycle of W-" << std::endl;
    ret = false;
  }

  bool evcycle = event->check_cycle();
  if (evcycle) {
    std::clog << "Didn't find cycle of event" << std::endl;
    ret = false;
  }
  
  return ret;
}

// ===================================================================
/**
   Test of relatives interface.

   @tparam  T Floating point value type
   @return  Non-zero on error
   @ingroup hepmc_test
*/
template <typename T=hepmc4::default_values_type>
int testRelatives()
{
  int    ret   = 0;
  size_t depth = 5;
  auto   event = hepmc4::tests::make<T>();
  hepmc4::tests::generate(event, depth);

  for (auto p : event->particles()) {
    if (not testAncestors(p)) ret++;
    if (not testDescendants(p)) ret++;
  }
  for (auto v : event->vertices()) {
    if (not testAncestors(v)) ret++;
    if (not testDescendants(v)) ret++;
  }

  if (not testLoop<T>()) ret++;
  
  return ret;
}
                   
// ===================================================================
/**
   This is a test of relatives searches


   @ingroup hepmc_test
*/
int main()
{
  return testRelatives<>();
}
//
// EOF
//

