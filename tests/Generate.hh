#ifndef HEPMC4_TESTS_GENERATE_HH
#define HEPMC4_TESTS_GENERATE_HH
#include <HepMC3/GenEvent.h>
#include <HepMC3/GenParticle.h>
#include <HepMC3/GenVertex.h>
#include <HepMC3/GenRunInfo.h>
#include <list>
#include <numeric>

namespace HepMC3
{
  using GenEventPtr=std::shared_ptr<GenEvent>;
  using GenRunInfoPtr = std::shared_ptr<GenRunInfo>;
  
  namespace tests {
    size_t SubGenerate(GenEvent&                  event,
                       GenParticlePtr&            p,
                       size_t                             d,
                       std::list<GenParticlePtr>& leaves)
    {
      auto m  = p->momentum();
      auto m1 = m * .3;
      auto m2 = m * .7;
      auto p1 = std::make_shared<GenParticle>(m1, d,  1);
      auto p2 = std::make_shared<GenParticle>(m2, d, -1);
      auto v  = std::make_shared<GenVertex>  (FourVector());

      v->set_status(d);
      v->add_particle_in (p);
      v->add_particle_out(p1);
      v->add_particle_out(p2);

      event.add_vertex(v);
  
      leaves.emplace_back(p1);
      leaves.emplace_back(p2);

      return 2;
    }

    void Generate(GenEvent&      event,
                  GenRunInfoPtr& runInfo,
                  size_t         depth,
                  size_t         size,
                  double         off=0)
    {
      event.clear();
      event.add_attribute("MaxDepth",std::make_shared<IntAttribute>(depth));
      event.add_attribute("MaxSize", std::make_shared<IntAttribute>(size));

      if (runInfo) {
        auto names = runInfo->weight_names();
        std::vector<double> values(names.size());
        std::iota(values.begin(),values.end(), 1);
        std::transform(values.begin(),values.end(),values.begin(),
                       [off](auto x) { return x+off; });
        event.weights() = values;
      }
      else
        event.weights() = std::vector<double>{1+off,2+off};
  
      auto f1 = FourVector(0,0,  7000, 7000);
      auto f2 = FourVector(0,0,  7000,-7000);
      auto f3 = FourVector(30,30,  70,14000);
      auto b1 = std::make_shared<GenParticle>(f1, 4, 2212);
      auto b2 = std::make_shared<GenParticle>(f2, 4, 2212);
      auto o1 = std::make_shared<GenParticle>(f3, 1, 21);
      auto ip = std::make_shared<GenVertex>  (FourVector());

      ip->add_particle_in (b1);
      ip->add_particle_in (b2);
      ip->add_particle_out(o1);

      event.add_vertex(ip);
  
      std::list<GenParticlePtr> leaves{o1};

      size_t counter = 3;
      size_t level   = 1;
      bool   more    = true;

      while (level < depth and more) {
        std::list<GenParticlePtr> newLeaves;
        for (auto& p : leaves) {
          counter += SubGenerate(event, p, level, newLeaves);

          if (counter < size) continue;

          more = false;
          break;
        }
        level++;
        leaves = newLeaves;
      }

      auto l = (std::string(level >= depth ? "Depth" : "") +
                std::string(counter >= size?"Size":""));
      event.add_attribute("Limit",std::make_shared<StringAttribute>(l));
      event.add_attribute("Depth",std::make_shared<IntAttribute>(level));
      event.add_attribute("Size", std::make_shared<IntAttribute>(counter));
    }
  }
}
#endif

      
