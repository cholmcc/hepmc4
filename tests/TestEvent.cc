#include "Generate.hh"
#include <HepMC3/WriterAscii.h>
#include <HepMC3/ReaderAscii.h>
#include <HepMC3/WriterAsciiHepMC2.h>
#include <HepMC3/ReaderAsciiHepMC2.h>
#include <HepMC3/Print.h>
#include <iomanip>
#include <iostream>

template<typename Input, typename Unary>
auto comprehension(Input&& input, Unary&& lambda){
  std::vector<decltype(lambda(*input.begin()))> output;
  std::transform(input.begin(),input.end(),std::back_inserter(output),lambda);
  return output;
}


int main()
{
  size_t depth = 3;
  size_t size  = 30;
  size_t nev   = 4;
  
  HepMC3::GenEvent event;

  HepMC3::GenRunInfoPtr runInfo = nullptr;
  // auto runInfo  = std::make_shared<HepMC3::GenRunInfo>();
  if (runInfo) {
    runInfo->add_attribute("Depth",
                           std::make_shared<HepMC3::IntAttribute>(depth));
    runInfo->add_attribute("Size",
                           std::make_shared<HepMC3::IntAttribute>(size));
    runInfo->set_weight_names({"W", "A weight", "Another weight"});
    event.set_run_info(runInfo);
  }

  HepMC3::WriterAscii       writer3("output.hepmc3");
  HepMC3::WriterAsciiHepMC2 writer2("output.hepmc2");

  for (int iev = 0; iev < nev; iev++) {
    HepMC3::tests::Generate(event, runInfo, 4, 30, double(iev)/10);
    event.set_event_number(iev);

    if (runInfo) 
      runInfo->add_attribute("Counter",
                             std::make_shared<HepMC3::IntAttribute>(iev));
    HepMC3::Print::content(event);

    
    writer3.write_event(event);
    writer2.write_event(event);
  }

  writer3.close();
  writer2.close();

  return 0;
}

    
  
  
                      
  
