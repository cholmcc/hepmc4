//  Library for handling HEP MC data
//  Copyright (C) 2023  Christian Holm Christensen
//
//  This program is free software: you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation, either version 3 of
//  the License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see
//  <https://www.gnu.org/licenses/>.
//                              
/**
   @file      testCopy.cc
   @copyright (c) 2023 Christian Holm Christensen
   @brief     Test copying of events
*/
#include <iostream>
#include <iomanip>
#include "generate"
#include <hepmc4/indexed_event>

/**
   Check that two sets of attributes are the same but does not share
   memory.
   
   @param lhs  Left hand side
   @param rhs  Right hand side
   @return Non-zero on error 
   @ingroup hepmc_test
*/
int check_attributes(const hepmc4::with_attributes& lhs,
                     const hepmc4::with_attributes& rhs)
{
  int ret = 0;
  auto& alhs = lhs.attributes();
  // auto& arhs = rhs.attributes();
  for (auto na : alhs) {
    auto key = na.first;
    auto la  = na.second;
    if (not rhs.has_attribute(key)) {
      std::cerr << "RHS missing attribute " << std::quoted(key) << std::endl;
      ret++;
      continue;
    }
    auto ra = rhs.attribute(key);
    if (not ra) continue; // Shouldn't happen

    if (ra.get() == la.get()) {
      std::cerr << "Attribute " << std::quoted(key)
                << " points to same memory! "
                << la.get() << " vs " << ra.get() << std::endl;
      ret++;
    }
  }
  return ret;
}

/**
   Compare left hand side set to right hand side set.

   @tparam T    Floating point value type.
   @param  lhs  Left hand side 
   @param  rhs  Right hand side
   @param  what What is being compared  
   @return Non-zero on error 
   @ingroup hepmc_test
*/
template <typename T>
int check_set(const std::unordered_set<T>& lhs,
              const std::unordered_set<T>& rhs,
              const std::string& what)
{
  int ret = 0;
  for (auto l = lhs.begin(), r = rhs.begin();
       l != lhs.end() and r != rhs.end(); ++l, ++r) {
    if (l->get() == r->get()) {
      std::cerr << what << " points to same memory!" << std::endl;
      ret++;
    }
    int rr =  check_attributes(*l->get(), *r->get());
    ret    += rr;
    if (rr > 0) {
      std::cerr << "While comparing\n  "
                << *(l->get()) << " vs\n  "
                << *(r->get()) << std::endl;
    }
  }
  return ret;
}

/**
   Compare left hand side set to right hand side set.

   @tparam T    Floating point value type.
   @param  lhs  Left hand side 
   @param  rhs  Right hand side
   @param  what What is being compared  
   @return Non-zero on error 
   @ingroup hepmc_test
*/
template <typename T>
int check_list(const std::list<T>& lhs,
               const std::list<T>& rhs,
               const std::string& what)
{
  int ret = 0;
  for (auto l = lhs.begin(), r = rhs.begin();
       l != lhs.end() and r != rhs.end(); ++l, ++r) {
    if (l->get() == r->get()) {
      std::cerr << what << " points to same memory!" << std::endl;
      ret++;
    }
    int rr =  check_attributes(*l->get(), *r->get());
    ret    += rr;
    if (rr > 0) {
      std::cerr << "While comparing\n  "
                << *(l->get()) << " vs\n  "
                << *(r->get()) << std::endl;
    }
  }
  return ret;
}

/**
   In this check, we generate an event and then copy it.  We then
   check that

   - All memory managed by the two events do not overlap - i.e., that
     the copy was deep.
   
   - That the two events compare equal.

   @tparam T    Floating point value type.
   @return Non-zero on error 
   @ingroup hepmc_test
*/
template <typename T=hepmc4::default_values_type>
int testCopy()
{
  int ret = 0;
  using builder_type = hepmc4::builder<T>;
  auto  lhs          = hepmc4::tests::make<T>();
  hepmc4::tests::generate(lhs);
  auto  rhs          = builder_type::event(*lhs); // Deep copy

  // Check that attributes are unique 
  ret += check_attributes(*lhs,*rhs);
  // Check that run infos are unique 
  if (lhs->run_info().get() == rhs->run_info().get()) {
    std::cerr << "Run infos point to same memory" << std::endl;
    ret++;
  }
  ret += check_attributes(*(lhs->run_info()),*(rhs->run_info()));

  // Sort events by depth so we may compare particles and vertices
  hepmc4::indexed_event<float> ilhs{hepmc4::depth_indexer<T>()(lhs)};
  hepmc4::indexed_event<float> irhs{hepmc4::depth_indexer<T>()(rhs)};
  
  // Check that vertices and particles are unique 
  ret += check_list(ilhs.vertices(),  irhs.vertices(),  "Vertex");
  ret += check_list(ilhs.particles(), irhs.particles(), "Particle");
  // ret += check_set(lhs->vertices(),  rhs->vertices(),  "Vertex");
  // ret += check_set(lhs->particles(), rhs->particles(), "Particle");


  // Now check that the events are considered equal
  // lhs->verbose_eq = true;
  bool eq = lhs->operator==(*rhs);
  std::cout << "Events are " << (eq ? "" : "NOT ") << "equal" << std::endl;
  if (not eq) {
    std::cout << "LHS:\n" << *lhs << "\n" << "RHS:\n" << *rhs << std::endl;
    ret++;
  }

  return ret;
}

/**
   In this check, we generate an event and then copy it.
   
   @ingroup hepmc_test
*/
int main()
{  
  return testCopy<>();
}
//
// EOF
//

