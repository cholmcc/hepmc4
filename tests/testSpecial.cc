//  Library for handling HEP MC data
//  Copyright (C) 2023  Christian Holm Christensen
//
//  This program is free software: you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation, either version 3 of
//  the License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see
//  <https://www.gnu.org/licenses/>.
//                              
/**
   @file      testSpecial.cc
   @copyright (c) 2023 Christian Holm Christensen
   @brief     Test special attributes
*/
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <hepmc4/compat>
#include <hepmc4/cross_section>
#include <hepmc4/heavy_ion>
#include <hepmc4/attribute>
#include <hepmc4/details>

//====================================================================
/**
   Test special attributes

   @tparam S Special attribute type 
   @ingroup hepmc_test
*/
template <typename S>
int testSpecial(const std::string& input, std::shared_ptr<S> ptr)
{
  using ptr_type = std::shared_ptr<S>;
  
  std::stringstream in(input);
  hepmc4::attribute<ptr_type> attribute(ptr);
  attribute.parse(in);
  //  S& o = *(ptr.get());
  // std::cout << o << std::endl; 
  std::stringstream out;
  attribute.format(out);

  size_t      newline  = input.find('\n');
  std::string expected = input.substr(0,newline); // Remove newline
  size_t      hash     = expected.find('#');
  expected             = expected.substr(0,hash); // Remove comment
  hepmc4::rtrim(expected,[](int ch) {
    return std::isalpha(ch) or std::isspace(ch);});

  std::string got(out.str());
  hepmc4::rtrim(got,std::isspace); // Remove trailing white-space
  
  if (got != expected) {
    std::cerr << "=== Input: " << std::quoted(input)    << "\n"
              << "Got:       " << std::quoted(got)      << '\n'
              << "Expected:  " << std::quoted(expected) << std::endl;
    return 1;
  }
  return 0;
}

//--------------------------------------------------------------------
/**
   Test special attributes

   @tparam  T      Floating point value type
   @ingroup hepmc_test
*/
template <typename T>
int testCrossSection()
{
  auto test = [](const std::string& s) -> int {
    auto ptr = std::make_shared<hepmc4::cross_section<T>>();
    return testSpecial<hepmc4::cross_section<T>>(s,ptr); };
  
  int ret = 0;
  test("1.23 foo bar baz");
  test("1.23 0.123 foo bar baz");
  test("1.23\nfoo bar baz");
  test("1.23 0.123 10\nfoo bar baz");
  ret += test("1.23 0.123 10 20\nfoo bar baz");
  test("1.23 0.123 10 20 2.34\nfoo bar baz");
  ret += test("1.23 0.123 10 20 2.34 0.234  # foo\nfoo bar baz");
  ret += test("1.23 0.123 10 20 2.34 0.234 3.45 0.345 # foo\nfo ba");

  return ret;
}
//--------------------------------------------------------------------
/**
   Test special attributes

   @tparam  T      Floating point value type
   @ingroup hepmc_test
*/
template <typename T>
int testHeavyIon()
{
  auto test = [](const std::string& s) -> int {
    auto ptr = std::make_shared<hepmc4::heavy_ion<T>>();
    return testSpecial<hepmc4::heavy_ion<T>>(s,ptr);
  };

  hepmc4::heavy_ion<T> hi;
  std::cout << hi << std::endl;
  
  int ret = 0;
  ret += test("10 20 20 50 10 10 2 3 4 1.2 3.4 5.6 7.8 9.1"); 
  ret += test("v0 10 20 20 50 10 10 2 3 4 1.2 3.4 5.6 7.8 9.1"); 
  ret += test("v1 10 20 20 50 2 3 4 1.2 3.4 5.6 7.8 9.1 5 5 5 5 0 0"); 
  ret += test("v1 10 20 20 50 2 3 4 1.2 3.4 5.6 7.8 9.1 5 5 5 5 "
              "2 1 2.3 4 5.6 3 0 1.2 1 3.4 2 5.6"); 
  ret += test("v1 10 20 20 50 2 3 4 1.2 3.4 5.6 7.8 9.1 5 5 5 5 "
              "0 3 0 1.2 1 3.4 2 5.6"); 
  
  return ret;
}  

// -------------------------------------------------------------------
/**
   Test writing and reading events in a specific format.

   An event is generated and written to a string stream.

   The event is read back from the string stream.

   The two events, before writing and after reading, are compared. 

   @tparam  T      Floating point value type
   @return  Non-zero on error
   @ingroup hepmc_test
*/
template <typename T=hepmc4::default_values_type>
int testSpecialAll()
{
  int ret = 0;
  ret += testCrossSection<T>();
  ret += testHeavyIon<T>();
  
  return ret;
}

//====================================================================
/**
   Test cross-section, heavy_ion parsing, etc
*/
int main()
{
  return testSpecialAll<>();
}

    
//
// EOF
//
