//  Library for handling HEP MC data
//  Copyright (C) 2023  Christian Holm Christensen
//
//  This program is free software: you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation, either version 3 of
//  the License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see
//  <https://www.gnu.org/licenses/>.
//                              
/**
   @file      testEvent.cc
   @copyright (c) 2023 Christian Holm Christensen
   @brief     Test events
*/
#include <iostream>
#include <iomanip>
#include "generate"
#include <hepmc4/io_factory>

/**
   Set proper status codes on particles for pruning.

   @tparam  T      Floating point value type
   @param   event  Event to prune 
   @ingroup hepmc_test
*/
template <typename T>
void fix_generate(hepmc4::event_ptr<T>& event)
{
  event->clear();
  
  hepmc4::tests::generate(event);
  for (auto& particle : event->particles())
    switch (particle->status) {
    case 1: particle->status = 3; break;
    case 2: particle->status = 3; break;
    case 3: particle->status = 2; break;
    case 4: if (not particle->termination()) particle->status = 1;
      break;
    }
}

/**
   Test of pruning of events.


   @tparam  T Floating point value type
   @return  Non-zero on error
   @ingroup hepmc_test
*/
template <typename T=hepmc4::default_values_type>
int testPrune()
{
  const std::string outname = "prune.hepmc3";
  auto output = hepmc4::io_factory::open_output(outname,std::cout);
  auto writer = hepmc4::io_factory::deduce_writer<T>(outname,
                                                     output.stream);

  auto event = hepmc4::tests::make<T>();
  fix_generate(event);
  
  writer->write_event(event);

  // The selection of particles
  auto select = [](const hepmc4::particle_ptr<T>& particle) {
    switch (particle->status) {
    case hepmc4::particle<T>::final_state: // Final st
    case hepmc4::particle<T>::decayed:     // Decayed
    case hepmc4::particle<T>::beam:        // Beam
      return true;
    }
    // if (particle->pid() == 9902210) return true;
    return false;
  };

  fix_generate(event);
  event->prune(select);
  writer->write_event(event);

  int ret = 0;
  if (event->vertices().size() != 9) {
    std::clog << "Unexpected number of vertices: "
              << event->vertices().size() << ", expected 9"
              << std::endl;
    ret++;
  }
  if (event->particles().size() != 2*8+8+2) {
    std::clog << "Unexpected number of particles: "
              << event->particles().size() << ", expected " << 2*8+8+2
              << std::endl;
    ret++;
  }
  return ret;
}

/**
   Test pruning of an event.

   @ingroup hepmc_test
*/
int main()
{
  return testPrune<>();
}
//
// EOF
//

