## Changes from HepMC version 3 

- Get rid of particle and vertex `id`s. 

  - These are not really needed when the event is in-memory, and
    caused a lot of head-aches and superfluous book-keeping. 
    
  - Unique identifiers are _only_ established for output and used
    during input. 
    
    The class [`hepmc4::indexed_event`](hepmc4/indexed_event) is a
    container of such transient IDs, and uses a
    [`hepmc4::indexer_base`](hepmc4/indexed_event) algorithm to create
    these IDs.  Two such algorithms are defined 
    
    - [`hepmc4::simple_indexer`](hepmc4/indexed_event) which simply
      numbers the vertices and particles consecutively.  This can be
      used when the output format does not rely on a specific ordering
      of the particles and vertices
      ([`hepmc4::ascii4`](hepmc4/ascii4),
      [`hepmc4::protobuf`](hepmc4/protobuf),
      etc.). 
      
    - [`hepmc4::depth_indexer`](hepmc4/indexed_event) which orders
      vertices and particles by their _depth_ in the event hierarchy.
      I.e, a primary collision vertex will have depth 0, and later
      vertices (in the event history) will have depth larger
      than 0. This algorithm is used for formats that rely on a
      specific ordering of vertices and particles
      ([`hepmc4::ascii2`](hepmc4/ascii2),
      [`hepmc4::ascii3`](hepmc4/ascii3),
      [`hepmc4::hepevt`](hepmc4/hepevt),
      [`hepmc4::root`](hepmc4/root)). 
      
- Overhaul of Event, run information, particle, and vertex attributes.

  - Attributes are stored with the object they pertain to.  This is
    done through the base class
    [`hepmc4::with_attributes`](hepmc4/with_attributes).
  
    This allows for fast addition and removal of both attributes and
    event objects (vertex and particle), since we do not need to
    reference the (possibly) owning event. 
    
    Also, since identifiers are no longer used, this makes it very
    easy to keep attributes and objects in sync, and allows for a more
    stream-lined input and output format (i.e., `ascii4`). 
    
  - Attributes are encoded to and decoded from strings via _traits_.
    That allows us to use almost any kind of object as an attribute as
    long as we have an appropriate trait for that type of object. 
    
  - Attributes can be set by _value_ (rather than pointer to container
    of value), and attribute values can be retrieved directly. 
    
- Template classes for data classes. 

  - All data classes (event, particle, vertex, etc.) are parameterised
    on the precision of the floating-point values stored (e.g.,
    `double` or `float`). 
    
  - Calculations on four-vectors are _always_ promoted to at least
    double precision. 
    
  - This allows the user to decide what kind of precision is needed
    for the users application.
    
  - As a consequence of this, the library is _header-only_. 

- Data is in general `public` 

  - Data members of data classes (events, particles, vertices, etc.)
    are generally `public`.  For example, the 4-momentum of a particle
    is accessible as the member `momentum`. 
     
  - Only when other data relies on a data member (e.g., the production
    and termination vertex of a particle, incoming and outgoing
    particles of a vertex, and similar) are these members `protected`
    or `private`. 
    
    That is, when there's little or no chance of corruption of the
    _structure_ of an event, then those data members are `public`. 
    
- STL-like naming conventions. 

  - Classes have lower-case names with underscores separating words
    (rather than _CamelCase_). 
  - `private` and `proteced` members are prefixed by an underscore
    `_`. 
  - Incoming particles of a vertex are accessed through `add_incoming`
    and `incoming`. 
  - Outgoing particles of a vertex are accessed through `add_outgoing`
    and `outgoing`. 
    
  
- Use C++ features 

  - The code uses newer C++ features, rather than f.ex. C string
    parsing as much as possible. 
    
  - To ensure compatibility with older C++ standards (pre C++17),
    [compatiblity](hepmc4/compat) code has been introduced. 
    
- Readers and writers used `std::iostream` 

  - Input is read from `std::istream`. 
  - Output is written to `std::ostream`. 
  - The readers and writers rely on standard C++ buffering techniques,
    though the file input buffer size can be customised (defaults to
    256kb). 
  - Functions to deduce input and output formats are provided. 
  - `std::iostream` input/output does seem to be inherently a bit
    slower than C-like `scanf` or the like. 
  
- New ASCII format (ASCII version 4 format)

  - This format is rather simple: 
  
    - Run information comes first, followed by list of events 
    - Each event has some header lines _immediately_ followed by the
      attributes of the event 
    - Then, for each event follows the list of vertices. 
      - If a vertex has no attributes, status of zero, and zero
        position, then it may be omitted from the output entirely. 
      - If not, then a vertex is a single identifying line (ID,
        status, position) _immediately_ followed by any attributes of
        the vertex. 
    - Finally, the list of particles is given. 
      - A particle specification is a single line that specifies the
        ID of the production and termination vertices (both possibly
        0), pdg code, status, momentum, and mass, _immediately_
        followed by attributes of the particle. 
      - If a particle refers to a vertex that was not listed in the
        above list of vertices, then that vertex will be created on
        input. 
        
  - The schema of the format makes it very efficient to read and
    write. 
    
    - On output, we need only loop over vertices and particles once,
      and they can be output in any order.
      
    - Particles need no be assign an identifier. 
    
    - On input, we can assign attributes to objects _immediately_. 
    
    - On input, vertices can be added to the event _immediately_. 
    
    - A simple cache of vertices can be used when reading in particles
      and restoring production and termination vertices. 

- Compressed ASCII files can be read or written.  The code,
  optionally, uses [Boost iostream](https://boost.org) library to
  enable reading and writing compressed (`gzip`, `bunzip2`, and `zip`)
  ASCII formats.   This is transparently handled by the
  `hepmc4::io_factory` helper functions `open_input` and
  `open_output`. 
  
- New ROOT output format 

  - Events are output to a `TTree` with a branch of `TClonesArray` of
    `TParticles`.  These are standard ROOT objects, which means the
    generated data file is readable with all ROOT installations
    without the need for custom code. 
    
  - Auxiliary information, such as cross-section, PDF, heavy-ion
    geometry, and so on, is stored in simple branches of fundamental
    leaves.  Again, this makes is very easy for anyone with ROOT
    installed to read back the written data. 

- New writer of GraphViz event graphs 

  - Events can be illustrated by using the `hepmc4::dot::writer`
    class.  This will write every event as
    [GraphViz](https://graphviz.org) graphs that can be rendered using
    regular GraphViz tools. 

- Pythonic Python interface 

  - The [Python](https://python.org) interface has been overhauled and
    made much more Pythonic.  For example,
    
    - The `Reader` and `Writer` classes can be used as context
      managers. 
    - `Reader` is iterable so that we can iterate over that class to
      get the events. 
    - Properties are used where they make sense 
    - Attributes are indexed by the `[]` operator. 
    
  - The [`pybind11`](https://pybind11.readthedocs.io/en/stable/) code
    is hand-crafted.  This allows for much more flexibility than the
    auto-generated code from
    [`binder`](https://cppbinder.readthedocs.io/en/latest/).  The
    library is small enough that we can do this (some 1,000 lines of
    code in [`python/pyhepmc.cc`](python/pyhepmc.cc).

- Compatibility 

  - ASCII formats 2 (aka, `IO_GenEvent`) and 3 are fully supported. 
  - HEPEVT format is fully supported.
  - HepMC3 ProtoBuf format is fully supported. 
  - Old ROOT formats are _not_ supported. 
  
  
