//  Library for handling HEP MC data
//  Copyright (C) 2023  Christian Holm Christensen
//
//  This program is free software: you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation, either version 3 of
//  the License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see
//  <https://www.gnu.org/licenses/>.
//                              
/**
   @file      compare.cc
   @copyright (c) 2023 Christian Holm Christensen
   @brief     Compare event files
*/
#include <hepmc4/io_factory>
   
/** Do nothing */
template <typename T> void nothing(hepmc4::event_ptr<T>&) {}

//====================================================================
/**
   Reads in events from two readers and compares the events one-by-one.
*/
template <typename T>
bool compare(std::shared_ptr<hepmc4::reader<T>>& reader1,
             std::shared_ptr<hepmc4::reader<T>>& reader2,
             int maxEvents=-1)
{
  using builder_type = hepmc4::builder<T>;
  auto event1  = builder_type::event();    // empty
  auto event2  = builder_type::event();    // empty

  using middle_func=decltype(&nothing<T>);
  middle_func middle = nothing<T>;
  
  if (std::dynamic_pointer_cast<hepmc4::hepevt::reader<T>>(reader1) or
      std::dynamic_pointer_cast<hepmc4::hepevt::reader<T>>(reader2))
    middle = hepmc4::hepevt::bare_bones<T>;
  else if (std::dynamic_pointer_cast<hepmc4::ascii2::reader<T>>(reader1) or
           std::dynamic_pointer_cast<hepmc4::ascii2::reader<T>>(reader2))
    middle = hepmc4::ascii2::bare_bones<T>;
  else if (std::dynamic_pointer_cast<hepmc4::root::reader<T>>(reader1) or
           std::dynamic_pointer_cast<hepmc4::root::reader<T>>(reader2))
    middle = hepmc4::root::bare_bones<T>;
  
  auto more = [maxEvents](int counter) {
    return maxEvents <= 0 or counter < maxEvents; };

  int counter = 0;
  int ret     = 0;
  while (more(counter++) and
         reader1->read_event(event1) and
         reader2->read_event(event2)) {
    if (counter % 100 == 0)
      std::clog << "Event # "
                << std::setw(6) << event1->number << " "
                << std::setw(6) << event2->number << std::endl;

    // std::clog << *event1 << std::endl;
    // Clean events by reader type (most restrictive) before comparing.
    // - HEPEVT does not store _any_ attributes
    // - Ascii2 (aka IO_GenEvent) stores sub-set of attributes 
    (*middle)(event1);
    (*middle)(event2);

    // Make the comparisons verbose 
    event1->verbose_eq = true; // false;
    bool eq = (*event1 == *event2);
    if (not eq) {
      std::clog << "Events " << event1->number << " differ\n"
        // << "=== Event 1 ===\n"
        // << *event1 << "\n"
        // << "=== Event 2 ===\n"
        // << *event2
                << std::endl;
      ret++;
      // break;
    }
  }

  return ret;
}
    

//====================================================================
void
usage(const std::string& progname, std::ostream& out=std::cout)
{
  out << "Usage: " << progname << " [OPTION] [INPUT1 INPUT2]\n\n"
      << "Options:\n"
      << "  -h               This help\n"
      << "  -1 INPUT1        Specify first input source\n"
      << "  -2 INPUT2        Specify second input source\n"
      << "  -n NEV           Limit the number of events compared to NEV\n"
      << "Inputs can be in any format recognised by HepMC4, including\n"
      << "- Asciiv2 (aka IO_GenEvent)\n\n"
      << "- Asciiv3\n"
      << "- Asciiv4\n"
      << "- HEPEVT\n"
      << "- ROOT (if compiled in)\n"
      << "- ProtoBuf (if compiled in)\n\n"
      << "Inputs can be compressed using gzip, bzip2, or zip."
      << std::endl;
}


  
//====================================================================
int
main(int argc, char** argv)
{
  std::string input1FileName("");
  std::string input2FileName("");
  int         maxEvents = 0;

  for (int i = 1; i < argc; ++i) {
    if (argv[i][0] == '-') {
      switch (argv[i][1]) {
      case '1':   input1FileName  = argv[++i];            break;
      case '2':   input2FileName  = argv[++i];            break;
      case 'n':   maxEvents       = std::stoi(argv[++i]); break;
      case 'h':   usage(argv[0]);                        return 0;
      default:
        std::cerr << argv[0] << ": Unknown option: " << std::quoted(argv[i])
                  << std::endl;
        return 1;
      }
      continue;
    }
    if (input1FileName.empty()) input1FileName = argv[i];
    else                        input2FileName = argv[i];
  }

  auto input1Stream  = hepmc4::io_factory::open_input (input1FileName);
  auto input2Stream  = hepmc4::io_factory::open_input (input1FileName);
  if (&input1Stream.stream == &input2Stream.stream) 
    throw std::runtime_error("Cannot read both sets of "
                             "events from the same stream");

  auto reader1 = hepmc4::io_factory::deduce_reader<float>(input1FileName,
                                                          input1Stream);
  auto reader2 = hepmc4::io_factory::deduce_reader<float>(input2FileName,
                                                          input2Stream);

  int ret = compare(reader1, reader2, maxEvents);

  reader1.reset();
  reader2.reset();
  
  return ret;
}
//
// EOF
//

  

