//  Library for handling HEP MC data
//  Copyright (C) 2023  Christian Holm Christensen
//
//  This program is free software: you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation, either version 3 of
//  the License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see
//  <https://www.gnu.org/licenses/>.
//                              
/**
   @file      convert.cc
   @copyright (c) 2023 Christian Holm Christensen
   @brief     Convert event file to another format
*/
#include <hepmc4/io_factory>

//====================================================================
template <typename T>
bool convert(std::shared_ptr<hepmc4::reader<T>>& reader,
             std::shared_ptr<hepmc4::writer<T>>& writer,
             int maxEvents=-1)
{
  using builder_type = hepmc4::builder<T>;
  auto event  = builder_type::event();    // empty

  auto more = [maxEvents](int counter) {
    return maxEvents <= 0 or counter < maxEvents; };

  int counter = 0;
  while (more(counter++) and reader->read_event(event)) {
    if (counter % 100 == 0)
      std::clog << "Event # " << std::setw(6) << event->number << std::endl;

    writer->write_event(event);
  }

  return true;
}
    

//====================================================================
void
usage(const std::string& progname, std::ostream& out=std::cout)
{
  out << "Usage: " << progname << " [-n NEV] [INPUT [OUTPUT]]\n\n"
      << "Converts INPUT to OUTPUT, using different formats.\n"
      << "INPUT format is deduced from file content, while OUTPUT format "
      << "is deduced by file name 'extension'.\n\n"
      << "  Extension | Format\n"
      << "  ---------------------------------\n"
      << "  .hepmc2   | Asciiv2 (IO_GenEvent)\n"
      << "  .hepmc3   | Asciiv3 \n"
      << "  .hepmc4   | Asciiv4 \n"
      << "  .hepevt   | HEPEVT\n"
      << "  .root     | Single ROOT file*\n"
      << "  .list     | List or ROOT files*\n"
      << "  .proto    | ProtoBuf**\n\n"
      << "*Only if compiled with HEPMC4_HAS_ROOT\n"
      << "**Only if compiled with HEPMC4_HAS_PROTOBUF\n\n"
      << "Both input and output may be compressed if compiled with \n"
      << "HEPMC4_HAS_BOOST_IOSTREAMS\n\n"
      << "  Extension | Compression \n"
      << "  -----------------------\n"
      << "  .gz       | GZip\n"
      << "  .bz2      | BZip2\n"
      << "  .z,.zip   | ZLib\n\n"
      << "If either INPUT or OUTPUT not specified, read from standard input \n"
      << "or write to standard output, respectively."
      << std::endl;
}


  
//====================================================================
int
main(int argc, char** argv)
{
  std::string inputFileName("");
  std::string outputFileName("");
  int         maxEvents = 0;

  for (int i = 1; i < argc; ++i) {
    if (argv[i][0] == '-') {
      switch (argv[i][1]) {
      case 'i':   inputFileName  = argv[++i];            break;
      case 'o':   outputFileName = argv[++i];            break;
      case 'n':   maxEvents      = std::stoi(argv[++i]); break;
      case 'h':   usage(argv[0]);                        return 0;
      default:
        std::cerr << argv[0] << ": Unknown option: " << std::quoted(argv[i])
                  << std::endl;
        return 1;
      }
      continue;
    }
    if (inputFileName.empty()) inputFileName = argv[i];
    else                       outputFileName = argv[i];
  }

  auto inputStream  = hepmc4::io_factory::open_input (inputFileName);
  auto outputStream = hepmc4::io_factory::open_output(outputFileName);

  outputStream.stream << std::setprecision(12) << std::scientific;
  
  auto reader = hepmc4::io_factory::deduce_reader<float>(inputFileName,
                                                         inputStream);
  auto writer = hepmc4::io_factory::deduce_writer<float>(outputFileName,
                                                         outputStream);

  convert(reader, writer, maxEvents);

  reader.reset();
  writer.reset();
  
  return 0;
}

  

//
// EOF
//
