# Examples of HepMC4 usage 

- [`generator`](generator) A simple abstract interface of a generator,
  plus a simple implementation that builds a $`W^-`$ events. 
  
- [`compare.cc`](compare.cc)  Read events from two sources and compare
  the events one-by-one. 
- [`convert.cc`](convert.cc) Read in events from one source, of any
  supported input format, and write them out, to any supported format. 
- [`prune.cc`](prune.cc) Read events from input and _prune_ them.   By
  pruning, we mean only keep particles that satisfy a criteria, and
  remove all others.   Also, as part of the process, short-circuit
  vertices where intermediate particles are removed. 
  
  The default pruning is of particles that are _not_ either
  - beam particles, 
  - Decayed, _nor_
  - Final state particles. 
  
  Command line options allows tuning of which particles to keep,
  either selected by particle stats (as above) or by particle type
  identifier (PDG code). 
  
- [`pythia.cc`](pythia.cc)  Example of generating
  [Pythia8](https://pythia.org) events and writing them to HepMC
  format.  The event generator is by default set up to produce
  proton-proton collisions.  However, if the program executed is _not_
  named `pythia` then the program will use the `Angantyr` heavy-ion
  model to produce events.  This can be accomplished by f.ex. 
  
      ln -s pythia angantyr 
      ./angantyr 
      
  Various command line options customises the event generation
  program, including the option to read settings from a configuration
  file. 
  
- [`w.cc`](w.cc) Example of generating a $`W^-`$ event and writing it
  to HepMC output.  This uses [`generator`](generator).
  

## Testing 

Run 

    make test 
    
to generate an event file (`all.orig.hepmc3` using
[`pythia.cc`](pythia.cc)), and the convert it to other supported
formats, and finally comparing against the original file. 


