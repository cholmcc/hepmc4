//  Library for handling HEP MC data
//  Copyright (C) 2023  Christian Holm Christensen
//
//  This program is free software: you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation, either version 3 of
//  the License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see
//  <https://www.gnu.org/licenses/>.
//                              
/**
   @file      prune.cc
   @copyright (c) 2023 Christian Holm Christensen
   @brief     Prune events
*/
#include <hepmc4/io_factory>

//====================================================================
/**
   On-the-fly calculation of mean and variance

   @param x    Next obersavtion
   @param n    Number of observations so far
   @param mean Current, and updated mean
   @param var  Current, and updated variance
   @param ddof Delta degrees of freedom 
*/
template <typename T>
void update(const T& x, size_t& n, T& mean, T& var, size_t ddof=1)
{
  if (n <= 0) {
    mean = x;
    var  = 0;
    n    = 1;
  }
  n++;
  T n1 =  T(1. / n);
  T dx =  x - mean;
  mean += n1 * dx;
  var  *= T(n - 1) / n;
  var  += n1 * dx * (x - mean);
  var  *= T(n) / (n - ddof);
}

//====================================================================
/**
   Round @a x to @f$10^{-n}@f$

   @param x  @f$ x@f$
   @param n  @f$ n@f$
   return @f$x@f$ rounded to @f$10^{-n}@f$
*/
template <typename T>
T round(const T& x, int n=0)
{
  T   tens = std::pow(10,-n);
  T   w    = std::floor(100 * std::abs(x) / tens + .00001);
  int m    = int(w / 100);
  int nxt  = int(w) % 100;
  m        = ((nxt > 50) or (nxt == 50 and m % 2 == 1) ? m+1 : m);
  return std::copysign(m * tens, x);
}

//====================================================================
/**
   Round the result @a x with error @a e to @a nsign significant
   digits.  The uncertainty @f$ e@f$ is rounded to @f$ n@f$
   significant digits (typically 1), and @f$ x @f$ is then rounded to
   the same precision.

   @param x   Result
   @param e     Uncertainty
   @param nsign Number of significant digits
   @return tuple of rounded value, rounded uncertainty, and precision
*/

template <typename T>
std::tuple<T,T,int>
round_result(const T& x, const T& e, int nsign=1)
{
  // Deduce exponent to use, and round to that. 
  auto inner = [nsign](const T& xx, const T& ee) {
    T   ae   = std::abs(ee);
    int emin = int(std::ceil(std::log10(ae)+1e-15)) - nsign;
    return std::make_pair(round(xx,-emin), emin);
  };

  auto re = inner(e, e);
  auto rx = inner(x, re.first);

  return std::make_tuple(rx.first, re.first, std::max(-rx.second,0));
}

//====================================================================
/**
   Format the result @a x with error @a e to @a nsign significant digits.
   
   @param x   Result
   @param e     Uncertainty
   @param nsign Number of significant digits
   @return String with rounded value, rounded uncertainty, to found precision.
*/
template <typename T>
std::string format_result(const T& mean, const T& err, int nsign=1)
{
  auto res  = round_result(mean, err, nsign);
  std::stringstream s;
  s << std::setprecision(std::get<2>(res))
    << std::fixed
    << std::get<0>(res) << " +/- " << std::get<1>(res);
  return s.str();
}
    
//====================================================================
/**
   Calculate final result and return a formatted version

   @param mean   Mean
   @param var    Variance
   @param n      Number of observations
*/
template <typename T>
std::string result(const T& mean, const T& var, size_t n)
{
  return format_result(mean, std::sqrt(var/n));
}

//====================================================================
/**
   Prune events read via @a reader and write out via @a writer.

   The particles _kept_ are those particles whos'

   - status is one of the status code passed in @a status, or
   - pid (PDG code) is one of the codes passed in @a pdgs

   All other particles are removed from the event, and associated
   vertices are short-circuited.

   @param reader    The reader to use
   @param writer    The writer to use
   @param status    Select particles with one of these status codes
   @param pdgs      Select particles with one of these PID (PDG number) codes
   @param maxEvents Maximum number of events to process
   @param verbose   If true, write out status
   @return true on success
*/
template <typename T>
bool prune(std::shared_ptr<hepmc4::reader<T>>& reader,
           std::shared_ptr<hepmc4::writer<T>>& writer,
           std::unordered_set<int>             status,
           std::unordered_set<int>             pdgs,
           int                                 maxEvents=-1,
           bool                                verbose=false)
{
  using builder_type = hepmc4::builder<T>;
  auto event  = builder_type::event();    // empty

  auto more = [maxEvents](int counter) {
    return maxEvents <= 0 or counter < maxEvents; };

  // The selection of particles
  auto select = [status,pdgs](const hepmc4::particle_ptr<T>& particle) {
    if (status.count(particle->status)) return true;
    if (pdgs  .count(particle->pid))    return true;
    return false;
  };

  if (verbose) 
    std::clog << std::setw(6) << " " << " | "
              << std::setw(6+1+5) << "Before" << " | "
              << std::setw(6+1+5) << "After" << " | "
              << std::setw(4+1+1+4+1) << "Reduction" << "\n"
              << std::setw(6)  << "Event" << " | "
              << std::setw(6)  << "Npart" << " " 
              << std::setw(5)  << "Nvert" << " | " 
              << std::setw(6)  << "Npart" << " " 
              << std::setw(5)  << "Nvert" << " | " 
              << std::setw(5)  << "Npart" << " " 
              << std::setw(5)  << "Nvert" << "\n"
              << "-------+--------------+--------------+-------------"
              << std::endl;

  T   meanRP = 0, varRP = 0, meanRV = 0, varRV = 0;
  size_t nRP = 0, nRV = 0;
  int counter = 0;
  while (more(counter++) and reader->read_event(event)) {
    size_t n_part_before = event->particles().size();
    size_t n_vert_before = event->vertices() .size();

    event->prune(select);
    
    size_t n_part_after = event->particles().size();
    size_t n_vert_after = event->vertices() .size();

    if (verbose) {
      float  r_part       = float(n_part_after)/n_part_before;
      float  r_vert       = float(n_vert_after)/n_vert_before;

      update((1-r_part)*100, nRP, meanRP, varRP);
      update((1-r_vert)*100, nRV, meanRV, varRV);
      
      std::clog << std::setw(6) << event->number << " | "
                << std::setw(6) << n_part_before << " "
                << std::setw(5) << n_vert_before << " | "
                << std::setw(6) << n_part_after << " "
                << std::setw(5) << n_vert_after << " | "
                << std::setw(4) << int((1-r_part)*100) << "% " 
                << std::setw(4) << int((1-r_vert)*100) << "%"
                << std::endl;
    }
    writer->write_event(event);
  }
  if (verbose) 
      std::clog << std::setw(6) << "Mean" << " | "
                << " (" << result(meanRP,varRP,nRP) << ")% "
                << " (" << result(meanRV,varRV,nRV) << ")%"
                << std::endl;

  return true;
}
    

//====================================================================
void
usage(const std::string& progname, std::ostream& out=std::cout)
{
  out << "Usage: " << progname << " [OPTIONS] [INPUT [OUTPUT]]\n\n"
      << "Options:\n"
      << "  -h             This help\n"
      << "  -i INPUT       Input file\n"
      << "  -o OUTPUT      Output file\n"
      << "  -n MAX         Max number of events\n"
      << "  -s STATUS      Status code to keep\n"
      << "  -p PDG         Particle PDG code to keep\n"
      << "  -v             Be verbose\n\n"
      << "Prunes (removes particles and vertices) events read from  INPUT "
      << "and writes the reduced events to OUTPUT.\n\n"
      << "Particles are select for keeping based on their status (-s) or "
      << "PDG code (-p).\n"
      << "By default, final state (status=1), decayed (status=2), and beam "
      << "(status=4) particles are kept\n."
      << "The options -s and -p selects which particles to keep.  These option can be given multiple times.\n"
      << std::endl;
}


  
//====================================================================
int
main(int argc, char** argv)
{
  std::string             inputFileName("");
  std::string             outputFileName("");
  int                     maxEvents = 0;
  std::unordered_set<int> status;
  std::unordered_set<int> pdgs;
  bool                    verbose = false;

  for (int i = 1; i < argc; ++i) {
    if (argv[i][0] == '-') {
      switch (argv[i][1]) {
      case 'i':   inputFileName  = argv[++i];            break;
      case 'o':   outputFileName = argv[++i];            break;
      case 'n':   maxEvents      = std::stoi(argv[++i]); break;
      case 's':   status.insert(std::stoi(argv[++i]));   break;
      case 'p':   pdgs  .insert(std::stoi(argv[++i]));   break;
      case 'v':   verbose = true;                        break;
      case 'h':   usage(argv[0]);                        return 0;
      default:
        std::cerr << argv[0] << ": Unknown option: " << std::quoted(argv[i])
                  << std::endl;
        return 1;
      }
      continue;
    }
    if (inputFileName.empty()) inputFileName = argv[i];
    else                       outputFileName = argv[i];
  }

  if (status.size() == 0 and pdgs.size() == 0)
    status = {
      hepmc4::particle<float>::final_state,
      hepmc4::particle<float>::decayed,
      hepmc4::particle<float>::beam};
  
      
  auto inputStream  = hepmc4::io_factory::open_input (inputFileName);
  auto outputStream = hepmc4::io_factory::open_output(outputFileName);

  outputStream.stream << std::setprecision(12) << std::scientific;
  
  auto reader = hepmc4::io_factory::deduce_reader<float>(inputFileName,
                                                         inputStream);
  auto writer = hepmc4::io_factory::deduce_writer<float>(outputFileName,
                                                         outputStream);

  prune(reader, writer, status, pdgs, maxEvents, verbose);

  reader.reset();
  writer.reset();
  
  return 0;
}

  

//
// EOF
//
