//  Library for handling HEP MC data
//  Copyright (C) 2023  Christian Holm Christensen
//
//  This program is free software: you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation, either version 3 of
//  the License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see
//  <https://www.gnu.org/licenses/>.
//                              
/**
   @file      pythia.cc
   @copyright (c) 2023 Christian Holm Christensen
   @brief     Run Pythia event generate and produce HepMC output
*/
#include <hepmc4/io_factory>
#include <hepmc4/pythia8>
#include <hepmc4/database>

namespace hepmc4
{
  namespace pythia8
  {
    template <typename T>
    struct base
    {
      /** Type of floating point values */
      using values_type=T;
      /** Builder type */
      using builder_type=hepmc4::builder<values_type>;
      /** Builder type */
      using converter_type=hepmc4::pythia8::converter<values_type>;
      /** Particle type */
      using particle_type=typename builder_type::particle_type;
      /** Particle type */
      using particle_ptr=typename builder_type::particle_ptr;
      /** program name */
      std::string _prog_name;
      /** configuration file to read */
      std::string _configFileName;
      /** Output file name */
      std::string _outputFileName;
      /** Number of events to generate */
      size_t _nev = 10;
      /** Random number seed */
      long _seed = -1;
      /** Verbosity */
      bool _verbose = false;
      /** Center of mass energy (per nucleon) */
      values_type _cm_energy = 14000;
      /** Max number of errors */
      size_t _max_errors = 10;
      /** Current number of errors */
      size_t _n_errors = 0;
      /** Projectile */
      int _projectile = 2212;
      /** Target */
      int _target = 2212;
      /** Prune events */
      bool _prune;
      /** Pythia object */
      Pythia8::Pythia _pythia{"",false};
      /** Converter object */
      converter_type _converter;

      /**
         Print usage

         @param o Output stream 
      */
      virtual void usage(std::ostream& o=std::cout)
      {
        o << "Usage: " << _prog_name << " [OPTIONS]\n\n"
          << "Options:\n"
          << " -h        This help\n"
          << " -v        Be verbose (" << std::boolalpha << _verbose << ")\n"
          << " -n NEV    Number of events (" << _nev << ")\n"
          << " -N MAX    Maximum number of failures (" << _max_errors << ")\n"
          << " -c CONFIG Configuration file name (" << _configFileName << ")\n"
          << " -o OUTPUT Output file name (" << _outputFileName << ")\n"
          << " -s SEED   Random number seed (" << _seed << ")\n"
          << " -e GEV    Center of mass energy (" << _cm_energy << ")"
          << " -P        Prune events (" << _prune << ")"
          << std::endl;
      }
      /**
         Return values when handling options
      */
      enum class option_status {
        /** Option was OK, does not eat argument */
        ok,
        /** Option was OK, and eat argument */
        ok_eat,
        /** Application should exit - e.g., on `-h` */
        exit,
        /** Unknown option */
        bad
      };
      /**
         Get PDG code from option.

         @param option
         @param which   Which type of particle
         @return pdg PDG code of particle
      */
      static int pdg_code(const std::string& option,
                          const std::string& which)
      {
        int pid = 0;
        try {
          pid = std::stoi(option);
        }
        catch (std::exception&) {
          pid = hepmc4::database::pdg(option);
        }
        if (pid == 0)
          throw std::invalid_argument(option+" for "+which+
                                      " not a valid PDG code or "
                                      "particle name");
        return pid;
      }
      /**
         Parse an option

         @param option The option
         @param arg    Possible argument
         @return one of option_status.  `ok` means that the option was
                 OK, `ok_eat` means the option was OK, but also that
                 we eate the following argument. `exit` means the
                 application sould end (e.g., after `-h`), and `bad`
                 means the option wasn't recoginsed.
      */
      virtual option_status parse_option(char option, char* arg)
      {
        switch (option) {
        case 'n': _nev           = std::stoi(arg);return option_status::ok_eat;
        case 'N': _max_errors    = std::stoi(arg);return option_status::ok_eat;
        case 'c': _configFileName= arg;           return option_status::ok_eat;
        case 'o': _outputFileName= arg;           return option_status::ok_eat;
        case 's': _seed          = std::stoi(arg);return option_status::ok_eat;
        case 'v': _verbose       = true;          return option_status::ok;
        case 'P': _prune         = true;          return option_status::ok;
        case 'h': usage();                        return option_status::exit;
        }
        return option_status::bad;
      }
      /**
         Make Pythia be quiet depending on the verbosity settings 
      */
      virtual void silence()
      {
        std::string q = _verbose ? "off" : "on";
        std::string v = _verbose ? "on"  : "off";
        _pythia.readString("Print:quiet                      = "+q);
        _pythia.readString("Init:showAllSettings             = "+v);
        _pythia.readString("Stat:showProcessLevel            = "+v);
        _pythia.readString("Init:showAllParticleData         = "+v);
        _pythia.readString("Init:showChangedParticleData     = "+v);
        _pythia.readString("Init:showChangedSettings         = "+v);
        _pythia.readString("Init:showMultipartonInteractions = "+v);
      }
      /**
         Set the seed to pythia to use.  If the set seed is 0 or
         positive, then set the set to that value.  Note, 0 means use
         default Pythia seed.  If the set set is negative, then do no
         set the seed and Pythia will use the current time.
      */
      virtual void seed()
      {
        _pythia.readString("Random:setSeed = "
                           +std::string(_seed >= 0 ? "yes" : "no"));
        _pythia.readString("Random:seed = "+std::to_string(_seed));
      }
      /**
         Configure Pythia.  Default configuration is to set the
         projectile and target PDG codes, as well as the collision
         energy in the centre of mass frame.  Also, Pythia is asked to
         decay particles with a mean lifetime (c*tau) less than 10mm.

         Specific classes should override this to do additional
         settings.
      */
      virtual void configure()
      {
        // Beam parameter settings. Values below agree with default ones.
        _pythia.readString("Beams:idA                    = "+
                           std::to_string(_projectile));
        _pythia.readString("Beams:idB                    = "+
                           std::to_string(_target));
        _pythia.readString("Beams:eA                     = "+
                           std::to_string(_cm_energy/2));
        _pythia.readString("Beams:eB                     = "+
                           std::to_string(_cm_energy/2));
        _pythia.readString("Beams:eCM                    = "+
                           std::to_string(_cm_energy));//As above
        _pythia.readString("ParticleDecays:limitTau0     = on");
        _pythia.readString("ParticleDecays:tau0Max       = 10");
      }
      /**
         Initialize Pythia
      */
      virtual void init()
      {
        if (_verbose) std::clog << "Initalising ." << std::flush;
        // Suppress output 
        silence();
        
        // Set the random number seed
        if (_verbose) std::clog << "." << std::flush;
        seed();
    
        // Configure the EG
        if (_verbose) std::clog << "." << std::flush;
        configure();
    
        // Read in commands from external file.
        if (_verbose) std::clog << "." << std::flush;
        if (not _configFileName.empty()) _pythia.readFile(_configFileName);

        // Initialization.
        if (_verbose) std::clog << "." << std::flush;
        _pythia.init();
        if (_verbose) std::clog << " done" << std::endl;
      }
      /**
         Make an event

         @return true on success 
      */
      virtual bool make_event()
      {
        if (not _pythia.next()) {
          // First few failures write off as "acceptable" errors, then quit.
          if (++_n_errors >= _max_errors) 
            throw std::runtime_error(" Event generation failed "+
                                     std::to_string(_n_errors)+" times");
          return false;
        }
        return true;
      }

      /**
         Loop and generate events

         @return 0 on success
      */
      virtual int loop()
      {
        // Create our wrapper around the output "file" */
        auto outputStream = hepmc4::io_factory::open_output(_outputFileName);
        if (_verbose)
          std::cerr << "Write output to "
                    << (_outputFileName.empty() ? "standard output" :
                        _outputFileName) << std::endl;

        // Select which particles to keep when pruning the events.
        // Here, we keep final-state, decayed, and beam particles
        // only.
        auto select = [](const particle_ptr& particle) -> bool {
          switch (particle->status) {
          case particle_type::final_state:
          case particle_type::decayed:
          case particle_type::beam:
            return true;
          }
          return false;
        };
        

        // Make an empty event, and add run_info to it
        auto event  = builder_type::event(); 
        event->run_info() = builder_type::run_info();

        // Create our writer.  Format is deduced from output name. 
        auto writer =
          hepmc4::io_factory::deduce_writer<values_type>(_outputFileName,
                                                         outputStream.stream);
        
        // Begin event loop.
        bool first = true;
        for (size_t iev = 0; iev < _nev; ++iev) {
          if (_verbose) 
            std::clog << "Event # " << std::setw(6) << iev << std::flush;
          
          // Generate event.     
          if (not make_event()) continue;

          size_t nOrig = _pythia.event.size();
          if (_verbose) std::clog << " " << std::setw(12) << nOrig;
          
          // Clear the event. 
          event->clear();

          // If this is the first event, fill in run_info 
          if (first) {
            _converter(event->run_info(),_pythia.info,_pythia.settings);
            first = false;
          }

          // Convert the Pythia event to the HepMC event 
          _converter(event,_pythia.event,&_pythia.info);

          event->number = iev;

          if (_prune)
            event->prune(select);
          // Write the event
          writer->write_event(event);

          if (_verbose) std::clog << " done" << std::endl;
        }
        // Release the write rresource 
        writer.reset();
        return 0;
      }
      /**
         Run the job.  Parses the command line

         @param argc  Number of command line arguments
         @param argv  Command line arguments
         @return 0 on success
      */
      int run(int argc, char** argv)
      {
        _prog_name = argv[0];
        for (int i = 1; i < argc; i++) {
          if (argv[i][0] == '-') {
            auto status = parse_option(argv[i][1], argv[i+1]);
            switch (status) {
            case option_status::ok_eat:  ++i; // Fall through
            case option_status::ok:      break;
            case option_status::exit:    return 0;
            case option_status::bad:
              throw std::runtime_error(_prog_name+": Unknown option "+
                                       argv[i]);
              break;
            }
          }
          else
            throw std::runtime_error(std::string("Non-option: ")+argv[i]);
        }

        // Initialize 
        init();
        if (_verbose) 
          std::cerr << "Initialisation done, starting loop" << std::endl;

        // Loop and return 
        return loop();
      }
    };
    //----------------------------------------------------------------
    /**
       Function to execute Pythia

       @param argc  Number of command line arguments
       @param argv  Command line arguments
       @return 0 on success 
    */
    template <typename T,
              template <typename> class PythiaType>
    int do_main(int argc, char** argv)
    {
      PythiaType<T> pythia;
      return pythia.run(argc,argv);
    }
    
    //----------------------------------------------------------------
    /**
       Create pp events at @f$\sqrt{s}=14\,\mathrm{TeV}@f$
    */
    template <typename T>
    struct pp : public base<T>
    {
      using base_type=base<T>;
      /** Return of option parsing */
      using option_status=typename base_type::option_status;
      /** Type of collisions */
      std::string _type = "INEL";
      /** Use base member */
      using base_type::_pythia;
      /** Use base member */
      using base_type::_cm_energy;
      /** Set of types */
      const std::set<std::string> types = {"INEL","NSD"};

      /**
         Expand help output
      */
      void usage(std::ostream& o=std::cout) override
      {
        base_type::usage();
        o << " -t TYPE   Type of collisions (" << _type
          << ", one of INEL or NSD)\n"
          << std::endl;
      }

      /**
         Expand option parsing
      */
      option_status parse_option(char option, char* arg) override
      {
        switch (option) {
        case 't': _type = arg; return option_status::ok_eat;
        }
        return base_type::parse_option(option, arg);
      }
      
      /**
         Configure for pp min-bias, Monash tune
      */
      void configure() override
      {
        size_t typ_no  = std::distance(types.begin(),  types.find(_type));
        if (typ_no >= types.size())
          throw std::runtime_error(std::string("Unknown collision type ")
                                   +_type);
        bool nsd = typ_no == 1;
        
        if (not nsd) 
          _pythia.readString("SoftQCD:inelastic           = on");
        else {
          // Note, setting SoftQCD:inelastic will override other settings!
          _pythia.readString("SoftQCD:all                 = off");
          _pythia.readString("SoftQCD:nonDifferactive     = on");
          _pythia.readString("SoftQCD:doubleDifferactive  = on");
          _pythia.readString("SoftQCD:centralDifferactive = on");
        }
        _pythia.readString("Tune:ee                       = 7");
        _pythia.readString("Tune:pp                       = 14");

        base_type::configure();
      }
    };

    
    //----------------------------------------------------------------
    /**
       Create Pb-Pb events at
       @f$\sqrt{s_{\mathrm{NN}}}=5.02\,\mathrm{TeV}@f$
    */
    template <typename T>
    struct pbpb : public base<T>
    {
      /** Base type */
      using base_type=base<T>;
      /** Option status */
      using option_status=typename base_type::option_status;
      /** Values type */
      using values_type=typename base_type::values_type;
      /** Use base member */
      using base_type::_pythia;
      /** Use base member */
      using base_type::_cm_energy;
      /** Use base member */
      using base_type::_projectile;
      /** Use base member */
      using base_type::_target;
      /** Use base member */
      using base_type::_verbose;
      /** Use base member */      
      using base_type::pdg_code;
      /** Fit cross-section */      
      bool         _fit_sigma = false;
      /** Angantyr model */
      std::string  _model = "random";
      /** maximum impact parameter */      
      values_type _max_b = 14.5;
      /** Set of models */
      const std::set<std::string> models = {"fixed","random","opacity"};
      /** Lead PDG code */
      constexpr static const int pb_pdg = 1000822080;
      
      /**
         Override constructor so that we can change the default
         collision energy
      */
      pbpb() {
        _cm_energy  = 5023;
        _projectile = pb_pdg;
        _target     = pb_pdg;
      }
      
      /**
         Expand help output
      */
      void usage(std::ostream& o=std::cout) override
      {
        base_type::usage();
        o << " -f        Fit cross-section ("
          << std::boolalpha << _fit_sigma << ")\n"
          << " -b FERMI  Maximum impact parameter " << _max_b << "\n"          
          << " -m MODEL  Angantyr model (" << _model
          << " - one of fixed,random,opacity)" << "\n"
          << " -p PDG    Projectile (" << _projectile << ")\n"
          << " -t PDG    Target (" << _target << ")\n"
          << std::endl;
      }

      /**
         Expand option parsing
      */
      option_status parse_option(char option, char* arg) override
      {
        switch (option) {
        case 'f': _fit_sigma  = !_fit_sigma;   return option_status::ok;
        case 'm': _model      = arg;           return option_status::ok_eat;
        case 'b': _max_b      = std::stod(arg);return option_status::ok_eat;
        case 'p':
          _projectile = pdg_code(arg,"projectile");
          return option_status::ok_eat;
        case 't':
          _target     = pdg_code(arg,"target");
          return option_status::ok_eat;
        }
        return base_type::parse_option(option, arg);
      }

      /**
         Configure Pythia
      */
      void configure() override
      {
        size_t mod_no  = std::distance(models.begin(),  models.find(_model));
        if (mod_no > models.size())
          throw std::runtime_error(std::string("Unknown Angantyr model ")
                                   +_model);
    
        const std::vector<double> fit = {13.91,1.78,0.22,0.0,0.0,0.0,0.0,0.0};
        std::stringstream s;
        std::copy(fit.begin(), fit.end(),
                  std::ostream_iterator<double>(s, ","));
        std::string par = s.str();
        par             = par.substr(0,par.size()-1);//Swallow last ','

        // Force fit if not Pb-Pb at 5.023 TeV
        if (_projectile != pb_pdg or
            _target     != pb_pdg or
            _cm_energy  != 5023) _fit_sigma = true;
    
        // Angantyr model parameters 
        _pythia.readString("Angantyr::CollisionModel     = "+
                           std::to_string(mod_no));
        _pythia.readString("HeavyIon:showInit            = "+
                           std::string(_verbose ? "on" : "off"));
        _pythia.readString("HeavyIon:SigFitPrint         = off");
        _pythia.readString("HeavyIon:SigFitDefPar        = "+par);
        _pythia.readString("HeavyIon:SigFitNGen          = "+
                           std::string(_fit_sigma?"20":"0"));
        _pythia.readString("HeavyIon:bWidth              = "+
                           std::to_string(_max_b));

        base_type::configure();
      }
    };
  }
}

int main(int argc, char** argv)
{
  std::filesystem::path me(argv[0]);
  if (me.stem() == "pythia")  
    return hepmc4::pythia8::do_main<float,hepmc4::pythia8::pp>(argc,argv);
  return hepmc4::pythia8::do_main<float,hepmc4::pythia8::pbpb>(argc,argv);
}
//
// EOF
//


      
        
        
          
      

