//  Library for handling HEP MC data
//  Copyright (C) 2023  Christian Holm Christensen
//
//  This program is free software: you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation, either version 3 of
//  the License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see
//  <https://www.gnu.org/licenses/>.
//                              
/**
   @file      w.cc
   @copyright (c) 2023 Christian Holm Christensen
   @brief     Create W event
*/
#include <hepmc4/io_factory>
#include "generator"

//====================================================================
void
usage(const std::string& progname, std::ostream& out=std::cout)
{
  out << "Usage: " << progname << " [-l] [-n NEV] [OUTPUT]" << std::endl;
}

//====================================================================
int
main(int argc, char** argv)
{
  std::string outputFileName("");
  int         maxEvents = 1;
  bool        loop = false;
  
  for (int i = 1; i < argc; ++i) {
    if (argv[i][0] == '-') {
      switch (argv[i][1]) {
      case 'o':   outputFileName = argv[++i];            break;
      case 'n':   maxEvents      = std::stoi(argv[++i]); break;
      case 'h':   usage(argv[0]);                        return 0;
      case 'l':   loop           = not loop;             break;
      default:
        std::cerr << argv[0] << ": Unknown option: " << std::quoted(argv[i])
                  << std::endl;
        return 1;
      }
      continue;
    }
    outputFileName = argv[i];
  }

  auto outputStream = hepmc4::io_factory::open_output(outputFileName);
  auto writer = hepmc4::io_factory::deduce_writer<float>(outputFileName,
                                                         outputStream.stream);
  outputStream.stream << std::setprecision(12) << std::scientific;
  
  examples::w_generator<float> gen;
  gen.loop = loop;
  for (int iev = 0; iev < maxEvents; iev++) {
    auto event = gen.generate();
    writer->write_event(event);
  }

  writer.reset();

  return 0;
}

//
// EOF
//
