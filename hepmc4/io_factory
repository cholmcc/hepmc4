//  Library for handling HEP MC data
//  Copyright (C) 2023  Christian Holm Christensen
//
//  This program is free software: you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation, either version 3 of
//  the License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see
//  <https://www.gnu.org/licenses/>.
//
/**
   @file      io_factory
   @copyright (c) 2023 Christian Holm Christensen
   @brief     I/O facilities
   @ingroup   hepmc_io
*/
#ifndef HEPMC4_IO_FACTORY
#define HEPMC4_IO_FACTORY
#include <iostream>
#include <fstream>
#include <hepmc4/io>
#include <hepmc4/details>
#include <hepmc4/ascii2>
#include <hepmc4/ascii3>
#include <hepmc4/ascii4>
#include <hepmc4/hepevt>
#include <hepmc4/root>
#include <hepmc4/protobuf>
#include <hepmc4/dot>
#ifdef HEPMC4_HAS_BOOST_IOSTREAMS
# include <boost/iostreams/filtering_stream.hpp>
# include <boost/iostreams/filter/bzip2.hpp>
# include <boost/iostreams/filter/gzip.hpp>
# include <boost/iostreams/filter/zlib.hpp>
#endif

namespace hepmc4
{
  //====================================================================
  /**
     Wrapper around an input or output stream that may correspond to a
     file or to a some other stream.

     The wrapper is parameterised on

     - Type of stream to read from or write to
     - Type of file stream to possibly use
     - Number of bytes to use for buffering (defaults to 256kb)
     
     @headerfile io_factory hepmc4/io_factory
     @ingroup hepmc_io
     @tparam Stream  Stream type
     @tparam FStream File stream type
  */
  template<typename Stream, typename FStream> 
  struct basic_stream
  {
    /** Type of this */
    using this_type=basic_stream<Stream,FStream>;
    /** Type of stream, typically std::istream or std::ostream */
    using stream_type=Stream;
    /** Type of pointer to stream */
    using stream_ptr=Stream*;
    /** Type of file stream, typically std::ifstream or std::ofstream */
    using fstream_type=FStream;
    /** Type of underlying buffer type of file streams */
    using char_type=typename fstream_type::char_type;
    /**
       Construct a stream from a file name or default stream.

       If @a filename is empty or equal to `"-"`, then use the default
       stream given by @a def. Other wise, open the file and set the
       stream to be that file.  If a file is opened, then it is
       automatically closed whehn the stream object goes out of scope.

       This allows us to have a container of an input or output stream
       that can correspond to a file or some other stream (say
       standard input or output).

       Alternatively, this could be implemented using `std::variant`. 

       @param filename File name stream.  If empty or `-`, then do not
                       try to open a file but use the stream @a def
       @param def Default stream, in case @a filename is empty or `-`.
       @param nbuf Size of read buffer, if larger than 0.  Only
                   applies if @a filename is not empty or `-`
    */
    basic_stream(const std::string& filename,
                 stream_type& def,
                 size_t nbuf=0) // 2<<17
      : _fstream(filename.empty() or filename == "-" ?
                 nullptr : new fstream_type()),
        _pstream(nullptr),
        _buffer(nullptr),
        stream(_fstream ? *_fstream : def)
    {
      if (not _fstream) return;
      if (nbuf > 0) {
        _buffer = new char_type[nbuf];
        _fstream->rdbuf()->pubsetbuf(_buffer, nbuf);
      }
      _fstream->open(filename.c_str());
      
    }
    /**
       Construct a stream from other stream and pointer to decoder
       stream.  This will invalidate the other stream object and move
       the resources there.  This is used in case of comparessed
       files.

       @param other  Other stream to initialize from.
       @param str    Pointer to real stream 
    */
    basic_stream(this_type& other,stream_ptr str)
      : _fstream(other._fstream),
        _pstream(str),
        _buffer(other._buffer),
        stream(*str)
    {
      other._fstream = nullptr;
      other._pstream = nullptr;
      other._buffer  = nullptr;
    }
    /**
       Move constructor

       Moves the resources to constructed object

       @param other Other stream to initialize from.  This will be
                    invalid afterwards.
    */
    basic_stream(basic_stream&& other)
      : _fstream(std::move(other._fstream)),
        _pstream(std::move(other._pstream)),
        _buffer(std::move(other._buffer)),
        stream(other.stream)
    {
      other._fstream = nullptr;
      other._pstream = nullptr;
      other._buffer  = nullptr;
    }
    basic_stream(const basic_stream&) = delete;
    /**
       Destructor
    */
    ~basic_stream()
    {
      if (_pstream) {
        delete _pstream;
        _pstream = nullptr;
      }
      if (_fstream) {
        _fstream->close();
        delete _fstream;
        _fstream = nullptr;
      }
      
    }

    operator stream_type&() { return stream; }
  protected:
    /** Pointer to file stream */
    FStream* _fstream;
    /** Pointer to stream */
    Stream* _pstream;
    /** Buffer */
    char_type* _buffer;
  public:
    /** Stream to use */
    Stream& stream;
  };
  // =================================================================
  /**
     @{
     @name Common aliases

     @ingroup hepmc_io
     @headerfile io_factory hepmc4/io_factory
  */
  /** Input stream */     
  using input_stream  = basic_stream<std::istream,std::ifstream>;
  /** Output stream */     
  using output_stream = basic_stream<std::ostream,std::ofstream>;
  /** @} */
  
  //====================================================================
  /**
     Enumeration of I/O formats supported

     @ingroup hepmc_io
     @headerfile io_factory hepmc4/io_factory
  */
  enum class formats {
    ascii2,
    ascii3,
    ascii4,
    hepevt,
    root,
    root_list,
    protobuf,
    dot,
    unknown
  };
  
  //--------------------------------------------------------------------
  /**
     Output format name

     @ingroup hepmc_io
     @headerfile io_factory hepmc4/io_factory
     @param o Output stream
     @param f Format
     @return @a o
  */
  std::ostream& operator<<(std::ostream& o, const formats& f)
  {
    switch (f) {
    case formats::ascii2:    return o << "Ascii2";
    case formats::ascii3:    return o << "Ascii3";
    case formats::ascii4:    return o << "Ascii4";
    case formats::hepevt:    return o << "HEPEVT";
    case formats::root:      return o << "ROOT";
    case formats::root_list: return o << "ROOT-list";
    case formats::protobuf:  return o << "ProtoBuf";
    case formats::dot:       return o << "GraphViz";
    case formats::unknown:   return o << "unknown";
    }
    return o << "unknown";
  }

  //====================================================================
  /**
     Factory of readers and writers as well as streams
     
     @ingroup hepmc_io
     @headerfile io_factory hepmc4/io_factory
  */
  struct io_factory
  {
    /**
       Bytes identifying a compressed input
    */
    enum {
      gzip_b1 = 0x1f,
      gzip_b2 = 0x8b,
      bzip2_b1 = 'B',
      bzip2_b2 = 'Z',
      zip_b1   = 0x78,
      zip_b2_1 = 0x01,
      zip_b2_2 = 0x9c,
      zip_b2_3 = 0xda
    };
      
    //--------------------------------------------------------------------
    /**
       Open an input file.

       If [Boost `iostreams`](https://boost.org) library is used
       (macro `HEPMC4_HAS_BOOST_ISTREAMS is defined), then this can
       open compressed output file.  Which type of compression is used
       depends on the ending of `filename`.

       | Ending   | Algoritm |
       |----------|----------|
       | `.gz`    | GZip     |
       | `.gzip`  | GZip     |
       | `.bz2`   | BZip2    |
       | `.bzip2` | BZip2    |
       | `.z`     | ZLib     |
       | `.zip`   | ZLib     |

       The return stream object is transparent with respect to the
       compression.  That is, regular C++ I/O stream operations can be
       used on the stream.
       
       @param filename File name.  If empty or `-` use the stream
                       specified in @a in
       @param in Input stream if file name is empty or `-`
       @param nbuf Size of input buffer.  If 0, use default for the
                   C++ implementation.
       
       @return Input stream
       @throws std::runtime_error on error 
    */
    static input_stream open_input(const std::string& filename,
                                   std::istream& in=std::cin,
                                   size_t nbuf=0)
    {
      input_stream tmp(filename,in, nbuf);
#ifdef HEPMC4_HAS_BOOST_IOSTREAMS
      // Read two first bytes 
      unsigned char b1 = tmp.stream.get();
      if (b1 != gzip_b1 and 
          b1 != bzip2_b1  and
          b1 != zip_b1) {
        tmp.stream.putback(b1);
        return tmp;
      }
      unsigned char b2 = tmp.stream.get();
      tmp.stream.putback(b2);
      tmp.stream.putback(b1);
      if (tmp.stream.fail())
        throw std::runtime_error("Failed to unget");

      // This doesn't work with C++11 (parameter type `auto`) and
      // there seems no way to fake it
      // 
      // auto mk_filterd = [&tmp](auto cmp) {
      //   // Allocate on heap to ensure life-time 
      //   auto fin = new boost::iostreams::filtering_istream;
      //   fin->push(cmp);
      //   fin->push(tmp.stream);
      //   return input_stream(tmp, fin);
      // };

      if (b1 == gzip_b1 and b2 == gzip_b2) { // GZIP
        auto fin = new boost::iostreams::filtering_istream;
        fin->push(boost::iostreams::gzip_decompressor());
        fin->push(tmp.stream);
        return input_stream(tmp, fin);
        // return mk_filterd(boost::iostreams::gzip_decompressor());  C++14
      }
      if (b1 == bzip2_b1 and b2 == bzip2_b2)  { // BZIP2
        auto fin = new boost::iostreams::filtering_istream;
        fin->push(boost::iostreams::bzip2_decompressor());
        fin->push(tmp.stream);
        return input_stream(tmp, fin);
        // return mk_filterd(boost::iostreams::bzip2_decompressor());  C++14
      }
      if (b1 == zip_b1 and
          (b2 == zip_b2_1 or b2 == zip_b2_2 or b2 == zip_b2_3)) { // zlib
        auto fin = new boost::iostreams::filtering_istream;
        fin->push(boost::iostreams::zlib_decompressor());
        fin->push(tmp.stream);
        return input_stream(tmp, fin);
        // return mk_filterd(boost::iostreams::zlib_decompressor()); C++14
      }
             
#endif
      // ASCII
      return tmp;
    }
    //--------------------------------------------------------------------
    /**
       Open an output file.

       If [Boost `iostreams`](https://boost.org) library is used
       (macro `HEPMC4_HAS_BOOST_ISTREAMS is defined), then this can
       open compressed output file.  Which type of compression is used
       depends on the ending of `filename`.

       | Ending   | Algoritm |
       |----------|----------|
       | `.gz`    | GZip     |
       | `.gzip`  | GZip     |
       | `.bz2`   | BZip2    |
       | `.bzip2` | BZip2    |
       | `.z`     | ZLib     |
       | `.zip`   | ZLib     |

       The return stream object is transparent with respect to the
       compression.  That is, regular C++ I/O stream operations can be
       used on the stream.
       
       @param filename File name.  If empty or `-` use the stream
                       specified in @a in
       @param out Output stream if file name is empty or `-`
       @return Output stream
       @throws std::runtime_error on error 
    */
    static output_stream open_output(const std::string& filename,
                                    std::ostream& out=std::cout)
    {
      output_stream tmp(filename, out);
#ifdef HEPMC4_HAS_BOOST_IOSTREAMS
      auto test = [](const std::filesystem::path& out,
                     const std::string& what) -> bool {
        return  (out.extension() == what);
      };

      enum {
        gzip,
        bzip2,
        zlib,
        plain
      };
      auto match = [test](const std::filesystem::path& out) {
        if (test(out,".gz"))     return gzip;
        if (test(out,".gzip"))   return gzip;
        if (test(out,".bz2"))    return bzip2;
        if (test(out,".bzip2"))  return bzip2;
        if (test(out,".z"))      return zlib;
        if (test(out,".zip"))    return zlib;
        return plain;
      };

      // This doesn't work with C++11 and there seems no way to fake
      // it
      // 
      // auto mk_filterd = [&tmp](auto cmp) {
      //   // Allocate on heap to ensure life-time 
      //   auto fout = new boost::iostreams::filtering_ostream;
      //   fout->push(cmp);
      //   fout->push(tmp.stream);
      //   return output_stream(tmp, fout);
      // };

      auto sel = match(filename);
      if (sel == gzip) {
        auto fout = new boost::iostreams::filtering_ostream;
        fout->push(boost::iostreams::gzip_compressor());
        fout->push(tmp.stream);
        // return mk_filterd(boost::iostreams::gzip_compressor()); C++14
        return output_stream(tmp,fout);
      }
      if (sel == bzip2) {
        auto fout = new boost::iostreams::filtering_ostream;
        fout->push(boost::iostreams::bzip2_compressor());
        fout->push(tmp.stream);
        // return mk_filterd(boost::iostreams::bzip2_compressor()); C++14
        return output_stream(tmp,fout);
      }
      if (sel == zlib) {
        auto fout = new boost::iostreams::filtering_ostream;
        fout->push(boost::iostreams::zlib_compressor());
        fout->push(tmp.stream);
        // return mk_filterd(boost::iostreams::zlib_compressor()); C++14
        return output_stream(tmp,fout);
      }
#endif
      return tmp;
    }
    //--------------------------------------------------------------------
    /**
       Inspect the file content and try to deduce the file format.
       This will try to read the first few bytes of the input to
       figure out the format.

       | Number of bytes | Value        | Format                   |
       |-----------------|--------------|--------------------------|
       | 4               | `root`       | ROOT                     |
       | 4               | `hmpb`       | Protobuf                 |
       | 1               | `E`          | HEPEVT                   |
       | 7+              | `HepMC::`    | One of the ASCII formats |
       | 10              | `TFile-list` | List of ROOT files       |
       
       This is agnostic as to wether the stream read from is
       compressed or not.

       @param in Input stream to investigate
       @return Format identifier 
    */
    static formats deduce_input_format(std::istream& in)
    {
      std::string line;
      size_t      lineno = 0;

      if (in.fail()) 
        throw std::runtime_error("Input already in error");
      
      // Read first byte (character)
      std::string bytes;
      bytes.push_back(char(in.get()));
      // std::clog << "First byte " << std::quoted(bytes) << std::endl;
      if (bytes[0] == 'E') {
        return formats::hepevt;
      }

      // Read up to four bytes of data 
      for (size_t i = 0; i < 3; i++) bytes.push_back(char(in.get()));
      // std::clog << "First 4 bytes " << std::quoted(bytes) << std::endl;
      if (in.fail()) 
        throw std::runtime_error("Input in error");
      
      if (bytes == root::constants::file_magic)
        return formats::root;
      if (bytes == protobuf::constants::file_magic)
        return formats::protobuf;
      unsigned char b1 = bytes[0];
      unsigned char b2 = bytes[1];
      if ((b1 == 0x1f and b2 == 0x8b) or 
          (b1 == 'B'  and b2 == 'Z') or
          (b1 == 0x78 and (b2 == 0x1 or
                           b2 == 0x9c or
                           b2 == 0xda)))
        throw not_supported("Input seems to be a compressed file, "
                            "but Boost IOStreams library is not used");
        
        
      
      std::getline(in, line);
      line = bytes + line;
      trim(line);
      while ((line.empty() or line[0] == '#') and not in.eof()) {
        std::getline(in, line);
        trim(line);
        lineno++;
        //std::clog << std::setw(6) << lineno << " " << line << std::endl;
      }
      if (in.eof()) return formats::unknown;

      if (line.find(root::constants::file_header) == 0)
        return formats::root_list;
      
      auto test =
        [](const std::string& line, const std::string& start) -> bool {
          return line.find(std::string("HepMC::")+start) != std::string::npos;
        };
      
      if (not test(line, "Version")) {
        std::cerr << "Not HepMC ASCII input" << std::endl;
        return formats::unknown;
      }
      
      std::getline(in,line);
      lineno++;
    
      if (test(line,ascii2::constants::start)) return formats::ascii2;
      if (test(line,ascii3::constants::start)) return formats::ascii3;
      if (test(line,ascii4::constants::start)) return formats::ascii4;
      
      return formats::unknown;
    }

    //====================================================================
    /**
       Create a reader object and return pointer to it

       @tparam T Floating point value type
       @tparam R Reader template class
       @tparam A Stream type
       @tparam Args Constructor argument types
       @param arg0 Stream to read from
       @param args Additional arguments to reader constructor
       @return Pointer to reader
    */
    template <typename T,
              template <typename> class R, typename A,
              typename ...Args>
    static reader_ptr<T> mk_reader(A& arg0, Args ...args)
    {
      auto ptr = std::make_shared<R<T>>(arg0,args...);
      return std::dynamic_pointer_cast<reader<T>>(ptr);
    }
                                                      
    //--------------------------------------------------------------------
    template <typename T>
    static reader_ptr<T>  deduce_reader(const std::string& inputFileName,
                                        std::istream& in)
    {
      formats fmt = deduce_input_format(in);

      // std::clog << "Input format is " << fmt << std::endl;
      
      if (fmt == formats::root)
        return mk_reader<T,root::reader>(inputFileName);

      switch (fmt) {
      case formats::hepevt:    return mk_reader<T,hepevt  ::reader>(in,true);
      case formats::ascii2:    return mk_reader<T,ascii2  ::reader>(in,true);
      case formats::ascii3:    return mk_reader<T,ascii3  ::reader>(in,true);
      case formats::ascii4:    return mk_reader<T,ascii4  ::reader>(in,true);
      case formats::root_list: return mk_reader<T,root    ::reader>(in,true);
      case formats::protobuf:  return mk_reader<T,protobuf::reader>(in,true);
      default: break;
      }
      throw std::runtime_error("Unknown format of input stream");
    }
  
    //====================================================================
    /**
       Deduce the output format.  The format is recognised by the
       ending of @a outputFileName

       | Ending    | Format                        |
       |-----------|-------------------------------|
       | `.hepevt` | HEPEVT                        |
       | `.hepmc2` | Asciiv2 (aka `IO_GenEvent`)   |
       | `.hepmc3` | Asciiv3                       |
       | `.hepmc4` | Asciiv4                       |
       | `.root`   | ROOT(*)                       |
       | `.list`   | ROOT with index ASCII file(*) |       
       | `.proto`  | ProtoBuf(**)                  |
       | `.dot`    | GraphViz dot format           |
       | other     | Asciiv4                       |

       (*) Requires `HEPMC4_HAS_ROOT` 
       (**) Requires `HEPMC4_HAS_PROTOBUF`

       Note, compressed file ending (see open_output) are ignored, and
       the next ending is queried.  That is, a file name like

           output.hepevt.gz

       will be deduced to be HEPEVT

       @param outputFileName Output file name 
       @return Format identifier 
    */
    static formats deduce_output_format(const std::string& outputFileName)
    {
      std::filesystem::path out(outputFileName);

      auto test = [](const std::filesystem::path& out,
                     const std::string& what) -> bool {
        return  (out.extension() == what);
      };

      auto match = [test](const std::filesystem::path& out) {
        if (test(out,".hepevt")) return formats::hepevt;
        if (test(out,".hepmc2")) return formats::ascii2;
        if (test(out,".hepmc3")) return formats::ascii3;
        if (test(out,".hepmc4")) return formats::ascii4;
        if (test(out,".root"))   return formats::root;
        if (test(out,".list"))   return formats::root_list;
        if (test(out,".proto"))  return formats::protobuf;
        if (test(out,".dot"))    return formats::dot;
        return formats::unknown;
      };

      formats ret = formats::unknown;
      
      if ((ret = match(out)) != formats::unknown) return ret;
        
      auto sub = out.stem();
      if ((ret = match(sub)) != formats::unknown) return ret;
      
      return formats::ascii4;
    }

    //====================================================================
    /**
       Create a writer object and return pointer to it

       @tparam T Floating point value type
       @tparam W Writer template class
       @tparam A Stream type
       @tparam Args Constructor argument types
       @param args Arguments to writer constructor
       @return Pointer to writer
    */
    template <typename T, template <typename> class W,typename...Args>
    static writer_ptr<T> mk_writer(Args& ...args)
    {
      auto ptr = std::make_shared<W<T>>(args...);
      return std::dynamic_pointer_cast<writer<T>>(ptr);
    }
                                                      
    //--------------------------------------------------------------------
    /**
       Create a writer for the "file" (stream, really) given by @a
       filename and stream @a out.  See deduce_output_format for how
       the format of the output is deduced.

       @param filename File name
       @param out Stream to write to
       @return Writer
    */
    template<typename T>
    static writer_ptr<T> deduce_writer(const std::string& filename,
                                       std::ostream& out)
    {
      formats fmt = deduce_output_format(filename);
      
      // std::clog << "Output format is " << fmt << std::endl;
      
      switch (fmt) {
      case formats::hepevt:    return mk_writer<T,hepevt  ::writer>(out);
      case formats::ascii2:    return mk_writer<T,ascii2  ::writer>(out);
      case formats::ascii3:    return mk_writer<T,ascii3  ::writer>(out);
      case formats::ascii4:    return mk_writer<T,ascii4  ::writer>(out);
      case formats::root:      return mk_writer<T,root    ::writer>(filename);
      case formats::root_list: return mk_writer<T,root    ::writer>(out);
      case formats::protobuf:  return mk_writer<T,protobuf::writer>(out);
      case formats::dot:       return mk_writer<T,dot     ::writer>(out);
      case formats::unknown: break;
      }
      throw std::runtime_error("Unknown format of input stream");
    }
  };

}

#endif
// Local Variables:
//  mode: C++
// End:
