//  Library for handling HEP MC data
//  Copyright (C) 2023  Christian Holm Christensen
//
//  This program is free software: you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation, either version 3 of
//  the License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see
//  <https://www.gnu.org/licenses/>.
//                              
/**
   @file      root
   @copyright (c) 2023 Christian Holm Christensen
   @brief     ROOT Input/output format interface
   @ingroup   hepmc_root
*/
#ifndef HEPMC4_ROOT
#define HEPMC4_ROOT
#include <hepmc4/hepevt>
#if (defined(HEPMC4_HAVE_ROOT) || defined(HAVE_ROOT)) && \
  !defined(HEPMC4_HAS_ROOT)
# define HEPMC4_HAS_ROOT 1
#endif
#ifdef HEPMC4_HAS_ROOT
# include <TFile.h>
# include <TTree.h>
# include <TClonesArray.h>
# include <TBranch.h>
# include <TParticle.h>
# include <TChain.h>
#endif

namespace hepmc4
{
  /**
     @defgroup hepmc_root Input from/output to ROOT data structures.
     
     The code in this group will ouput event<T> objects to a
     [ROOT](https://root.cern.ch) data file, and read that back into
     event<T> objects.
     
     The events are stored in a
     [`TTree`](https://root.cern/doc/master/classTTree.html), by
     default called `T`.
     
     Particles of an event are stored as
     [`TParticle`](https://root.cern/doc/master/classTParticle.html)
     objects in a
     [`TClonesArray`](https://root.cern/doc/master/classTClonesArray.html)
     in the branch `particles`.
     
     Other objects, such as `pdf_info`, `cross_section`, and
     `heavy_ion` are stored in separate branches of the `TTree`.
     These are stored as `struct` branches.
     
     Altoghether, this means that processing the ROOT `TTree` can be
     done without custom code.

     Note that the I/O classes works in two different "modes".

     - File list driven
     
       - If the hepmc4::root::reader object is constructed from a
         std::istream object, then that stream is assumed to contain a
         list of ROOT files to read from.  The reader then constructs
         a [`TChain`](https://root.cern/doc/master/classTChain.html)
         of these files and reads the event data from there.

       - If the hepmc4::root::writer is constructed from a
         std::ostream object, then it will write the file name of the
         generated ROOT file (default to `hepmc.root`) to that stream
         and open the output file on its own.

     - Filename driven 
       
       - If the hepmc4::root::reader object is constructed from a
         std::string object, then that string is assumed to hold the
         name of a single ROOT file to read events from.  The reader
         then opens that ROOT file and reads the events from there.

       - If the hepmc4::root::writer is constructed from a std::string
         object, then that string is assumed to hold the name of the
         ROOT file to write.  The writer then opens the file and
         writes events to that file.
       
     @ingroup hepmc_io
  */
  namespace root
  {
    /**
       Constants used

       @ingroup hepmc_root
       @headerfile root hepmc4/root
    */
    struct constants {
      constexpr static const char* tree_name = "T";
      constexpr static const char* particles_name = "particles";
      constexpr static const char* header_name = "header";
      constexpr static const char* pdf_info_name = "pdf_info";
      constexpr static const char* cross_section_name = "cross_section";
      constexpr static const char* heavy_ion_name = "heavy_ion";
      constexpr static const char* default_file_name = "hepmc.root";
      constexpr static const char* file_header = "TFile-list";
      constexpr static const char* file_magic = "root";
      /** Special attributes */
      constexpr static const char* mpi = "mpi";
      constexpr static const char* event_scale = "event_scale";
      constexpr static const char* alpha_qed = "alphaQED";
      constexpr static const char* alpha_qcd = "alphaQCD";
      constexpr static const char* signal_process_id = "signal_process_id";
      constexpr static const char* signal_process_vertex = "signal_process_vertex";
    };

    //================================================================
    /**
       Floating point specification - not used

       @ingroup hepmc_root
       @headerfile root hepmc4/root
       @tparam T Floating point value type
    */
    template <typename T> struct float_spec; 
    /**
       Floating point specification - not used

       @ingroup hepmc_root
       @headerfile root hepmc4/root
    */
    template<> struct float_spec<float> {
      constexpr static const char* value="F";
    };
    /**
       Floating point specification - not used

       @ingroup hepmc_root
       @headerfile root hepmc4/root
    */
    template<> struct float_spec<double> {
      constexpr static const char* value="D";
    };
    
    //================================================================
    /**
       Header

       @ingroup hepmc_root
       @headerfile root hepmc4/root
       @tparam T Floating point value type
    */
    template <typename T>
    struct hd_data {
      /** Value type */
      using values_type=T;
      /** Event pointer */
      using event_ptr=hepmc4::event_ptr<values_type>;
      /** Value type */
      using event_type=hepmc4::event<values_type>;
      /** Leaves */
      constexpr static const char* leaves =
        "number/I:mpi:signal_process:signal_vertex:"
        "event_scale/F:alphaQED:alphaQCD";

      /** Event number */
      int number;
      /** Number of multi-parton-interactions */
      int mpi;
      /** Signal process ID */
      int signal_process;
      /** Signal vertex */
      int signal_vertex;
      /** event scale */
      values_type event_scale;
      /** QED coupling constant */
      values_type alpha_qed;
      /** QCD coupling constant */
      values_type alpha_qcd;

      /**
         Assign from an event

         @param event The event to assign from
         @return Refernece to self
      */
      hd_data& operator=(const event_ptr& event)
      {
        number         = 0;
        mpi            = 0;
        signal_process = 0;
        signal_vertex  = 0;
        event_scale    = 0;
        alpha_qed      = 0;
        alpha_qcd      = 0;

        auto ce = std::const_pointer_cast<const event_type>(event);

        number         = event->number;
        mpi            = ce->attribute_value(constants::mpi, mpi);
        signal_process = ce->attribute_value(constants::signal_process_id,
                                                 signal_process);
        signal_vertex  = ce->attribute_value(constants::signal_process_vertex,
                                                 signal_vertex);
        event_scale    = ce->attribute_value(constants::event_scale,
                                                 event_scale);
        alpha_qed      = ce->attribute_value(constants::alpha_qed,alpha_qed);
        alpha_qcd      = ce->attribute_value(constants::alpha_qcd,alpha_qcd);

        return *this;
      }
      /**
         Store values in event

         @param event Event to store in
      */
      void operator()(event_ptr& event)
      {
        event->number = number;
        if (mpi > 0)
          event->add_attribute(constants::mpi,                  mpi);
        if (signal_process != 0)
          event->add_attribute(constants::signal_process_id,  signal_process);
        if (signal_vertex != 0)
          event->add_attribute(constants::signal_process_vertex,signal_vertex);
        if (event_scale >= 0)
          event->add_attribute(constants::event_scale,          event_scale);
        if (alpha_qed >= 0)
          event->add_attribute(constants::alpha_qed,            alpha_qed);
        if (alpha_qcd >= 0)
          event->add_attribute(constants::alpha_qcd,            alpha_qcd);
      }
    };
      
      
    //----------------------------------------------------------------
    /**
       Structure of data to store in ROOT tree for cross-sections

       @ingroup hepmc_root
       @headerfile root hepmc4/root
       @tparam T Floating point value type
    */
    template <typename T>
    struct xs_data {
      /** Value type */
      using values_type=T;
      /** Value type */
      using event_ptr=hepmc4::event_ptr<values_type>;
      /** Builder type */
      using builder_type=hepmc4::builder<values_type>;
      /** Float specifier */
      constexpr static const char* float_letter=float_spec<T>::value;
      /** Leaf list */
      constexpr static const char* leaves =
        "attempted/G:accepted:value/F:uncertainty";
      
      /** Attempted number of events */
      long attempted = 0;
      /** Accepted number of events */
      long accepted = 0;
      /** Cross-section value */
      values_type value = 0;
      /** Cross-section uncertainty */
      values_type uncertainty = 0;

      /**
         Assign from an event

         @param event The event to assign from
         @return Refernece to self
      */
      xs_data& operator=(const event_ptr& event)
      {
        attempted   = 0;
        accepted    = 0;
        value       = 0;
        uncertainty = 0;
        
        auto xsec = event->cross_section();
        if (!xsec) return *this;

        attempted      = xsec->n_attempted;
        accepted       = xsec->n_accepted;
        value          = xsec->values[0];
        uncertainty    = xsec->uncertainties[0];

        return *this;
      }
      /**
         Store values in event

         @param event Event to store in
      */
      void operator()(event_ptr& event)
      {
        if (not event->cross_section())
          event->cross_section() = builder_type::cross_section();
        
        event->cross_section()->n_attempted      = attempted;
        event->cross_section()->n_accepted       = accepted;
        event->cross_section()->values[0]        = value;
        event->cross_section()->uncertainties[0] = uncertainty;
        
      }
    };
    //----------------------------------------------------------------
    /**
       Structure of data to store in ROOT tree for pdf information

       @ingroup hepmc_root
       @headerfile root hepmc4/root
       @tparam T Floating point value type
    */
    template <typename T>
    struct pdf_data {
      /** Value type */
      using values_type=T;
      /** Value type */
      using event_ptr=hepmc4::event_ptr<values_type>;
      /** Builder type */
      using builder_type=hepmc4::builder<values_type>;
      /** Float specifier */
      constexpr static const char* float_letter=float_spec<T>::value;
      /** Leaf list */
      constexpr static const char* leaves =
        "pid1/I:pid2:id1:id2:scale/F:x1:x2:xf1:xf2";

      /** Parton Id */
      int pid1 = 0;
      /** Parton Id */
      int pid2 = 0;
      /** PDF Id */
      int id1 = 0;
      /** PDF Id */
      int id2 = 0;
      /** Scale */
      values_type scale = 0;
      /** parton 1 X */
      values_type x1 = 0;
      /** parton 2 X */
      values_type x2 = 0;
      /** PDF evaluated at parton 1 X */
      values_type xf1 = 0;
      /** PDF evaluated at parton 2 X */
      values_type xf2 = 0;

      /**
         Assign from an event

         @param event The event to assign from
         @return Refernece to self
      */
      pdf_data& operator=(const event_ptr& event)
      {
        pid1 = pid2 = id1 = id2 = 0;
        scale = x1 = x2 = xf1 = xf2 = 0;
        
        auto info = event->pdf_info();
        if (!info) return *this;
        pid1  = info->parton_id.first;
        pid2  = info->parton_id.second;
        id1   = info->pdf_id.first;
        id2   = info->pdf_id.second;
        scale = info->scale;
        x1    = info->x.first;
        x2    = info->x.second;
        xf1   = info->xf.first;
        xf2   = info->xf.second;

        return *this;
      }
      /**
         Store values in event

         @param event Event to store in
      */
      void operator()(event_ptr& event) const
      {
        if (not event->pdf_info())
          event->pdf_info() = builder_type::pdf_info();
        
        *event->pdf_info() = {
          {pid1,pid2},
          { id1, id2},
          scale,
          {  x1,  x2},
          { xf1, xf2}
        };
      }
    };
    //----------------------------------------------------------------
    /**
       Structure of data to store in ROOT tree for pdf information

       @ingroup hepmc_root
       @headerfile root hepmc4/root
       @tparam T Floating point value type
    */
    template <typename T>
    struct hi_data {
      /** Value type */
      using values_type=T;
      /** Value type */
      using event_ptr=hepmc4::event_ptr<values_type>;
      /** Builder type */
      using builder_type=hepmc4::builder<values_type>;
      /** Float specifier */
      constexpr static const char* float_letter=float_spec<T>::value;
      /** Leaf list */
      constexpr static const char* leaves =
        "nCollHard/I:nPartProj:nPartTarg:nWoundedN:nNWounded:nWoundedWounded:"
        "b/F:psi:sigma:c:nSpecProjN/I:nSpecTargN:nSpecProjP:nSpecTargP";

      /** number of hard collisions */
      int n_coll_hard = 0;
      /** number of participants */
      int n_part_proj = 0;
      /** number of participants */
      int n_part_targ = 0;
      /** number of collisions */
      int n_coll = 0;
      /** Number of projectile single-diffractive */
      int n_wounded_n = 0;
      /** Number of target single-diffractive */
      int n_n_wounded = 0;
      /** Number of double-diffractive */
      int n_wounded_wounded = 0;
      /** Impact parameter */
      values_type impact_parameter = 0;
      /** Event angle */
      values_type event_angle = 0;
      /** inelastic NN cross-section */
      values_type sigma_inel_nn = 0.;
      /** Centrality */
      values_type centrality = 0;
      /** Number of spectator neutrons */
      int n_spec_proj_n = 0;
      /** Number of spectator neutrons */
      int n_spec_targ_n = 0;
      /** Number of spectator neutrons */
      int n_spec_proj_p = 0;
      /** Number of spectator neutrons */
      int n_spec_targ_p = 0;
      

      /**
         Assign from an event

         @param event The event to assign from
         @return Refernece to self
      */
      hi_data& operator=(const event_ptr& event)
      {
        n_coll_hard = n_part_proj = n_part_targ = 0, n_coll = 0;
        n_wounded_n = n_n_wounded = n_wounded_wounded = 0;
        impact_parameter = event_angle = sigma_inel_nn = centrality = 0;
        n_spec_proj_n = n_spec_targ_n = n_spec_proj_p = n_spec_targ_p = 0;
        
        auto hi = event->heavy_ion();
        if (!hi) return *this;

        n_coll_hard		= hi->n_coll_hard;
        n_part_proj		= hi->n_participants_projectile;
        n_part_targ		= hi->n_participants_target;
        n_coll			= hi->n_coll;
        n_n_wounded		= hi->n_coll_n_wounded;
        n_wounded_n		= hi->n_coll_wounded_n;
        n_wounded_wounded	= hi->n_coll_wounded_wounded;
        impact_parameter	= hi->impact_parameter;
        event_angle	        = hi->event_plane_angle;
        sigma_inel_nn		= hi->sigma_inel_nn;
        centrality		= hi->centrality;
        n_spec_proj_n	        = hi->n_neutron_spectators_projectile;
        n_spec_targ_n	        = hi->n_neutron_spectators_target;
        n_spec_proj_p	        = hi->n_proton_spectators_projectile;
        n_spec_targ_p	        = hi->n_proton_spectators_target;

        return *this;
      }
      /**
         Store values in event

         @param event Event to store in
      */
      void operator()(event_ptr& event) const
      {
        if (not event->heavy_ion())
          event->heavy_ion() = builder_type::heavy_ion();
        
        *event->heavy_ion() = {
          n_coll_hard,
          n_part_proj,
          n_part_targ,
          n_coll,
          n_n_wounded,
          n_wounded_n,
          n_wounded_wounded,
          impact_parameter,
          event_angle,
          sigma_inel_nn,
          centrality,
          0,
          n_spec_proj_n,
          n_spec_targ_n,
          n_spec_proj_p,
          n_spec_targ_p,
          {},
          {},
          false,
          '1'
        };
      }
    };

#ifdef HEPMC4_HAS_ROOT
    //================================================================
    /**
       Base class for I/O of events from and to a ROOT TTree       

       @ingroup hepmc_root
       @headerfile root hepmc4/root
       @tparam T Floating point value type
    */
    template <typename T>
    struct base
    {
      /** Values type */
      using values_type=T;
      /** Events type */
      using event_type=hepmc4::event<values_type>;
      /** Event pointer */
      using event_ptr=hepmc4::event_ptr<values_type>;
      /** Particle pointer */
      using particle_ptr=typename event_type::particle_ptr;
      /** Vetex pointer */
      using vertex_ptr=typename event_type::vertex_ptr;
      /** positoin type */
      using position_type=typename event_type::vertex_type::position_type;
      /** positoin type */
      using number_type=typename event_type::number_type;
      /** Cross-section data type */
      using hd_type=hd_data<values_type>;
      /** Cross-section data type */
      using xs_type=xs_data<values_type>;
      /** PDF-information data type */
      using pdf_type=pdf_data<values_type>;
      /** Heavy-ion-information data type */
      using hi_type=hi_data<values_type>;
      /** Indexed event */
      using indexed_type = indexed_event<values_type>;
      /** First and last relative */
      using first_last_type = typename indexed_type::first_last_type;

      /**
         Destructor
      */
      virtual ~base()
      {
        TFile* file = _tree->GetCurrentFile();
        if (not file) return;
        if (file->IsWritable()) file->Write();
        file->Close();
        delete file;
      }
    protected:
      /** Tree */
      TTree*        _tree = nullptr;
      /** Array of particles */
      TClonesArray* _particles = nullptr;
      /** Cross-section branch, if any */
      TBranch*      _cross_section = nullptr;
      /** Parton distribution function branch, if any */
      TBranch*      _pdf_info = nullptr;
      /** Heavy ion branch, if any */
      TBranch*      _heavy_ion = nullptr;
      /** Header data */
      hd_type       _hd_data;
      /** Cross-section data */
      xs_type       _xs_data;
      /** PDF data */
      pdf_type      _pdf_data;
      /** Heavy-ion data */
      hi_type       _hi_data;
      /** First event */
      bool          _first = true;
    };
      
    //================================================================
    /**
       Write events to a ROOT TTree

       @ingroup hepmc_root
       @headerfile root hepmc4/root
       @tparam T Floating point value type
    */
    template <typename T>
    struct writer : public hepmc4::writer<T>, public base<T>
    {
      /** Values type */
      using values_type=T;
      /** Base type */
      using base_type=hepmc4::writer<values_type>;
      /** Base type */
      using root_type=hepmc4::root::base<values_type>;
      /** Events type */
      using event_type=typename root_type::event_type;
      /** Event pointer */
      using event_ptr=typename root_type::event_ptr;
      /** Particle pointer */
      using particle_ptr=typename root_type::particle_ptr;
      /** Particle pointer */
      using particle_type=typename event_type::particle_type;
      /** Vetex pointer */
      using vertex_ptr=typename root_type::vertex_ptr;
      /** positoin type */
      using position_type=typename root_type::position_type;
      /** positoin type */
      using number_type=typename root_type::number_type;
      /** Cross-section data type */
      using hd_type=typename root_type::hd_type;
      /** Cross-section data type */
      using xs_type=typename root_type::xs_type;
      /** PDF-information data type */
      using pdf_type=typename root_type::pdf_type;
      /** Heavy-ion-information data type */
      using hi_type=typename root_type::hi_type;
      /** Indexed event */
      using indexed_type=typename root_type::indexed_type;
      /** First and last relative */
      using first_last_type=typename root_type::first_last_type;
      /** Encoder type */
      using encoder_type=hepmc4::hepevt::encoder<T>;
      /** Indexer type */
      using indexer_type = hepmc4::depth_indexer<values_type>;

      /**
         Delete default constructor
      */
      writer() = delete;
      /**
         Delete copy copy constructor
      */
      writer(const writer&) = delete;
      /**
         Construct from an output stream.  The writer will generate
         one file, and that file will be named according to defaults.
         The file name is written to the output stream

         @param out Output stream
         @param filename Name of actual ROOT file to write to. 
      */
      writer(std::ostream& out,
             const std::string& filename=constants::default_file_name)
      {
        TFile* file = TFile::Open(filename.c_str(), "RECREATE");
        _tree       = new TTree(constants::tree_name,
                                (std::string("HepMC")+hepmc4::version::string)
                                .c_str());
        _particles  = new TClonesArray("TParticle");
        _tree->Branch(constants::particles_name, &_particles);
        _tree->Branch(constants::header_name, &_hd_data, hd_type::leaves);
        _tree->SetDirectory(file);

        out << constants::file_header << "\n"
            << "# Names of ROOT files generated\n"
            << constants::default_file_name << std::endl;
      }
      
      /**
         Constructor

         @param filename Name of file to write to 
      */
      writer(const std::string& filename)
      {
        TFile* file = TFile::Open(filename.c_str(), "RECREATE");
        _tree       = new TTree(constants::tree_name,
                                (std::string("HepMC")+hepmc4::version::string)
                                .c_str());
        _particles  = new TClonesArray("TParticle");
        _tree->Branch(constants::particles_name, &_particles);
        _tree->Branch(constants::header_name, &_hd_data, hd_type::leaves);
        _tree->SetDirectory(file);
      }
      /**
         Write one event

         @param event Event to write
         @return true on success
      */
      bool write_event(const event_ptr& event)
      {
        if (_first) {
          if (event->cross_section()) 
            _cross_section = _tree->Branch(constants::cross_section_name,
                                           &_xs_data, xs_type::leaves);
          if (event->pdf_info()) 
            _pdf_info = _tree->Branch(constants::pdf_info_name,
                                      &_pdf_data, pdf_type::leaves);
          if (event->heavy_ion()) 
            _heavy_ion = _tree->Branch(constants::heavy_ion_name,
                                      &_hi_data, hi_type::leaves);
          _first = false;
        }
        _hd_data = event;
        if (_cross_section) _xs_data  = event;
        if (_pdf_info)      _pdf_data = event;
        if (_heavy_ion)     _hi_data  = event;

        // Index event (by depth)
        indexer_type indexer;
        indexed_type indexed(indexer(event));

        _particles->Clear();
        
        auto encode_particle = [this](number_type            id,
                                      const particle_ptr&    p,
                                      const position_type&   v,
                                      const first_last_type& m,
                                      const first_last_type& d) {
          auto idx = id - 1;
          TClonesArray& arr = *_particles;
          auto rp = new(arr[idx]) TParticle(p->pid,
                                            p->status,
                                            m.first-1,
                                            m.second-1,
                                            d.first-1,
                                            d.second-1,
                                            p->momentum[0],
                                            p->momentum[1],
                                            p->momentum[2],
                                            p->momentum[3],
                                            v[0], v[1], v[2], v[3]);
          rp->SetCalcMass  (p->mass);
          auto cp = std::const_pointer_cast<const particle_type>(p);
          values_type theta = cp->theta();
          values_type phi   = cp->phi();
          rp->SetPolarTheta(theta == 0 ? -99 : theta);
          rp->SetPolarPhi  (phi   == 0 ? -99 : phi);

          return true;
        };
        
        encoder_type::encode_particles(indexed, encode_particle);

        _tree->Fill();
          
        return true;
      }
    protected:
      using root_type::_tree;
      using root_type::_particles;
      using root_type::_cross_section;
      using root_type::_pdf_info;
      using root_type::_heavy_ion;
      using root_type::_hd_data;
      using root_type::_xs_data;
      using root_type::_pdf_data;
      using root_type::_hi_data;
      using root_type::_first;
    };
    //================================================================
    /**
       Read events from a ROOT TTree

       @ingroup hepmc_root
       @headerfile root hepmc4/root
       @tparam T Floating point value type
    */
    template <typename T>
    struct reader : public hepmc4::reader<T>, public base<T>
    {
      /** Values type */
      using values_type=T;
      /** Base type */
      using base_type=hepmc4::reader<values_type>;
      /** Base type */
      using root_type=hepmc4::root::base<values_type>;
      /** Events type */
      using event_type=typename root_type::event_type;
      /** Event pointer */
      using event_ptr=typename root_type::event_ptr;
      /** Particle pointer */
      using particle_ptr=typename root_type::particle_ptr;
      /** Vetex pointer */
      using vertex_ptr=typename root_type::vertex_ptr;
      /** positoin type */
      using position_type=typename root_type::position_type;
      /** positoin type */
      using number_type=typename root_type::number_type;
      /** Cross-section data type */
      using xs_type=typename root_type::xs_type;
      /** PDF-information data type */
      using pdf_type=typename root_type::pdf_type;
      /** Heavy-ion-information data type */
      using hi_type=typename root_type::hi_type;
      /** Indexed event */
      using indexed_type=typename root_type::indexed_type;
      /** First and last relative */
      using first_last_type=typename root_type::first_last_type;
      /** Encoder type */
      using decoder_type=hepmc4::hepevt::decoder<T>;
      /** Encoder type */
      using return_type=typename decoder_type::return_type;
      /** Builder type */
      using builder_type=hepmc4::builder<values_type>;

      /**
         Delete default constructor
      */
      reader() = delete;
      /**
         Delete copy copy constructor
      */
      reader(const reader&) = delete;
      /**
         Construct from a stream.  Stream should be clear text, and
         each line should be a file to read from.  Lines starting
         with '#' are considered comments

         @param in Input stream containing lines of file names to read from
         @param header_read Assume header was read from the stream 
      */ 
      reader(std::istream& in, bool header_read=false)
      {
        TChain* chain = new TChain(constants::tree_name);
        _tree      = chain;
        _particles = new TClonesArray("TParticle");
        _tree->SetBranchAddress(constants::header_name, &_hd_data);        
        _tree->SetBranchAddress(constants::particles_name, &_particles);

        std::string line;
        if (not header_read) {
          std::getline(in,line);
          if (line.find(constants::file_header) != 0)
            throw std::runtime_error("Failed to get ROOT-list header");
        }
        while (std::getline(in, line)) {
          trim(line); // Remove leading and trailing whitespace
          if (line[0] == '#') continue;
          chain->AddFile(line.c_str());
        }
      }
      /**
         Constructor

         @param filename ROOT file to read from 
      */
      reader(const std::string& filename)
      {
        TChain* chain = new TChain(constants::tree_name);
        chain->AddFile(filename.c_str());
        _tree      = chain;
        _particles = new TClonesArray("TParticle");
        _tree->SetBranchAddress(constants::header_name, &_hd_data);        
        _tree->SetBranchAddress(constants::particles_name, &_particles);
      }
      /**
         Read one event

         @param event Event to read in
         @return true on success, false if no more events to read
         @throws std::runtime_error on error 
      */
      bool read_event(event_ptr& event) override
      {
        if (_first) {
          _cross_section = _tree->GetBranch(constants::cross_section_name);
          _pdf_info      = _tree->GetBranch(constants::pdf_info_name);
          _heavy_ion     = _tree->GetBranch(constants::heavy_ion_name);
          
          if (_cross_section)
            _tree->SetBranchAddress(constants::cross_section_name,&_xs_data);
          if (_pdf_info)
            _tree->SetBranchAddress(constants::pdf_info_name,&_pdf_data);
          if (_heavy_ion)
            _tree->SetBranchAddress(constants::heavy_ion_name,&_hi_data);
          _first = false;
        }
        event->clear();
        _particles->Clear();
        
        long ret = _tree->GetEntry(_entry);
        if (ret < 0)
          throw std::runtime_error("Failed to read entry # "+
                                   std::to_string(_entry)+
                                   " from tree "+_tree->GetName()+" in file "+
                                   _tree->GetCurrentFile()->GetName()+
                                   ": "+std::to_string(ret));
        if (ret == 0) return false;

        _entry++;
        
        _hd_data(event);
        if (_cross_section) _xs_data (event);
        if (_pdf_info)      _pdf_data(event); 
        if (_heavy_ion)     _hi_data (event);

        auto decode = [this](number_type id) -> return_type {
          auto idx = id - 1;
          if (idx >= _particles->GetEntriesFast()) {
            return return_type(nullptr,{},{},{});
          }
          
          // Read in particles

          auto tparticle = static_cast<TParticle*>(_particles->At(idx));
          auto particle  = builder_type::particle(tparticle->Px(),
                                                  tparticle->Py(),
                                                  tparticle->Pz(),
                                                  tparticle->Energy(),
                                                  tparticle->GetPdgCode(),
                                                  tparticle->GetStatusCode(),
                                                  tparticle->GetCalcMass());


          first_last_type mothers = {tparticle->GetFirstMother()+1,
                                     tparticle->GetSecondMother()+1 };
          first_last_type daughters = {tparticle->GetFirstDaughter()+1,
                                       tparticle->GetLastDaughter()+1 };

          values_type theta = tparticle->GetPolarTheta();
          values_type phi   = tparticle->GetPolarPhi();
          if (not (isclose(theta,0) or isclose(theta,-99)) or
              not (isclose(phi,0)   or isclose(phi,  -99)))
            particle->polarity() = { theta, phi };
          
          position_type position{values_type(tparticle->Vx()),
                                 values_type(tparticle->Vy()),
                                 values_type(tparticle->Vz()),
                                 values_type(tparticle->T())};
          return std::make_tuple(particle,position,mothers,daughters);
        };
          
        decoder_type::decode_particles(event, decode);

        size_t nparticles = _particles->GetEntriesFast();
        if (nparticles != event->particles().size())
          throw std::runtime_error("Event has "+std::to_string(nparticles)+
                                   " particles, but "+
                                   std::to_string(event->particles().size())+
                                   " was read");

        return true;
      }
      /**
         Skip one event

         @return true on success, false if no more events to read
      */
      bool skip_event() override
      {
        _entry++;
        
        return _entry < _tree->GetEntries();
      }
    protected:
      using root_type::_tree;
      using root_type::_particles;
      using root_type::_cross_section;
      using root_type::_pdf_info;
      using root_type::_heavy_ion;
      using root_type::_hd_data;
      using root_type::_xs_data;
      using root_type::_pdf_data;
      using root_type::_hi_data;
      using root_type::_first;
      long _entry = 0;
    };
#else
    template <typename T>
    struct writer : public hepmc4::writer<T>
    {
      writer(std::ostream&) {
        throw not_supported("ROOT I/O not supported.  Make sure that "
                            "HEPMC4_HAS_ROOT is defined and ROOT headers "
                            "and libraries can be found");
      }
      writer(const std::string&) {
        throw not_supported("ROOT I/O not supported.  Make sure that "
                            "HEPMC4_HAS_ROOT is defined and ROOT headers "
                            "and libraries can be found");
      }
      bool write_event(const hepmc4::event_ptr<T>&) override { return true; }
    };
    template <typename T>
    struct reader : public hepmc4::reader<T>
    {
      reader(std::istream&,bool=false) {
        throw not_supported("ROOT I/O not supported.  Make sure that "
                            "HEPMC4_HAS_ROOT is defined and ROOT headers "
                            "and libraries can be found");
      }
      reader(const std::string&) {
        throw not_supported("ROOT I/O not supported.  Make sure that "
                            "HEPMC4_HAS_ROOT is defined and ROOT headers "
                            "and libraries can be found");
      }
      bool read_event(hepmc4::event_ptr<T>&) override { return true; }
      bool skip_event() override { return true; }
    };
#endif
    // -------------------------------------------------------------------
    /** Clear attributes of an attribute container */
    template <typename T>
    void clear_attr(T o)
    {
      auto p = std::dynamic_pointer_cast<with_attributes>(o);
      if (p) {
        for (auto a : p->attributes()) {
          if (a.first == constants::mpi ||
              a.first == constants::event_scale ||
              a.first == constants::alpha_qed ||
              a.first == constants::alpha_qcd ||
              a.first == constants::signal_process_id ||
              a.first == constants::signal_process_vertex ||
              a.first == particle<T>::polarity_name or
              a.first == cross_section<T>::name or
              a.first == pdf_info<T>::name or
              a.first == heavy_ion<T>::name)
            a.second->persistent = true;
        }
        p->clear(false,false);
        for (auto a : p->attributes()) {
          if (a.first == constants::mpi ||
              a.first == constants::event_scale ||
              a.first == constants::alpha_qed ||
              a.first == constants::alpha_qcd ||
              a.first == constants::signal_process_id ||
              a.first == constants::signal_process_vertex ||
              a.first == particle<T>::polarity_name or
              a.first == cross_section<T>::name or
              a.first == pdf_info<T>::name or
              a.first == heavy_ion<T>::name)
            a.second->persistent = false;
        }
      }
      else
        std::cerr << "Cannot dynamic cast to with_attributes" << std::endl;
    }
    // -------------------------------------------------------------------
    /**
       Clear non-ASCII2 attributes as they are not stored by ASCII2.
       This allows us to compare the events after reading it back
    */
    template <typename T>
    void bare_bones(event_ptr<T>& event)
    {
      clear_attr(event);
      clear_attr(event->run_info());
      for (auto p : event->particles()) clear_attr(p);
      for (auto v : event->vertices())  {
        clear_attr(v);
        v->status = 0;
      }
      event->run_info()->weights().clear(true);
    }
  }
}
#endif
// Local Variables:
//  mode: C++
// End:

 
