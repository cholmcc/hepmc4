//  Library for handling HEP MC data
//  Copyright (C) 2023  Christian Holm Christensen
//
//  This program is free software: you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation, either version 3 of
//  the License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see
//  <https://www.gnu.org/licenses/>.
//                              
/**
   @file      hepevt
   @copyright (c) 2023 Christian Holm Christensen
   @brief     HEPEVT interface 
   @ingroup   hepmc_hepevt
*/
#ifndef HEPMC4_HEPEVT
#define HEPMC4_HEPEVT
#include <hepmc4/ascii>
#include <tuple>

namespace hepmc4
{
  /**
     @defgroup hepmc_hepevt Interface to HEPEVT event record
     @ingroup hepmc
     
     Classes and functions to interface the (legacy) HEPEVT event
     record.
     
     The HEPEVT event structure is described by the Fortran `COMMON` block
      
           PARAMETER (HEPEVT_MAX=...)
           COMMON /HEPEVT/
          +   NEVHEP
          +   NHEP
          +   ISTHEP[HEPEVT_MAX]
          +   IDHEP[HEPEVT_MAX]
          +   JMOHEP[2][HEPEVT_MAX]
          +   JDAHEP[2][HEPEVT_MAX]
          +   PHEP[5][HEPEVT_MAX]
          +   VHEP[4][HEPEVT_MAX]
           INTEGER NEVHEP,NHEP,ISTHEP,IDHEP,JMOHEP,JDAHEP
           DOUBLE PRECISION PHEP,VHEP

     where `HEPEVT_MAX` is the maximum number of particles that can be
     store, and

     - `NEVHEP`       is the event number
     - `NHEP`         is the number of particles in the event
     - `ISTHEP[i]`    is the status code of a particle _i_
     - `IDHEP[i]`     is the particle identity (PDG number) of particle [i]
     - `JMOHEP[2][i]` are the indices (starting from 1) of the first
                      and last mother particles of particle _i_.
                      E.g., if a particle _i_ has _j_ and _k_ here,
                      then all particles between _j_ and _k_
                      (inclusive) are incoming particles of the
                      production vertex of particle _i_.
     - `JDAHEP[2][i]` are the indices (starting from 1) of the first
                      and last _possible_ daughter of particle _i_.  A
                      particle is only truly a daughter if its
                      `JMOHEP` range contains the mother particle
                      index.  That is, if particle _i_ has _j_ and _k_
                      here, then all particles between _j_ and _k_
                      (inclusive) _may_ be outgoing particles of the
                      termination vertex of particle _i_.  A particle
                      _m_ in the range _j_ through _k_ of particle _i_
                      _is_ a daughter _only if_ the range
                      `JMOHEP[2][m]` contains _i_.
     - `PHEP[5][i]`   Four momentum $`[p_x,p_y,p_z,E]`$ and generated
                      mass $`m`$ of particle _i_.  Note that the mass
                      $`m`$ may be zero, in which case the particle is
                      considered to be on-shell.
     - `VHEP[4][i]`   Four position $`[x,y,ztE]`$ of the production
                      vertex of particle _i_.  Note particles may
                      share a production vertex, but the may not
                      necessarily agree upon the position of that
                      vertex.  The position of the termination vertex
                      of a particle, if there is one, is given by its
                      daughter particles as given by `JDAHEP`.

     The corresponding C++ structure of the Fortran common block is given by

         const size_t hepevt_max = ...;
         struct Hepevt {
           int    nevhep;
           int    nhep;
           int    isthep[hepevt_max];
           int    idhep [hepevt_max];
           int    jmohep[hepevt_max][2];
           int    jdahep[hepevt_max][2];
           double phep[hepevt_max][5];
           double vhep[hepevt_max][4];
         };

         extern "C" struct Hepevt _hepevt;

     Note that column and row ordering is reversed between Fortran and
     C.  Typically, the application binary interface (ABI) of Fortran
     compilers lower case the name of all identifiers, and prefixes
     the identifiers by an underscore (e.g., `HEPEVT` -> `_hepevt`),
     though some compilers has a different Fortran ABI for Fortran
     (notabily some Windows compilers upper case all identifiers and
     do not prefix them).

     The interface here uses a templated structure for its reflection
     of the HEPEVT common block.  This allows interfacing third-party
     code with variable size of the common block (i.e., different
     sizes of `HEPEVT_MAX`) or different sizes of the contained data
     (e.g., `REAL` instead of `DOUBLE PRECISION` for `PHEP` and `VHEP`
     or `INTEGER*2` instead of `INTEGER` for the integer fields.

     On disk, the HEPEVT event record can be encoded in two ways:
     _summary_ or _full_.  Both formats have the top-level structure 

         event : header
                 particle-list

         header : "E" event-number n-particles
         
         particle-list :
                       | particle
                       | particle-list 

     What differs between the summary and full event record formats,
     is how the particles are written.  The full record has the format

         particle : particle-first-line
                    particle-second-line

         particle-first-line : status pid mothers daughters momentum mass

         particle-second-line : spaces position

         mothers : first-mother last-mother

         daughters : first-daughter last-daughter

         momentum : px py pz E

         position : x y z t

         spaces : " "*48

     The summary particle format is simply

         particle : status pid daughters px py pz m

     Above, the formatting of the various fields are stricly defined, so that

     - integer numbers (`status`, `pid`, `first-mother`,
       `last-mother`, `first-daughter`, and `last-daughter`) are
       written as space-padded 8 characters.

     - real numbers (`px`, `py`, `pz`, `E`, `x`, `y`, `z`, and `t`)
       are written in scientific notation (with a capital `E`), space
       padded 19 chararacters wide with a precision of 8.

     The hepmc4::hepevt::writer class respects these conventions,
     while the hepmc4::hepevt::reader is more loose and forgiving.
  */
  /**
     Name space for HEPEVT event record interface code.

     The page @ref hepmc_hepevt describes the format
     
     @ingroup hepmc_hepevt
     @headerfile hepevt hepmc4/hepevt
  */
  namespace hepevt
  {
    //================================================================
    /**
       Interface to HEPEVT common block

       @see hepmc_hepevt
       
       @ingroup hepmc_hepevt
       @headerfile hepevt hepmc4/hepevt
       
       @tparam N Maximum number of particles
       @tparam R Floating point type
       @tparam I Integer type 
    */
    template <size_t N, typename R=double, typename I=int>
    struct common
    {
      /** Integer type */
      using integer_type=I;
      /** Real type */
      using real_type=R;
      /** Size of the common block */
      constexpr static int size = N;

      /** Event number */
      integer_type nevhep;
      /** Number of particles in event (at most N) */
      integer_type nhep;
      /** status code of particles */
      integer_type isthep[N];
      /** ID of particles (PDG code) */
      integer_type idhep[N];
      /** First and last mother index (1-based) of particles */
      integer_type jmohep[N][2];
      /** First and last daughter index (1-based) of particles */
      integer_type jdahep[N][2];
      /** Four-momentum and mass particles */
      real_type phep[N][5];
      /** Production vertex position of particles */
      real_type vhep[N][4];
    };

    /**
       Stream HEPEVT structure
       
       @tparam N Maximum number of particles
       @tparam R Floating point type
       @tparam I Integer type
       @param o Output stream
       @param c HEPEVT structure
       @return @a o
       @ingroup hepmc_hepevt
    */
    template <size_t N, typename R=double, typename I=int>
    std::ostream& operator<<(std::ostream& o, const common<N,R,I>& c)
    {
      o << "HEPEVT Event # " << c.nevhep << " with " << c.nhep
        << " (" << N << " max) particles\n"
        << std::setw(4)      << "ID"               << " "
        << std::setw(2)      << "St"               << " "
        << std::setw(10)     << "PID"              << " "
        << std::setw(7)      << "Mothers"          << " "
        << std::setw(7)      << "Daugrts"         << " "
        << std::setw(4*10+3) << "--- Momentum ---" << " "
        << std::setw(10)     << "Mass"             << " "
        << std::setw(4*10+3) << "--- Position ---" << '\n';
      for (I i = 0; i < c.nhep; i++) 
        o << "\n"
          << std::setw(4)  << i               << " "
          << std::setw(2)  << c.isthep[i]     << " "
          << std::setw(10) << c.idhep[i]      << " "
          << std::setw(3)  << c.jmohep[i][0]  << " "
          << std::setw(3)  << c.jmohep[i][1]  << " "
          << std::setw(3)  << c.jdahep[i][0]  << " "
          << std::setw(3)  << c.jdahep[i][1]  << " "
          << std::setw(10) << c.phep[i][0]    << " "
          << std::setw(10) << c.phep[i][1]    << " "
          << std::setw(10) << c.phep[i][2]    << " "
          << std::setw(10) << c.phep[i][3]    << " "
          << std::setw(10) << c.phep[i][4]    << " "
          << std::setw(10) << c.vhep[i][0]    << " "
          << std::setw(10) << c.vhep[i][1]    << " "
          << std::setw(10) << c.vhep[i][2]    << " "
          << std::setw(10) << c.vhep[i][3]    << " ";
      
      return o;
    }
    //================================================================
    /**
       Encode a HEPEVT like event structure from an event

       The code here represents a generic way to encode a
       `hepmc4::event<T>` into a HEPEVT-like structure.  The call-back
       passed in is expected to write a particle to the target
       structure and have the prototype

            (int,particle_ptr,four_vector,std::pair,std::pair)->bool

       This code is used to write an event to the HEPEVT common block
       and to output formatted according to HEPEVT.  The code will
       also be used to export an event into a ROOT `TTree` with a
       `TClonesArray` of `TParticle` objects in a `TBranch`.
        
        @ingroup hepmc_hepevt
        @headerfile hepevt hepmc4/hepevt
        @tparam T Floating poing type (e.g., `float`)
     */
    template <typename T>
    struct encoder
    {
      /** Value type */
      using values_type=T;
      /** Event type */
      using event_type=hepmc4::event<values_type>;
      /** Event pointer */
      using event_ptr=hepmc4::event_ptr<values_type>;
      /** Particle pointer */
      using particle_ptr=typename event_type::particle_ptr;
      /** Particle pointer */
      using vertex_ptr=typename event_type::vertex_ptr;
      /** Particle pointer */
      using position_type=typename event_type::vertex_type::position_type;
      /** Id type */
      using number_type   = typename event_type::number_type;
      /** Indexed event type */
      using indexed_type=indexed_event<values_type>;
      /** First and last */
      using first_last_type=typename indexed_type::first_last_type;

      /**
         Encode particles from event

         @tparam Encode Function type of decoder
         @param indexed  Indexed event to encode from 
         @param encode_particle Function to encode particles
         @return true on success
      */
      template <typename Encode>
      static bool encode_particles(const indexed_type& indexed,
                                   Encode              encode_particle)
      {
        int id = 1;
        for (auto p : indexed.particles()) {
          auto prod = p->production();
          auto v    = (prod ? prod->position : position_type());
          auto m    = indexed.mother_ids  (p);
          auto d    = indexed.daughter_ids(p);

          if (not encode_particle(id++, p, v, m, d)) return false;
        }
        return true;
      }
    };

    //================================================================
    /**
       Decode a HEPEVT like event structure onto an event

       The code here represents a generic way to decode a HEPEVT-like
       structure into a `hepmc4::event<T>` .  The call-back passed in
       is expected to retrive a particle from the source structure and
       have the prototype

             (int)->(particle_ptr,four_vector,std::pair,std::pair)

       This code is used to read an event from the HEPEVT common
       block and from input formatted according to HEPEVT.  The code
       will also be used to import an event from a ROOT `TTree` with
       a `TClonesArray` of `TParticle` objects in a `TBranch`.
       
       @ingroup hepmc_hepevt
       @headerfile hepevt hepmc4/hepevt
       @tparam T Floating poing type (e.g., `float`)
    */
    template <typename T>
    struct decoder
    {
      /** Value type */
      using values_type=T;
      /** Event type */
      using event_type=hepmc4::event<values_type>;
      /** Event pointer */
      using event_ptr=hepmc4::event_ptr<values_type>;
      /** Particle pointer */
      using particle_ptr=typename event_type::particle_ptr;
      /** Particle pointer */
      using vertex_ptr=typename event_type::vertex_ptr;
      /** Particle pointer */
      using position_type=typename event_type::vertex_type::position_type;
      /** Id type */
      using number_type   = typename event_type::number_type;
      /** Map from ID to particle */
      using id_particle_map=std::unordered_map<number_type,particle_ptr>;
      /** Map from ID to particle */
      using id_particle_type=typename id_particle_map::value_type;
      /** First and last relative */
      using first_last_type=std::pair<number_type,number_type>;
      /** builder type */
      using builder_type=hepmc4::builder<values_type>;
      /** Return type of decoder */
      using return_type=std::tuple<particle_ptr,
                                   position_type,
                                   first_last_type,
                                   first_last_type>;
      
      /**
         Decode particles to event

         @tparam Decode Function type of decoder
         @param event   Event to fill into 
         @param decode_particle Function to decode particles
         @return true on success
      */
      template <typename Decode>
      static bool decode_particles(event_ptr& event,
                                   Decode     decode_particle)
      {
        id_particle_map id_particle;
        
        number_type id = 1;
        while(true) {
          auto ret         = decode_particle(id);
          particle_ptr    particle    = std::get<0>(ret);
          position_type   position    = std::get<1>(ret);
          first_last_type mothers     = std::get<2>(ret);
          first_last_type daughters   = std::get<3>(ret);
          if (not particle) break;
          
          number_type idx  = id - 1;
          id_particle[id] = particle;

          // Check if mothers have an end vertex 
          vertex_ptr prod = nullptr;
          for (int mid = mothers.first; mid <= mothers.second; mid++) {
            auto mother = id_particle[mid];
            if (not mother) continue;

            auto term = mother->termination();
            if (term) {
              prod = term;
              break;
            }
          }
          
          // If we didn't find a production vertex, make one and add
          // mothers to it.
          if (not prod and mothers.first != 0 and mothers.second != 0) {
            prod = builder_type::vertex(position, 0);
            event->add_vertex(prod);
            
            for (int mid = mothers.first; mid <= mothers.second; mid++) {
              auto mother = id_particle[mid];
              if (not mother) continue; // Shouldn't happen
              prod->add_incoming(mother);
            }
          }
          if (prod) {
            prod->add_outgoing(particle);
          }

          // Now check if we have a production vertex on any of the
          // daughters
          if (daughters.first < idx) {
            vertex_ptr term = 0;
          
            // Since we make the particles sequentially, we can deduce
            // whether we've made the particles at this point.
            for (int did = daughters.first;
                 did <= std::min(daughters.second,idx); did++) {
              auto daughter = id_particle[did];
              if (not daughter)  continue;

              auto dprod = daughter->production();
              if (dprod) {
                term = dprod;
                break;
              }
            }
            // If we found a daughter production vertex, then add this
            // particle as incoming to that vertex
            if (term) term->add_incoming(particle);
          }
          ++id;
        }
        return true;
      }
    };
      
    //================================================================
    /** Exports an event to the HEPEVT common structure

        @ingroup hepmc_hepevt
        @headerfile hepevt hepmc4/hepevt
        @tparam N Maximum number of particles
        @tparam T Floating point type of event
        @tparam TT Floating point type of HEPEVT
        @tparam I  Integer type of HEPEVT
        @param event  Event to convert to HEPEVT
        @param common HEPEVT common to write to 
     */
    template <size_t N,
              typename T,
              typename TT=T,
              typename I=typename event<T>::number_type>
    void convert(const event_ptr<T>& event, common<N,TT,I>& common)
    {
      using values_type     = T;
      using event_type      = hepmc4::event<values_type>;
      using indexed_type    = indexed_event<values_type>;
      using indexer_type    = hepmc4::depth_indexer<values_type>;
      using position_type   = typename event_type::vertex_type::position_type;
      using particle_ptr    = typename event_type::particle_ptr;
      using number_type     = typename event_type::number_type;
      using encoder_type    = encoder<values_type>;
      using first_last_type = typename indexed_type::first_last_type;
      
      size_t nParticles = event->particles().size();
      if (N < nParticles) 
        throw std::runtime_error("HEPEVT too small "+std::to_string(N)+
                                 " to accommodate event with "+
                                 std::to_string(nParticles) + " particles");


      // Index event (by depth)
      indexer_type indexer;
      indexed_type indexed(indexer(event));
        
      // Write header 
      common.nevhep = event->number;
      common.nhep   = nParticles;

      auto encode_particle = [&common](number_type            id,
                                       const particle_ptr&    p,
                                       const position_type&   v,
                                       const first_last_type& m,
                                       const first_last_type& d) {
        auto idx = id - 1;
        common.isthep[idx]    = p->status;
        common.idhep [idx]    = p->pid;
        common.jmohep[idx][0] = m.first;
        common.jmohep[idx][1] = m.second;
        common.jdahep[idx][0] = d.first;
        common.jdahep[idx][1] = d.second;
        common.phep  [idx][0] = p->momentum[0];
        common.phep  [idx][1] = p->momentum[1];
        common.phep  [idx][2] = p->momentum[2];
        common.phep  [idx][3] = p->momentum[3];
        common.phep  [idx][4] = p->mass;
        common.vhep  [idx][0] = v[0];
        common.vhep  [idx][1] = v[1];
        common.vhep  [idx][2] = v[2];
        common.vhep  [idx][3] = v[3];
        return true;
      };

      encoder_type::encode_particles(indexed, encode_particle);
    }

    //================================================================
    /** Imports an event from the HEPEVT common structure

        @ingroup hepmc_hepevt
        @headerfile hepevt hepmc4/hepevt
        @tparam N Maximum number of particles
        @tparam T Floating point type of event
        @tparam TT Floating point type of HEPEVT
        @tparam I  Integer type of HEPEVT
        @param event  Event to read into
        @param common HEPEVT common to read from 
     */
    template <size_t N,
              typename T,
              typename TT=T,
              typename I=typename event<T>::number_type>
    void convert(const common<N,TT,I>& common, event_ptr<T>& event)
    {
      using values_type     = T;
      using event_type      = hepmc4::event<values_type>;
      using position_type   = typename event_type::vertex_type::position_type;
      using builder_type    = builder<values_type>;
      using number_type     = typename event_type::number_type;
      using decoder_type    = decoder<values_type>;
      using return_type     = typename decoder_type::return_type;
      
      // Clear event and set event number 
      event->clear();
      event->number = common.nevhep;
      event->reserve(common.nhep,0);
      
      auto decode = [&common](number_type id) -> return_type {
        if (id > common.nhep) 
          return return_type(nullptr,{},{},{});
          
        number_type idx = id - 1;
        auto particle  = builder_type::particle(common.phep[idx][0],
                                                common.phep[idx][1],
                                                common.phep[idx][2],
                                                common.phep[idx][3],
                                                common.idhep[idx],
                                                common.isthep[idx],
                                                common.phep[idx][4]);
        auto mothers   = std::make_pair(common.jmohep[idx][0],
                                        common.jmohep[idx][1]);
        auto daughters = std::make_pair(common.jdahep[idx][0],
                                        common.jdahep[idx][1]);
        auto position  = position_type(common.vhep[idx][0],
                                       common.vhep[idx][1],
                                       common.vhep[idx][2],
                                       common.vhep[idx][3]);

        return std::make_tuple(particle,position,mothers,daughters);
      };

      decoder_type::decode_particles(event, decode);
    }
        
    //================================================================
    /**
       @defgroup hepmc_hepevt_io Input/output in HEPEVT format
       @ingroup hepmc_io

       See also the page on @ref hepmc_hepevt
    */
    /**
       Constants used in HEPEVT output

       @headerfile hepevt hepmc4/hepevt
       @ingroup hepmc_hepevt_io
    */
    struct constants {
      constexpr static const char* start = "";
      constexpr static const char* end   = "";
      constexpr static char event_mark = 'E';
    };

    //================================================================
    /**
       Type of format

       @headerfile hepevt hepmc4/hepevt
       @ingroup hepmc_hepevt_io
    */
    enum class format_type {
      full,
      summary
    };
      
    
    //================================================================
    /**
       Writer of HEPEVT input from an event 
       
       @ingroup hepmc_hepevt_io
       @headerfile hepevt hepmc4/hepevt
       @tparam T Floating point type 
     */
    template <typename T>
    struct writer : public ascii_writer<constants,T,depth_indexer>
    {
      /** base type */
      using base_type=ascii_writer<constants,T,depth_indexer>;
      /** Type of value */
      using constants_type=typename base_type::constants_type;
      /** Type of value */
      using values_type=typename base_type::values_type;
      /** Type of events */
      using event_type=typename base_type::event_type;
      /** Type of event pointer */
      using event_ptr=typename base_type::event_ptr;
      /** Type of particle pointer */
      using particle_ptr=typename base_type::particle_ptr;
      /** Indexed event type */
      using indexed_type=typename base_type::indexed_type;
      /** Indexer type */
      using indexer_type=typename base_type::indexer_type;
      /** First and last */
      using first_last_type=typename indexed_type::first_last_type;
      /** Four-vector type */
      using position_type=typename event_type::vertex_type::position_type;
      /** Encoder type */
      using encoder_type=encoder<values_type>;
      using number_type   = typename event_type::number_type;
      
      /**
         Default constructor deleted
      */
      writer() = delete;
      /**
         Copy constructor deleted
      */
      writer(const writer&) = delete;
      
      /**
         Constructor

         @param output Stream to write to
         @param format Type of format to use (full or summary)
      */
      writer(std::ostream& output, format_type format=format_type::full)
        : base_type(output, depth_indexer<values_type>()),
          _format(format)
      {}

    protected:
      /** Base member */
      using base_type::_out;
      
      constexpr static size_t int_width=8;
      constexpr static size_t value_width=19;
      constexpr static size_t value_precision=8;
      /** format to write */
      format_type _format = format_type::full;
      
      /**
         Write out the event header.

         @param event   Event to write header for
         @param indexed Event indexed.
         @return true on success
      */
      bool write_event_header(const event_ptr& event,
                              const indexed_type& indexed) override
      {
        _out << constants_type::event_mark
                   << std::right
                   << std::setw(int_width) << event->number
                   << std::setw(int_width) << indexed.particles().size()
                   << std::left
                   << std::endl;
        return true;
      }
      /**
         Write out particles.

         The particles and vertices are sorted by vertex depth
         
         @param indexed Indexes of particles and verticeis

         @return `true` on success 
      */
      bool write_particles_and_vertices(const event_ptr& ,
                                        const indexed_type& indexed) override
      {
        auto& o = _out;
        auto full = [&o](number_type            /*id*/,
                         const particle_ptr&    p,
                         const position_type&   v,
                         const first_last_type& m,
                         const first_last_type& d) {
          o << std::setw(int_width)   << p->status
            << std::setw(int_width)   << p->pid
            << std::setw(int_width)   << m.first
            << std::setw(int_width)   << m.second
            << std::setw(int_width)   << d.first
            << std::setw(int_width)   << d.second
            << std::setw(value_width) << p->momentum[0]
            << std::setw(value_width) << p->momentum[1]
            << std::setw(value_width) << p->momentum[2]
            << std::setw(value_width) << p->momentum[3]
            << std::setw(value_width) << p->mass
            << "\n"
            << std::setw(6*int_width) << " "
            << std::setw(value_width) << v[0]
            << std::setw(value_width) << v[1]
            << std::setw(value_width) << v[2]
            << std::setw(value_width) << v[3]
            << '\n';
          return true;
        };
        
        auto summary = [&o](number_type            /*id*/,
                            const particle_ptr&    p,
                            const position_type&   /*v*/,
                            const first_last_type& /*m*/,
                            const first_last_type& d) {
          // Calculate mass if it is zero?
          auto m0  = (isclose(p->mass,0) ? p->momentum.invariant() : p->mass);
          o << std::setw(int_width)   << p->status
            << std::setw(int_width)   << p->pid
            << std::setw(int_width)   << d.first
            << std::setw(int_width)   << d.second
            << std::setw(value_width) << p->momentum[0]
            << std::setw(value_width) << p->momentum[1]
            << std::setw(value_width) << p->momentum[2]
            << std::setw(value_width) << m0
            << '\n';
          return true;
        };

        _out << std::right
                   << std::scientific
                   << std::uppercase;
        if (_format == format_type::full)
          encoder_type::encode_particles(indexed,full);
        else 
          encoder_type::encode_particles(indexed,summary);

        _out << std::left
                   << std::defaultfloat
                   << std::nouppercase
                   << std::flush;
        return true;
      }        
    };

    // ===============================================================
    /**
       Read events from HEPET ASCII file 

       @ingroup hepmc_hepevt_io
       @headerfile hepevt hepmc4/hepevt
       @tparam T Floating point type 
    */
    template <typename T=default_values_type>
    struct reader : public ascii_reader<constants,T> 
    {
      /** base type */
      using base_type=ascii_reader<constants,T>;
      /** Type of string constants */
      using constants_type=typename base_type::constants_type;
      /** Type of value */
      using values_type=typename base_type::values_type;
      /** Type of events */
      using event_type=typename base_type::event_type;
      /** Type of event pointer */
      using event_ptr=typename base_type::event_ptr;
      /** Type of events */
      using position_type=typename event_type::vertex_type::position_type;
      /** Type of vertex pointer */
      using vertex_ptr=typename base_type::vertex_ptr;
      /** Type of particle pointer */
      using particle_ptr=typename base_type::particle_ptr;
      /** Number type */
      using number_type=typename event_type::number_type;
      /** Builder type */
      using builder_type=typename base_type::builder_type;
      /** Decoder type */
      using decoder_type=decoder<values_type>;
      /** Return type */
      using return_type=typename decoder_type::return_type;
      /** Return type */
      using first_last_type=typename decoder_type::first_last_type;

      /**
         Delete default constructor
      */
      reader() = delete;
      /**
         Delete copy constructor
       */
      reader(const reader&) = delete;
      
      /**
         Constructor

         @param in          Input stream to read from
         @param format      Expected format. 
      */
      reader(std::istream& in, 
             format_type format=format_type::full)
        : base_type(in,false), _format(format)
      {}
      /**
         Constructor

         @param in          Input stream to read from
         @param header_read If true, assume file header already read
         @param format      Expected format. 
      */
      reader(std::istream& in, bool header_read,
             format_type format=format_type::full)
        : base_type(in,header_read), _format(format)
      {}
    protected:
      /** Base member */
      using base_type::_in;
      /** Base member */
      using base_type::throw_error;
      /** Mapping from id to particle */
      using id_particle_map=std::unordered_map<int,particle_ptr>;
      /** format to write */
      format_type _format = format_type::full;

      /**
         Override to do nothing
      */
      void read_file_header() override
      {
        if (this->_header_read) this->_token = "E";
        this->_header_read = true;
        this->_line_no = 0;
      }
      /**
         Read in body of an event

         @param event event to read into.
         @return false if no more data to read.
         @throws std::runtime_error on error
      */
      bool read_body(event_ptr& event) override
      {
        // Read in header
        auto& token = this->token();
        if (this->is_file_footer(token)) return false;

        // Read event number and number of particles 
        number_type number;
        size_t n_particles;
        _in >> number >> n_particles;
        if (_in.fail()) 
          throw_error("Failed to read event line");

        event->number = number;
        token.clear();

        auto& i = _in;
        
        auto decode = [&i,this](number_type id) -> return_type {
          // Skip white-space
          while (std::isspace(i.peek()) and not i.eof()) i.get();

          // Check for end of file or next event 
          if (i.eof() or i.peek() == constants_type::event_mark) 
            return return_type(nullptr,{},{},{});
          

          // Read in particles
          int status, pid;
          first_last_type mothers;
          first_last_type daughters;
          
          i >> status >> pid >> daughters.first >> daughters.second;
          if (i.eof())
            return return_type(nullptr,{},{},{});
          if (i.fail()) 
            throw_error("Failed to read start of particle "+
                              std::to_string(id));

          auto particle = builder_type::particle(0,0,0,0,pid,status);

          if (this->_format == format_type::full) {
            std::swap(mothers,daughters);
            i >> daughters.first >> daughters.second;
            if (not i)
              throw_error("Failed to read full daughters");
          }

          values_type px, py, pz, e, m;
          i >> px >> py >> pz >> m;
          if (not i)
            throw_error("Failed to read 3-momentum");
          
          if (this->_format == format_type::full) {
            e = m;
            i >> m;
            if (not i)
              throw_error("Failed to read full mass");
          }
          else
            e = std::sqrt(px*px+py*py+pz*pz+m*m);

          particle->momentum = {px,py,pz,e};
          particle->mass     = m;
          this->_line_no++;

          position_type position;
          if (this->_format == format_type::full) {
            i >> position[0] >> position[1] >> position[2] >> position[3];
            this->_line_no++;
            if (not i)
              throw_error("Failed to read full production position");
          }
          return std::make_tuple(particle,position,mothers,daughters);
        };
          
        decoder_type::decode_particles(event, decode);

        if (n_particles != event->particles().size())
          throw_error("Event has "+std::to_string(n_particles)+
                      " particles, but only "+
                      std::to_string(event->particles().size())+" was read");

        return true;
      }
    };    
    // -------------------------------------------------------------------
    /**
       Clear attributes of an attribute container
       
       @headerfile hepevt hepmc4/hepevt
       @ingroup hepmc_hepevt
    */
    template <typename T>
    void clear_attr(T o)
    {
      auto p = std::dynamic_pointer_cast<with_attributes>(o);
      if (p)
        p->clear(true);
      else
        std::cerr << "Cannot dynamic cast to with_attributes" << std::endl;
    }

    // -------------------------------------------------------------------
    /**
       Clear attributes as they are not stored by HEPEVT.  This allows
       us to compare the events after reading it back
       
       @headerfile hepevt hepmc4/hepevt
    */
    template <typename T>
    void bare_bones(event_ptr<T>& event)
    {
      clear_attr(event);
      clear_attr(event->run_info());
      for (auto p : event->particles()) clear_attr(p);
      for (auto v : event->vertices())  {
        clear_attr(v);
        v->status = 0;
      }
      event->run_info()->weights().clear(true);
    }
  }
}
#endif
// Local Variables:
//  mode: C++
// End:
