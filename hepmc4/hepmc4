//  Library for handling HEP MC data
//  Copyright (C) 2023  Christian Holm Christensen
//
//  This program is free software: you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation, either version 3 of
//  the License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see
//  <https://www.gnu.org/licenses/>.
//                              
/**
   @file      hepmc4
   @copyright (c) 2023 Christian Holm Christensen
   @brief     General parameters and constants
   @ingroup   hepmc
*/
#ifndef HEPMC4_HEPMC4
#define HEPMC4_HEPMC4
#include <stdexcept>

/**
   @defgroup hepmc  HepMC4 template class library.

   A class library to handle High Energy Physics "Monte-Carlo" data. 
 */
namespace hepmc4
{
  /**
     @defgroup hepmc_const Constants
     @ingroup hepmc

     Various constants
  */
  /**
     Namespace for version numbers
     @ingroup hepmc_const
  */
  namespace version
  {
    /** @{
        @name Version number
        @headerfile hepmc4 hepmc4/hepmc4
    */
    /** HepMC4 major version */
    constexpr unsigned int major = 4;
    /** HepMC4 minor version */
    constexpr unsigned int minor = 0;
    /** HepMC4 patch version */
    constexpr unsigned int patch = 0;
    /** HepMC4 combined version number
        
        Lowest 8 bits (0..7) is the patch version
        Middle 8 bits (8..15) is the minor version
        Top 8 bits (16..23) is the major version
     */
    constexpr unsigned int value = ((major << 8) | minor) << 8 | patch;
    /** HepMC4 version string */
    constexpr char string[] = "4.0.0";
    /** @} */
  };
  /** @{
      @name Other constants
      @ingroup hepmc_const
  */
  /**
     HepMC4 copyright string
      
     @headerfile hepmc4 hepmc4/hepmc4
   */
  constexpr char copyright[] = "(c) 2023 Christian Holm Christensen";
  /** @} */

  /**
     @{
     @name Floating-point precision used
  */
  /**
     Default value type of four vectors
     @ingroup hepmc_const
     @headerfile hepmc4 hepmc4/hepmc4
  */
  using default_values_type=float;
  /* @} */

  /**
     Exception for not-supported features

     @ingroup hepmc
     @headerfile hepmc4 hepmc4/hepmc4
  */
  struct not_supported : public std::runtime_error
  {
    /**
       Constructor

       @param msg Message
    */
    not_supported(const std::string& msg)
      : std::runtime_error(msg)
    {}
    /**
       Constructor

       @param msg Message
    */
    explicit not_supported(const char* msg)
      : std::runtime_error(msg)
    {}
    not_supported(const not_supported&) = default;
    /**
       Default assignment operator 
    */
    not_supported& operator=(const not_supported&) = default;
    /**
       Default move constructor
    */
    not_supported(not_supported&&) = default;
    /**
       Default move assignment operator 
    */
    not_supported& operator=(not_supported&&) = default;
  };
  
}

//====================================================================
/**
   @example compare.cc

   Example of comparing events.

   This program reads in events from two sources, and compare them
   one-by-one using hepmc::event::operator==.

   Note, since some event formats does not support all features of an
   event, there are hooks to scrape the events of things that cannot
   be compared.
*/
//--------------------------------------------------------------------
/**
   @example convert.cc

   Convert input from one format to another format (or the same).
   
   This program reads in events from one source, and writes out the
   events in some format.

   Do

       ./convert -h

   for more information.
*/
//--------------------------------------------------------------------
/**
   @example generator

   Example of how a generator can look.  The header defines an
   abstract interface and a concrete implementation that generates a
   single @f$W^-@f$ event.
*/
//--------------------------------------------------------------------
/**
   @example w.cc

   Example of generating a @f$W^-@f$ event.  This uses the header
   `generator`.

                             p7
       p1                   /  
         \v1__p2      p5---v4  
               \_v3_/       \  
               /    \        p8
          v2__p4     \         
         /            p6       
       p3                      


   where

   | # | type | st | pdg  | par | px  | py   | pz     | E     | m    |
   |---|------|----|------|-----|-----|------|--------|-------|------|
   | 1 | p    | 4  | 2212 | 0,0 | 0   |  0   | 7000   |7000   | 0.938|
   | 2 | p    | 4  | 2212 | 0,0 | 0   |  0   |-7000   |7000   | 0.938|
   | 3 | d    | 3  |    1 | 1,1 | 0.75| -1.56|   32.19|  32.24| 0    |
   | 4 | ubar | 3  |   -2 | 2,2 |-3.04|-19.00|  -54.63|  57.92| 0    |
   | 5 | W-   | 2  |  -24 | 1,2 | 1.52|-20.68|  -20.61|  85.93|80.799|
   | 6 | gamma| 1  |   22 | 1,2 |-3.81|  0.11|   -1.83|   4.23| 0    |
   | 7 | d    | 1  |    1 | 5,5 |-2.45| 28.82|    6.08|  29.55| 0.01 |
   | 8 | ubar | 1  |    2 | 5,5 | 3.96|-49.50|  -26.69|  56.37| 0.006|

   ![W event](w.png)
*/
//--------------------------------------------------------------------
/**
   @example prune.cc

   Read events from input and _prune_ them.  By pruning, we mean only
   keep particles that satisfy a criteria, and remove all others.
   Also, as part of the process, short-circuit vertices where
   intermediate particles are removed.
  
   The default pruning is of particles that are _not_ 
   
   - beam particles, 
   - Decayed, _nor_
   - Final state particles. 
  
   Command line options allows tuning of which particles to keep,
   either selected by particle stats (as above) or by particle type
   identifier (PDG code).

   This can be used as a filter, for example

       ./pythia -n 1000 | prune > pythia.hepmc

   or

       ./angantyr -n 10 | prune > angantyr.hepmc

   For the default selection (beam, decayed, and final state
   particles), we find

   - Pythia8 inelastic pp collisions at @f$\sqrt{s}=14\mathrm{TeV}@f$
     are reduced by approximately 40% and 60% of particles and
     vertices, respectively (of up to ~1500 and ~1000 particles and
     vertices, respectively).

   - Pythia8/Angantyr min-bias Pb-Pb collisions at
     @f$\sqrt{s_{\mathrm{NN}}}=5.02\mathrm{TeV}@f$ are reduced by
     approximately 50% and 70% of particles and vertices, respectively
     (of up to ~100k and 50k particles and vertices, respectively).
   
*/
//--------------------------------------------------------------------
/**
   @example pythia.cc

   Example of using [Pythia8](https://pythia.org) to generate events
   and output them as HepMC.

   The exact format of the output is dicated by the requested filename.

   If the name of the executable is `angantyr`, then use the
   Pythia8/Angantyr heavy-ion model for the event generation.
   Otherwise, use the Pythia8/Monash tune for pp events.  When
   executed as `angantyr` additional command line arguments are
   available.

   To enable `angantyr`, simply build the program

       g++ pythia.cc -o pythia

   and create a symbolic link named `angantyr`

       ln -s pythia angantyr  

   Collision energy can be set by a command-line switch (`-e`).

   Other Pythia8 settings can be supplied via a Pythia8 configuration
   file specified by the `-c` command line switch.
   
*/
#endif
// Local Variables:
//  mode: C++
// End:
