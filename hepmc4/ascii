//  Library for handling HEP MC data
//  Copyright (C) 2023  Christian Holm Christensen
//
//  This program is free software: you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation, either version 3 of
//  the License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see
//  <https://www.gnu.org/licenses/>.
//
/**
   @file      ascii
   @copyright (c) 2023 Christian Holm Christensen
   @brief     Base classes for ASCII I/O
   @ingroup   hepmc_io
*/
#ifndef HEPMC4_ASCII
#define HEPMC4_ASCII
#include <hepmc4/io>
#include <hepmc4/indexed_event>
#include <iostream>
#include <iomanip>

namespace hepmc4
{
  /**
     Base class for ASCII writers

     @tparam C       constants type
     @tparam T       Floating point values type
     @tparam Indexer Indexer of particles and vertices 
     @ingroup hepmc_io
     @headerfile ascii hepmc4/ascii
  */
  template <typename C,
            typename T=default_values_type,
            template <typename> class Indexer=simple_indexer>
  struct ascii_writer : public writer<T> 
  {
    /** base type */
    using base_type=writer<T>;
    /** Type of value */
    using values_type=typename base_type::values_type;
    /** Type of string constants */
    using constants_type=C;
    /** Type of events */
    using event_type=typename base_type::event_type;
    /** Type of event pointer */
    using event_ptr=typename base_type::event_ptr;
    /** Type of vertex pointer */
    using vertex_ptr=typename event_type::vertex_ptr;
    /** Type of particle pointer */
    using particle_ptr=typename event_type::particle_ptr;
    /** Type of heavy-ion pointer */
    using heavy_ion_ptr=typename event_type::heavy_ion_ptr;
    /** Type of pdf info pointer */
    using pdf_info_ptr=typename event_type::pdf_info_ptr;
    /** Type of cross-section pointer */
    using cross_section_ptr=typename event_type::cross_section_ptr;
    /** Indexed event type */
    using indexed_type=indexed_event<values_type>;
    /** Indexer type */
    using indexer_type=Indexer<values_type>;
    
    /**
       Constructor

       @param output  Output stream to write to
       @param indexer Indexer used to index particles and vertices
     */
    ascii_writer(std::ostream& output, indexer_type indexer=simple_indexer<T>())
      : _out(output),
        _indexer(indexer)
    {}
    /**
       Destructor.  Writes footer to output. 
    */
    virtual ~ascii_writer()
    {
      if (_footer_written) return;
      write_file_footer();
    }
    /**
       Write an event to output

       @param event  Event to write
       @return `true` on success. 
       @throws std::exception on error
    */
    bool write_event(const event_ptr& event) override
    {
      if (not _header_written) write_file_header();

      indexed_type indexed(_indexer(event));
      if (not write_event_header          (event, indexed)) return false;
      if (not write_particles_and_vertices(event, indexed)) return false;
      if (not write_event_footer          (event, indexed)) return false;
        
      return true;
    }
  protected:
    /** Whether the file header has been written */
    bool _header_written = false;
    /** Whether the file footer has been written */
    bool _footer_written = false;
    /** Output stream reference */
    std::ostream& _out;
    /** Indexer to use */
    indexer_type _indexer;

    /**
       @{
       @name Step member functions
    */
    /**
       Write the file header to file
    */
    void write_file_header()
    {
      //std::clog << "Output stream in writer @ " << &_out << std::endl;
      if (constants_type::start[0] != '\0'){
        _out << "HepMC::Version " << version::string << "\n"
             << "HepMC::" << constants_type::start << std::endl;
      }
      _header_written = true;
    }
    /**
       Write the file footer to file
    */
    void write_file_footer()
    {
      if (constants_type::end[0] != '\0') 
        _out << "HepMC::" << constants_type::end << std::endl;
      _footer_written = true;
    }
    /** @} */
    /**
       @{
       @name member functions to be overwritten
    */
    /**
       Write event header

       @param event Event to write header for
       @return true on success 
    */
    virtual bool write_event_header(const event_ptr& event,
                                    const indexed_type& /*indexed*/)
    {
      _out << constants_type::event_mark << " " << event->number;
      return true;
    }
    /**
       Write event footer

       @return `true` on success
     */
    virtual bool write_event_footer(const event_ptr&,
                                    const indexed_type&)
    {
      _out << std::flush;
      return true;
    }
    /**
       Write particles and vertices

       @return true on success
     */
    virtual bool write_particles_and_vertices(const event_ptr&,
                                              const indexed_type&) = 0;
    /** @} */
  };

  //==================================================================
  /**
     Base class for ASCII readers

     @ingroup hepmc_io
     @headerfile ascii hepmc4/ascii
  */
  template <typename C,
            typename T=default_values_type>
  struct ascii_reader : public reader<T> 
  {
    /** base type */
    using base_type=reader<T>;
    /** Type of string constants */
    using constants_type=C;
    /** Type of value */
    using values_type=typename base_type::values_type;
    /** Type of events */
    using event_type=typename base_type::event_type;
    /** Type of event pointer */
    using event_ptr=typename base_type::event_ptr;
    /** Type of vertex pointer */
    using vertex_ptr=typename event_type::vertex_ptr;
    /** Type of particle pointer */
    using particle_ptr=typename event_type::particle_ptr;
    /** Type of heavy-ion pointer */
    using heavy_ion_ptr=typename event_type::heavy_ion_ptr;
    /** Type of pdf info pointer */
    using pdf_info_ptr=typename event_type::pdf_info_ptr;
    /** Type of cross-section pointer */
    using cross_section_ptr=typename event_type::cross_section_ptr;
    /** Type of builder */
    using builder_type=typename base_type::builder_type;

    /**
       Delete default constructor
    */
    ascii_reader() = delete;
    /**
       Delete copy constructor
    */
    ascii_reader(const ascii_reader&) = delete;
    /** Constructor

        @param in          Input stream to read from
        @param header_read if true, then the reader will assume that
                           the file header has already been read, for
                           example by code that tries to deduce the
                           content of a file or stream.
     */
    ascii_reader(std::istream& in, bool header_read=false)
      : _header_read(header_read), _in(in)
    {}
    
    /**
       Read in events.

       @param event Event to read into
       
       @return true in an event was read, false if there are no more
       events to read.
       @throws std::exception on error
    */
    bool read_event(event_ptr& event) override
    {
      if (_in.eof() or _in.bad()) return false;
      if (not _header_read) read_file_header();
      if (_footer_read) return false;

      event->clear();
      return read_body(event);
    }
    /**
       Skip a single event
       
       @return true in an event was read, false if there are no more
       events to read.
       @throws std::exception on error
    */
    bool skip_event() override {
      if (_in.eof() or _in.bad()) return false;
      if (not _header_read) read_file_header();
      if (_footer_read) return false;

      // In case the reader needs some initial stuff (e.g., run_info),
      // and we're skipping the first event.
      if (not skip_front())
        throw_error("Failed to skip stuff at front of file");

      // Check that the next thing to read is indeed a start of a new
      // event. 
      if (not check_header())
        throw_error("Did not get event header");
      _token.clear();

      // Skip the event header line
      std::string line;
      std::getline(_in, line); 
      _line_no++;
      // std::clog << "Read line " << std::quoted(line) << std::endl;

      // Now loop as long as we do not have a new event start 
      while (not check_header() and not _in.eof()) {
        // Eat the line 
        std::getline(_in, line);
        _line_no++;
        // Check that we're not at the end of the event records.
        trim(line);
        if (is_file_footer(line)) return true;
      }
      
      return not _in.bad();
    }
      
  protected:
    /** Whether the file header has been written */
    bool _header_read = false;
    /** Whether the file footer has been written */
    bool _footer_read = false;
    /** Output stream reference */
    std::istream& _in;
    /** Rough line count */
    size_t _line_no = 0;
    /** The current token */
    std::string _token;
    /** End block to write */
    const std::string _end_block = (std::string("HepMC::")+
                                    constants_type::end);

    /**
       Get the token.  If current token is empty, read it.  Also
       increment the line number.  The token is returned as a mutable
       reference so that the caller may clear it.

       @return the next token 
    */
    virtual std::string& token()
    {
      if (_token.empty()) {
        _in >> _token;
        _line_no++;
        if (not _in)
          throw_error("Failed to read next token");
      }
      return _token;
    }
    /**
       Throw an exception

       @param msg Message of the exception 
       @throws std::exception on error
    */
    virtual void throw_error(const std::string& msg)
    {
      throw std::runtime_error("Line "+std::to_string(this->_line_no)
                               +": "+msg);
    }
      
    
    /**
       @{
       @name Step member functions
    */
    /**
       Read the file header from file (well, stream)
    */
    virtual void read_file_header()
    {
      if (_header_read) return;
      
      std::string line;
      std::getline(_in, line);
      while (line.empty()) {
        _line_no++;
        std::getline(_in, line);
      }
      
      if (line.find("HepMC::Version") == std::string::npos)
        throw_error("Wrong format line: "+line);

      std::getline(_in,line);
      if (line.find(std::string("HepMC::") + constants_type::start)
          == std::string::npos)
        throw_error("Wrong start line: "+line);

      _line_no += 1;
      _header_read = true;
    }
    /**
       Check wether read string is the end of events footer

       @param s String to check 
       @return true if the passed string @a s is a file footer 
    */
    virtual bool is_file_footer(const std::string& s)
    {
      bool is_end = (constants_type::end[0] != '\0' ?
                     s.find(_end_block) != std::string::npos :
                     false);
      _footer_read = is_end or _in.eof();
      // if (_footer_read) 
      //   std::clog << "At end " << s << " " << _in.eof() << std::endl;
      return _footer_read;
    }
    /**
       Read the event header

       @return `true` if token is event header 
    */
    virtual bool check_header()
    {
      if (not _token.empty())
        return _token[0] == constants_type::event_mark;
      
      char c = _in.peek();
      // std::cout << "peek -> " << c << std::endl;
      return c == constants_type::event_mark;
    }
    /** @} */
    /**
       @{
       @name member functions to be overwritten
    */
    /**
       Skip stuff at the beginning of the file

       @return true on success
       @throws std::exception on error
    */
    virtual bool skip_front() { return true; }
    
    /**
       Read the body of an event

       @param event Event to read into

       @return `true` on success, `false` if there are no more events
       to read.
       @throws std::exception on error
     */
    virtual bool read_body(event_ptr& event) = 0;
    /** @} */
  };  
}
  
  
#endif
// Local Variables:
//  mode: C++
// End:


