//  Library for handling HEP MC data
//  Copyright (C) 2023  Christian Holm Christensen
//
//  This program is free software: you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation, either version 3 of
//  the License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see
//  <https://www.gnu.org/licenses/>.
//                              
/**
   @file      attribute
   @copyright (c) 2023 Christian Holm Christensen
   @brief     Attribute template 
   @ingroup   hepmc_attr
*/
#ifndef HEPMC4_WITH_ATTRIBUTE
#define HEPMC4_WITH_ATTRIBUTE
#include <hepmc4/attribute_traits>
#include <memory>
#include <sstream>

namespace hepmc4
{
  //==================================================================
  /**
     @defgroup hepmc_attr Attributes
     @ingroup hepmc
     
     Attributes on vertices, particles, events, and runs.
     
     Users can use attributes to associate all sorts of things with
     types of objects.  For example, if a user need to store internal
     particle identifiers, then an attribute with that value can be
     added to particles.
  */
  /**
     Base class for attribute containers.  This case class defines
     the interface, and the flag `persistent`.  If this flag is set
     to `true` on some attribute, then that attribute will not be
     removed when an event is cleared.  Instead, it will stay, and
     the appropriate `reset` member function will be called.
     
     @ingroup hepmc_attr
     @headerfile attribute hepmc4/attribute
   */
  struct attribute_base
  {
    /** Convert to a string representation via trait */
    virtual std::ostream& format(std::ostream& o) const = 0;
    /** Convert to value from string representation via trait */
    virtual std::istream& parse(std::istream& i) = 0;
    /** Convert to a string via trait */
    virtual std::string to_string() const = 0;
    /** Get the type name */
    virtual std::string type_name() const = 0;
    /** reset value */
    virtual void reset() = 0;
    /** Whether this should not be removed on clear */
    bool persistent = false;

    /**
       Default constructor
    */
    attribute_base() = default;
    
    /**
       Copy constructor

       @param other Object to copy from 
    */
    attribute_base(const attribute_base& other)
      : persistent(other.persistent),
        _is_parsed(other._is_parsed)
    {}
  protected:
    /** Whether string has been parsed */
    bool _is_parsed = false;
  };

  /**
     Type of pointer to attribute 

     @ingroup hepmc_attr
  */
  using attribute_ptr=std::shared_ptr<attribute_base>;
  
  //------------------------------------------------------------------
  /**
     Attribute storage template class.  An attribute contains a value
     of a type, as iven by the first argument.  This type needs to be
     streamed in and out for input/output operations, and to be reset
     when asked to do so.  This is implemented via traits.  The type
     of the trait is the second template parameter and should be a
     class template that will be instantised on a smart pointer to
     value type type.  I.e., if the type `A` is passed as first
     parameter, then the trait template `B` passed as the second
     parameter, will be instantised on `std::smart_ptr<A>`.

     Thus, to accomplish streaming and reseting of a type, we simply
     need to implement the appropriate type specialisation of the
     template trait class `attribute_trait`.  We can also implement a
     specialisation of some other trait template.  E.g., if the type
     `A` is passed as the first parameter, and `my_trait` as the
     second, then the specialisation or instantation `my_trait<A>`
     will be used.  In that way, we can get different behaviour by
     passing different traits for the same value type.
     
     @ingroup hepmc_attr
     @headerfile attribute hepmc4/attribute
     @tparam T  Floating number type
     @tparam Trait Type trait that implements parsing, formatting, etc. 
  */
  template <typename T, template <typename> class Trait=attribute_trait>
  struct attribute : attribute_base
  {
    /** Type of value stored */
    using value_type=T;
    /** Trait that works on value */
    using trait_type=Trait<T>;

    /**
       Copy constructor - uses format and parse

       @param other Object to copy from 
    */
    attribute(const attribute<value_type>& other)
      : attribute_base(other)
    {
      trait_type::copy(_value,other._value);
    }
    /**
       Construct the attribute store

       @param value Value of attribute 
    */
    attribute(const T& value=value_type())
      : _value(value)
    {}
    /**
       Construct the attribute store

       @param s String attribute to construct from 
    */
    attribute(const std::shared_ptr<attribute<std::string>>& s)
    {
      std::stringstream str(s->value());
      parse(str);
    }

    /**
       Convert to a string representation via trait

       @param o Output stream to format to
       @return @a o
    */
    std::ostream& format(std::ostream& o) const override 
    {
      return trait_type::format(o, _value);
    }
    /**
       Convert to value from string representation via trait

       @param i Input stream to parse from
       @return @a i
    */
    std::istream& parse(std::istream& i) override 
    {
      trait_type::parse(i,_value);
      this->_is_parsed = true;
      return i;
    }
    /**
       Convert to a string via trait

       @return Value as string 
    */
    std::string to_string() const override
    {
      return trait_type::to_string(_value);
    }
    /**
       Get the value of the attribute.  Value can be assigned to.

       @return The value of the attribute 
    */
    value_type& value() { return _value; }
    /**
       Get the value of the attribute. Value is constant.

       @return The value of the attribute 
    */
    const value_type& value() const { return _value; }
    /**
       Reset value to default value
    */
    void reset() override 
    {
      trait_type::reset(_value);
      this->_is_parsed = false;
    }
    /**
       Get (mangled) type name

       @return Manged type name 
    */
    std::string type_name() const override
    {
      return trait_type::type_name();
    }
  protected:
    /** The stored value */
    value_type _value;
  };
    
  //==================================================================
  /**
     Check if type is pointer to attribute, base template 

     @ingroup hepmc_attr
     @headerfile attribute hepmc4/attribute
     @tparam T  Floating number type
     @tparam Trait Type trait that implements parsing, formatting, etc. 
  */
  template<class T,template<typename> class Trait=attribute_trait>
  struct is_attribute_ptr : std::false_type {};
  
  /**
     Check if type is pointer to attribute, specialisation 
     
     @ingroup hepmc_attr
     @headerfile attribute hepmc4/attribute
     @tparam T  Floating number type
     @tparam Trait Type trait that implements parsing, formatting, etc.
  */
  template<class T,template <typename> class Trait>
  struct is_attribute_ptr<std::shared_ptr<attribute<T,Trait>>> :
    std::true_type {};
}
#endif
// Local Variables: 
//   mode: C++
// End:



